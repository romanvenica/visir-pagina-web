<?php
session_start();
$p_rrhh = $_SESSION["p_rrhh"];
if ($p_rrhh != 1)
{
  header('Location: index.php');
  die();
} 
?>


<!DOCTYPE html>
<html>
<head>
  <link rel="icon" href="resources/pyramid.png">
	<script src="js/jquery-3.2.1.js"></script>
	<link rel="stylesheet" type="text/css" href="css/estilos.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <script type="text/javascript" src="js/sha.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<title>Carga de RRHH</title>
</head> 
<body>

  

  <nav class="navbar navbar-inverse">
    <div class="container-fluid">

          <div class="navbar-header">
            <a class="navbar-brand" href="bienvenido.php">Visir</a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav pull-right">

              <?php 

              if ($_SESSION["p_admin"] == 1) 
              {
                echo
                "
                <li>
                <a href='admin.php'>Admin</a>
                </li>
                "
                ;
              }

              if ($_SESSION["p_prod"] == 1) 
              {
                echo
                "
                <li>
                <a href='cargaProduccion.php'>Produccion</a>
                </li>
                "
                ;
              }

              if ($_SESSION["p_sdo"] == 1) 
              {
                echo
                "
                <li>
                <a href='cargaDatos.php'>Saldo</a>
                </li>
                "
                ;
              }
              
              

              if ($_SESSION["p_rrhh"] == 1) 
              {
                echo
                "
                <li>
                <a href='cargaRRHH.php'>RRHH</a>
                </li>
                "
                ;
              }

              ?>
              <a href="logout.php">
                <button class="btn btn-danger navbar-btn" class="active">Cerrar Sesion</button>
              </a>

            </ul>
          </div>
    </div>
  </nav>
  
  <div class="container-fluid col-lg-6 col-lg-offset-3">
    <div class="panel panel-primary filterable">

      <div class="panel-heading">
          <h5 class="panel-title">
            RRHH
          </h5>
      </div>
    
      <div class="panel-footer">
    
        <div class="panel-rrhh">
        
            <div class="rrhhFechaDiv">

                <object type='image/svg+xml' id="rrhhIconCheck" data='resources/check.svg' class='oculto svgIcon svgIconRRHH'></object>
                <object type='image/svg+xml' id="rrhhIconCancel" data='resources/cancel.svg' class='oculto svgIcon svgIconRRHH'></object>
                
                <span class="texto-fecha">Fecha:</span>
                &#160;
                <input type="date" id="rrhhFechaInput" class="rrhhFechaInput">
                <span id="rrhhErrorSpanFecha" class="rrhhErrorSpanFecha oculto">*Seleccione una fecha</span>
                
            </div> 

            <br>

            <div class="rrhhTituloTexto">
              Asistencia esperada:
            </div>

            <div class="rrhhAsistenciaInput" >
              <input type="text" id="inputRRHHEsperada" class="inputRRHH">
            </div>
            <div class="rrhhTituloTexto">
              Asistencia real:
            </div>

            <div class="rrhhAsistenciaInput" >
              <input type="text" id="inputRRHHReal" class="inputRRHH">
            </div>
            
            <div class="campo-panel">
              <span class="sdoErrorSpanCampos oculto" id="sdoErrorSpanCampos">
                *Solo numeros entero.
              </span>
              <br>
              <span class="sdoErrorSpanCampos oculto" id="sdoErrorSpanProporcion">
                *La asistencia real no puede ser mayor a la esperada.
              </span>
            </div>
            
          </div>
      
      
        <div class="panel-botones">
          <button type="button" class="btn btn-primary" onclick="validarRRHH('#cargarDatos')">Cargar</button>
          <button type="button" class="btn btn-warning" onclick="validarRRHH('#editarDatos')">Modificar</button>
          <button type="button" class="btn btn-danger" onclick="validarBorrar()">Borrar</button>
          <!-- <button type="button" class="btn btn-default" onclick="verDatos()">Ojito</button> -->
        </div>

      </div>

        <!--Modal confirmacion agregar datos-->

        <div id="cargarDatos" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Cargar Datos?</h4>
              </div>

              <div class="modal-body">
               
                <div class="panel-rrhh">

                  <div class="rrhhFechaDiv">
                        
                      <span class="texto-fecha">
                        Fecha:
                      </span>
                        &#160;

                      <span  id="fechaElegida">...</span>

                  </div> 
                  <br>
                  <br>


                  <div class="rrhhTituloTexto">
                    Asistencia esperada:
                  </div>

                  <div class="rrhhAsistenciaInput" >
                    <span id="asistenciaRealElegida">...</span>
                  </div>

                  <div class="rrhhTituloTexto">
                    Asistencia real:
                  </div>

                  <div class="rrhhAsistenciaInput">
                    <span id="asistenciaEsperadaElegida">...</span>
                  </div>
              
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="cargarAsistenciaLaunch()">Cargar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>

            </div>
          </div>
        </div>

  
        <!-- ################################################################# -->     
        <!--Modal confirmacion editar datos-->

        <div id="editarDatos" class="modal fade"  role="dialog">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modificar Datos?</h4>
                <h5>
                  <span id="rrhhTituloFechaModificar">
                    ...
                  </span>
                </h1>
              </div>
              
              <div class="modal-body">

                <div class="panel-izquierdo-modificar panel-rrhh">

                  <div class="cabeza-panel-izquierdo">
                    Datos Anteriores
                  </div>

                  <div class="panel-rrhh">
            

                    <br>

                    <div class="rrhhTituloTexto">
                      Asistencia esperada:
                    </div>

                    <div class="rrhhAsistenciaInput">
                      <span id="rrhhAsistenciaEsperadaAnterior">...</span>
                    </div>

                    <div class="rrhhTituloTexto">
                      Asistencia real:
                    </div>

                    <div class="rrhhAsistenciaInput">
                      <span id="rrhhAsistenciaRealAnterior">...</span>
                    </div>
              
                </div>
              </div>


                 <div class="panel-izquierdo-modificar panel-derecho panel-rrhh">

                  <div class="cabeza-panel-izquierdo">
                    Datos Nuevos
                  </div>
                  
                  <div class="panel-rrhh">


                    <br>

                    <div class="rrhhTituloTexto">
                      Asistencia esperada:
                    </div>

                    <div class="rrhhAsistenciaInput" >
                      <span id="rrhhEsperadaFechaModificar">...</span>
                    </div>

                    <div class="rrhhTituloTexto">
                      Asistencia real:
                    </div>

                    <div class="rrhhAsistenciaInput" >
                      <span id="rrhhRealFechaModificar">...</span>
                    </div>
                
                  </div>
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal" onclick="modificarAsistenciaLaunch()">Modificar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>

        <div id="borrarDatos" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Borrar Datos?</h4>
              </div>

              <div class="modal-body">
               
                <div class="panel-rrhh">

            
                  <div class="rrhhFechaDiv">
                        
                      <span class="texto-fecha">
                        Fecha:
                      </span>
                        &#160;

                      <span id="rrhhFechaActualBorrar">...</span>

                  </div> 
                  <br>
                  <br>


                  <div class="rrhhTituloTexto">
                    Asistencia esperada:
                  </div>

                  <div class="rrhhAsistenciaInput" >
                    <span id="rrhhAsistenciaActualEsperadaBorrar">...</span>
                  </div>

                  <div class="rrhhTituloTexto">
                    Asistencia real:
                  </div>

                  <div class="rrhhAsistenciaInput" >
                    <span id="rrhhAsistenciaRealActualBorrar">...</span>
                  </div>
              
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="borrarAsistenciaLaunch()">Borrar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>

            </div>
          </div>
        </div>



        <!-- MODALES DE AVISOS -->

        <!-- MODAL CONTRASENIA QUE PIDE MODIFICAR -->
        <div id="contraseniaModificar" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title prodContraseniaTextoTitulo">Requiere autorizacion</h4>
              </div>

              <div class="modal-body">
                <div class="prodContraseniaDivPrincipal">
                  <span>Contraseña:</span>
                  <br>
                  <input type="password" class="inputCentrado" id="prodContrInput">
                </div>
                <div class="prodContraseniaDivMensaje">
                  <span class="prodContraseniaSpanMsj oculto">
                    Contraseña incorrecta
                  </span>
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="validarContraseniaModificar()">Aceptar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>

            </div>
          </div>
        </div>

        <!-- MODAL CONTRASENIA QUE PIDE BORRAR -->
        <div id="contraseniaBorrar" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title prodContraseniaTextoTitulo">Requiere autorizacion</h4>
              </div>

              <div class="modal-body">
                <div class="prodContraseniaDivPrincipal">
                  <span>Contraseña:</span>
                  <br>
                  <input type="password" class="inputCentrado" id="prodContrInputBorrar">
                </div>
                <div class="prodContraseniaDivMensaje">
                  <span class="prodContraseniaSpanMsj oculto">
                    Contraseña incorrecta
                  </span>
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="validarContraseniaBorrar()">Aceptar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>

            </div>
          </div>
        </div>


        <!-- MODAL QUE AVISA QUE NO HAY FECHA CARGADA -->
        <div id="sinFechaModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">

              <div class="modal-header">
                <h4 class="modal-title prodContraseniaTextoTitulo">Alerta</h4>
              </div>

              <div class="modal-body">
                <div class="prodContraseniaDivPrincipal">
                  <span>No se ha encontrado registro cargado en esa fecha.</span>
                  <br>
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
              </div>

            </div>
          </div>
        </div>


        <!-- MODAL QUE AVISA QUE SE BORRARON LOS DATOS -->

        <div id="asistenciaBorradaModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">

              <div class="modal-header">
                <h4 class="modal-title prodContraseniaTextoTitulo">Alerta</h4>
              </div>

              <div class="modal-body">
                <div class="prodContraseniaDivPrincipal">
                  <span>Se borraron los datos de asistencia correctamente.</span>
                  <br>
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
              </div>

            </div>
          </div>
        </div>


        <!-- MODAL QUE AVISA QUE SE CARGARON LOS DATOS -->

        <div id="asistenciaCargadaModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">

              <div class="modal-header">
                <h4 class="modal-title prodContraseniaTextoTitulo">Alerta</h4>
              </div>

              <div class="modal-body">
                <div class="prodContraseniaDivPrincipal">
                  <span>La asistencia se cargó correctamente.</span>
                  <br>
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
              </div>

            </div>
          </div>
        </div>

        <!-- MODAL QUE AVISA QUE HAY FECHA CARGADA -->
        <div id="fechaExistenteModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">

              <div class="modal-header">
                <h4 class="modal-title prodContraseniaTextoTitulo">Alerta</h4>
              </div>

              <div class="modal-body">
                <div class="prodContraseniaDivPrincipal">
                  <span>Ya hay un registro cargado en esa fecha.</span>
                  <br>
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
              </div>

            </div>
          </div>
        </div>


        <!-- MODAL QUE TE MUESTRA LOS REGISTROS DEL MES -->
        <div id="registrosMesModal" class="modal fade modalTabla" role="dialog">
          <div class="modal-dialog inicial">
            <div class="modal-content">

              <div class="modal-header">
                <h4 class="modal-title prodContraseniaTextoTitulo">Asistencia del mes</h4>
              </div>

              <div class="modal-body divTablaScrollable">
                <div>
                    <table class="admin-tabla-posta table table-hover table-condensed" id="tablaRRHH">
                    <thead>
                      <tr>
                        <th>Asistencia esperada</th>
                        <th>Asistencia real</th> 
                        <th>Fecha</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
              </div>

            </div>
          </div>
        </div>


        <!-- MODAL QUE AVISA QUE NO HAY CONEXIONES CON LA BASE DE DATOS -->
        <div id="noConexionModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">

              <div class="modal-header">
                <h4 class="modal-title text-center prodContraseniaTextoTitulo textoRojo">Alerta</h4>
              </div>

              <div class="modal-body">
                <div class="prodContraseniaDivPrincipal">
                  <span>No se puede conectar con el servidor.</span>
                  <br>
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
              </div>

            </div>
          </div>
        </div>



    </div>
  </div>

</body>
  <script type="text/javascript" src="js/jsRRHH.js"></script>

</html>