<?php

session_start();
include("php/sesion.php");


if (isset($_SESSION["p_acc"])) 
{
	header('Location: bienvenido.php');
}
else
{
	
	if(!empty($_POST['usuario']) && !empty($_POST['password'])) 
	{	
		$username = $_POST['usuario'];
		$password = $_POST['password'];
		$passwordHash = sha1($password);

		$user = mysqli_query($connect, "SELECT * FROM usuarios WHERE usuario = '$username' AND password = '$passwordHash'");

		mysqli_close($connect);	
		
		$numrows=mysqli_num_rows($user);
	 	if($numrows!=0)
		{
			
	 		while($row=mysqli_fetch_assoc($user))
	 		{

	 			$dbiduser=$row['id_user'];
	 			$p_acc=$row['p_acc'];
				$p_admin=$row['p_admin'];	 			
				$p_prod=$row['p_prod'];	 			
				$p_rrhh=$row['p_rrhh'];	 			
				$p_sdo=$row['p_sdo'];	 			
				$p_ing_min=$row['p_ing_min'];	 			
				$p_ing_may=$row['p_ing_may'];	 			
				$p_ing_ext=$row['p_ing_ext'];	 			
				$p_egr=$row['p_egr'];	 			
				$p_egr_ext=$row['p_egr_ext'];	 			
				$p_ret=$row['p_ret'];	 			
				$p_acob=$row['p_acob'];	 			
				$p_apag=$row['p_apag'];	 			
				$p_sfr=$row['p_sfr'];	 			
	 		}

	 		$_SESSION["iduser"] = "$dbiduser";
	 		$_SESSION["p_acc"] = "$p_acc";
	 		$_SESSION["p_admin"] = "$p_admin";
	 		$_SESSION["p_prod"] = "$p_prod";
	 		$_SESSION["p_rrhh"] = "$p_rrhh";
	 		$_SESSION["p_sdo"] = "$p_sdo";
	 		$_SESSION["p_sfr_ant"] = "$p_sfr_ant";
	 		$_SESSION["p_ing_min"] = "$p_ing_min";
	 		$_SESSION["p_ing_may"] = "$p_ing_may";
	 		$_SESSION["p_ing_ext"] = "$p_ing_ext";
	 		$_SESSION["p_egr"] = "$p_egr";
	 		$_SESSION["p_egr_ext"] = "$p_egr_ext";
	 		$_SESSION["p_ret"] = "$p_ret";
	 		$_SESSION["p_acob"] = "$p_acob";
	 		$_SESSION["p_apag"] = "$p_apag";
	 		$_SESSION["p_sfr"] = "$p_sfr";

 			header('Location: bienvenido.php');

	 	}
	 	else 
 		{
			$message = "Nombre de usuario o contraseña invalidos!";
 		}
	}
}

 ?>

<!DOCTYPE html>
<sdf>
<head>
  <link rel="icon" href="resources/pyramid.png">
	<script src="js/jquery-3.2.1.js"></script>
	<link rel="stylesheet" type="text/css" href="css/estilos.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<script type="text/javascript" src="js/bootstrap.js"></script>
	
	<title>Ingresando</title>
	


	<style>
		
	.top{
		margin-top: 30%;
	}

	.caja{
		border-radius: 5px;
	}

	</style>

</head>
<body>

	<nav class="navbar navbar-inverse">
      <div class="container-fluid">

          <a class="navbar-brand" href="#">Visir</a>

      </div>
    </nav>


	<div class="container">
		<div class="col-md-4 col-md-offset-4">
			<section>
				<div class="panel panel-default top caja">
				  <div class="panel-body">
				    <h3 class="text-center">Login</h3>

					<form method = "post" action="index.php">
					<form>

					<div class="input-group input-group-lg">
					  <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-user" aria-hidden="true"></i></span>
					  <input type="text" name="usuario" id="usuario" class="form-control" placeholder="Usuario" aria-describedby="sizing-addon1" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" required>
					</div>
					<br>

					<div class="input-group input-group-lg">
					  <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-lock" aria-hidden="true"></i></span>
					  <input type="password" name="password" id="password" class="form-control" placeholder="Contraseña" aria-describedby="sizing-addon1" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" required>
					</div>

					<br>

					<input type="submit" id="botonSubmit" class="btn btn-primary btn-block" value="Ingresar" name="submit">
					</button>
					
					
					</form>

					</div>
				</div>
					<div id="errores">
						<span class="hidden text-danger" id="errorUsuario" >El usuario solo debe contener letras, numeros y guiones bajos.</span>
						<span class="hidden text-danger" id="errorContrasenia">Contraseña sin formato valido.</span>
					</div>
					<?php if (!empty($message)) {echo "<p class=text-danger \"color\" >" . "* ". $message . "</p>";} ?>
			</section>
		</div>
	</div>

	


</body>
</html>