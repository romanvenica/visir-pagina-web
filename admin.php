<?php
session_start();
$p_admin = $_SESSION["p_admin"];
if ($p_admin != 1)
{
  header('Location: index.php');
  die();
} 
?>

<!DOCTYPE html>
<html>
<head>
  <link rel="icon" href="resources/pyramid.png">
	<script src="js/jquery-3.2.1.js"></script>
	<link rel="stylesheet" type="text/css" href="css/estilos.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <script type="text/javascript" src="js/sha.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<title>Carga de saldo</title>
</head> 
<body>

  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <a class="navbar-brand" href="bienvenido.php">Visir</a>
        <ul class="nav navbar-nav pull-right">

          <?php 
          if ($_SESSION["p_admin"] == 1) 
          {
            echo
            "
            <li>
            <a href='admin.php'>Admin</a>
            </li>
            "
            ;
          }

          if ($_SESSION["p_prod"] == 1) 
          {
            echo
            "
            <li>
            <a href='cargaProduccion.php'>Produccion</a>
            </li>
            "
            ;
          }

          if ($_SESSION["p_sdo"] == 1) 
          {
            echo
            "
            <li>
            <a href='cargaDatos.php'>Saldo</a>
            </li>
            "
            ;
          }
          
          

          if ($_SESSION["p_rrhh"] == 1) 
          {
            echo
            "
            <li>
            <a href='cargaRRHH.php'>RRHH</a>
            </li>
            "
            ;
          }
          ?>

          <a href="logout.php">
            <button class="btn btn-danger navbar-btn" class="active">Cerrar Sesion</button>
          </a>
        </ul>
    </div>
  </nav>
  
  <div class="container-fluid col-lg-6 col-lg-offset-3">
    <div class="panel panel-primary filterable">

      <div class="panel-heading">
          <h5 class="panel-title">
            ADMINISTRADOR
          </h5>
          <div class="pull-right">
          </div>
      </div>
    
      <div class="panel-footer">

        <div class="div-admin">
          <div class="admin-filtros">
            <span>Filtro:</span>
           <!-- <input style="display:none" type="text" name="filtro" class="form-control">-->
            <input autocomplete="off" type="text" id="inputAdminFiltro" name="filtro" class="form-control">
          </div>

          <div class="admin-tabla">

            <table class="admin-tabla-posta table table-hover table-condensed" id="tablaUsuarios">
              <thead>
                <tr>
                  <th>Usuario</th>
                  <th>Recordatorio</th> 
                  <th></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>

          <div class="admin-botones">
            <button class="btn btn-primary admin-boton-agregar" data-toggle="modal" data-target="#agregarUsuario">
              Agregar
            </button>
          </div>      
        </div>
      </div>

    </div>
  </div>

    <!-- MODAL AGREGAR USUARIO -->
    <div id="agregarUsuario" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title prodContraseniaTextoTitulo">Agregar Usuario</h4>
          </div>

          <div class="modal-body">
            <form id="formAgregarUser">
              <div class="form-group">
                <label for="usuario">Usuario:</label>
                <input maxlength="15" type="text" class="form-control" id="usuario" autocomplete="off">
              </div>
              <div class="form-group">
                <label for="contrasenia">Contraseña:</label>
                <input maxlength="40" type="text" class="form-control" id="contrasenia" autocomplete="off">
              </div>
              <div class="form-group">
                <label for="recordatorio">Recordatorio de quien es el usuario (opcional):</label>
                <input maxlength="30" type="text" class="form-control" id="recordatorio" autocomplete="off">
              </div>
              <div class="checkbox">
                <label><input type="checkbox" id="p_admin"> Admin </label>
              </div>
              <div class="checkbox">
                <label class="user-checkbox"><input type="checkbox" id="p_prod"> Produccion </label>
                <label class="user-checkbox"><input type="checkbox" id="p_rrhh"> RRHH </label>
                <label class="user-checkbox"><input type="checkbox" id="p_sdo" onclick="checkSaldoAgregar()"> Saldo </label>
              </div>
              <div class="checkbox">
                <label class="agregar-texto-checkbox gris user-checkbox"><input class="checksSdoAgregar" disabled type="checkbox" id="p_ing_min"> Ing. Minorista </label>
                <label class="agregar-texto-checkbox gris user-checkbox"><input class="checksSdoAgregar" disabled type="checkbox" id="p_ing_may"> Ing. Mayorista </label>
                <label class="agregar-texto-checkbox gris user-checkbox"><input class="checksSdoAgregar" disabled type="checkbox" id="p_ing_ext"> Ing. Extraord. </label>
                <label class="agregar-texto-checkbox gris user-checkbox"><input class="checksSdoAgregar" disabled type="checkbox" id="p_egr"> Egreso </label>
                <label class="agregar-texto-checkbox gris user-checkbox"><input class="checksSdoAgregar" disabled type="checkbox" id="p_egr_ext"> Egreso Extraord. </label>
                <label class="agregar-texto-checkbox gris user-checkbox"><input class="checksSdoAgregar" disabled type="checkbox" id="p_ret"> Retiros </label>
                <label class="agregar-texto-checkbox gris user-checkbox"><input class="checksSdoAgregar" disabled type="checkbox" id="p_acob"> A Cobrar </label>
                <label class="agregar-texto-checkbox gris user-checkbox"><input class="checksSdoAgregar" disabled type="checkbox" id="p_apag"> A Pagar </label>
                <label class="agregar-texto-checkbox gris user-checkbox"><input class="checksSdoAgregar" disabled type="checkbox" id="p_sfr"> Saldo Final Real </label>
              </div>
            </form>
          </div>

          <div class="modal-footer">
            <span id="spanErrorCargarCampos" class="spanErrorAdmin oculto">Campos de usuario o contraseña incorrectos.</span>
            <span id="spanErrorCargarExiste" class="spanErrorAdmin oculto">Ya existe un usuario con ese nombre.</span>
            <button type="button" class="btn btn-primary" onclick="checkearUsuario(), cargarTablaUsuarios()">Aceptar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="limpiarForm()">Cerrar</button>
          </div>

        </div>
      </div>
    </div>



    <!-- MODAL VER USUARIO -->
    <div id="modalVerUsuario" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title prodContraseniaTextoTitulo">Datos de usuario</h4>
          </div>

          <div class="modal-body">
            <form action="/action_page.php">
              <div class="form-group">
                <label for="usuario">Usuario:</label>
                <label type="text" class="form-control" id="verUsuario" autocomplete="off">
              </div>
              <div class="form-group">
                <label for="recordatorio">Recordatorio de quien es el usuario (opcional):</label>
                <label type="text" class="form-control" id="verRecordatorio" autocomplete="off">
              </div>
              <div class="checkbox">
                <label class="gris"><input disabled type="checkbox" id="ver_p_admin"> Admin </label>
              </div>
              <div class="checkbox">
                <label class="gris user-checkbox"><input disabled type="checkbox" id="ver_p_prod"> Produccion </label>
                <label class="gris user-checkbox"><input disabled type="checkbox" id="ver_p_rrhh"> RRHH </label>
                <label class="gris user-checkbox"><input disabled type="checkbox" id="ver_p_sdo" onclick="checkSaldoAgregar()"> Saldo </label>
              </div>
              <div class="checkbox">
                <label class="ver-texto-checkbox gris user-checkbox"><input class="checksSdoVer" disabled type="checkbox" id="ver_p_ing_min"> Ing. Minorista </label>
                <label class="ver-texto-checkbox gris user-checkbox"><input class="checksSdoVer" disabled type="checkbox" id="ver_p_ing_may"> Ing. Mayorista </label>
                <label class="ver-texto-checkbox gris user-checkbox"><input class="checksSdoVer" disabled type="checkbox" id="ver_p_ing_ext"> Ing. Extraord. </label>
                <label class="ver-texto-checkbox gris user-checkbox"><input class="checksSdoVer" disabled type="checkbox" id="ver_p_egr"> Egreso </label>
                <label class="ver-texto-checkbox gris user-checkbox"><input class="checksSdoVer" disabled type="checkbox" id="ver_p_egr_ext"> Egreso Extraord.</label>
                <label class="ver-texto-checkbox gris user-checkbox"><input class="checksSdoVer" disabled type="checkbox" id="ver_p_ret"> Retiros </label>
                <label class="ver-texto-checkbox gris user-checkbox"><input class="checksSdoVer" disabled type="checkbox" id="ver_p_acob"> A Cobrar </label>
                <label class="ver-texto-checkbox gris user-checkbox"><input class="checksSdoVer" disabled type="checkbox" id="ver_p_apag"> A Pagar </label>
                <label class="ver-texto-checkbox gris user-checkbox"><input class="checksSdoVer" disabled type="checkbox" id="ver_p_sfr"> Saldo Final Real </label>
              </div>
            </form>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal" id="botonAceptarVer">Aceptar</button>
          </div>

        </div>
      </div>
    </div>


    <!-- MODAL MODIFICAR USUARIO -->
    <div id="modalModUsuario" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title prodContraseniaTextoTitulo">Modificar Usuario</h4>
          </div>
          <div class="modal-body">
            <form id="formModificarUser">
              <input type="hidden" id="formModificarIdOculto">
              <div class="form-group">
                <label for="usuario">Usuario:</label>
                <input maxlength="15" type="text" class="form-control" id="modUsuario" autocomplete="off">
              </div>
              <div class="form-group">
                <label for="contrasenia">Nueva Contraseña:</label>
                <input type="checkbox" id="checkNuevaContra" onclick="nuevaContrasenia()">
                <input maxlength="40" disabled type="text" class="form-control" id="modContrasenia" autocomplete="off">
              </div>
              <div class="form-group">
                <label for="recordatorio">Recordatorio de quien es el usuario (opcional):</label>
                <input maxlength="30" type="text" class="form-control" id="modRecordatorio" autocomplete="off">
              </div>
              <div class="checkbox">
                <label><input type="checkbox" id="mod_p_admin"> Admin </label>
              </div>
              <div class="checkbox">
                <label class="user-checkbox"><input type="checkbox" id="mod_p_prod"> Produccion </label>
                <label class="user-checkbox"><input type="checkbox" id="mod_p_rrhh"> RRHH </label>
                <label class="user-checkbox"><input type="checkbox" id="mod_p_sdo" onclick="checkSaldoModificar()"> Saldo </label>
              </div>
              <div class="checkbox">
                <label class="mod-texto-checkbox gris user-checkbox"><input class="checksSdoMod" disabled type="checkbox" id="mod_p_ing_min"> Ing. Minorista </label>
                <label class="mod-texto-checkbox gris user-checkbox"><input class="checksSdoMod" disabled type="checkbox" id="mod_p_ing_may"> Ing. Mayorista </label>
                <label class="mod-texto-checkbox gris user-checkbox"><input class="checksSdoMod" disabled type="checkbox" id="mod_p_ing_ext"> Ing. Extraord. </label>
                <label class="mod-texto-checkbox gris user-checkbox"><input class="checksSdoMod" disabled type="checkbox" id="mod_p_egr"> Egreso </label>
                <label class="mod-texto-checkbox gris user-checkbox"><input class="checksSdoMod" disabled type="checkbox" id="mod_p_egr_ext"> Egreso Extraord.</label>
                <label class="mod-texto-checkbox gris user-checkbox"><input class="checksSdoMod" disabled type="checkbox" id="mod_p_ret"> Retiros </label>
                <label class="mod-texto-checkbox gris user-checkbox"><input class="checksSdoMod" disabled type="checkbox" id="mod_p_acob"> A Cobrar </label>
                <label class="mod-texto-checkbox gris user-checkbox"><input class="checksSdoMod" disabled type="checkbox" id="mod_p_apag"> A Pagar </label>
                <label class="mod-texto-checkbox gris user-checkbox"><input class="checksSdoMod" disabled type="checkbox" id="mod_p_sfr"> Saldo Final Real </label>
              </div>
            </form>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="checkearUsuarioModificar(), cargarTablaUsuarios()">Aceptar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <span id="spanErrorModificarCampos" class="spanErrorAdmin oculto">Campos de usuario o contraseña incorrectos.</span>
            <span id="spanErrorModificarExiste" class="spanErrorAdmin oculto">Ya existe un usuario con ese nombre.</span>
          </div>

        </div>
      </div>
    </div>




    <!-- MODAL CONTRASENIA -->
    <div id="contraseniaBorrar" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title prodContraseniaTextoTitulo">Requiere autorizacion</h4>
          </div>

          <div class="modal-body">
            <div class="prodContraseniaDivPrincipal">
              <span>Contraseña:</span>
              <br>
              <input type="password" class="inputCentrado" id="sdoContrInputBorrar">
            </div>
            <div class="prodContraseniaDivMensaje">
              <span class="prodContraseniaSpanMsj oculto" id="prodContraseniaSpanMsjBorrar">
                Contraseña incorrecta
              </span>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="validarContraseniaBorrar()">Aceptar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>

    <!-- MODAL BORRAR-->
       <div id="modalBorrarUsuario" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title prodContraseniaTextoTitulo">Seguro quiere borrar al usuario?</h4>
          </div>

          <div class="modal-body">
            <form action="/action_page.php">
              <input type="hidden" id="formBorrarIdOculto">
              <div class="form-group">
                <label for="usuario">Usuario:</label>
                <label type="text" class="form-control" id="borrarUsuario" autocomplete="off">
              </div>
              <div class="form-group">
                <label for="recordatorio">Recordatorio de quien es el usuario (opcional):</label>
                <label type="text" class="form-control" id="borrarRecordatorio" autocomplete="off">
              </div>
              <div class="checkbox">
                <label class="gris"><input disabled type="checkbox" id="borrar_p_admin"> Admin </label>
              </div>
              <div class="checkbox">
                <label class="gris user-checkbox"><input disabled type="checkbox" id="borrar_p_prod"> Produccion </label>
                <label class="gris user-checkbox"><input disabled type="checkbox" id="borrar_p_rrhh"> RRHH </label>
                <label class="gris user-checkbox"><input disabled type="checkbox" id="borrar_p_sdo" onclick="checkSaldoAgregar()"> Saldo </label>
              </div>
              <div class="checkbox">
                <label class="borrar-texto-checkbox gris user-checkbox"><input class="checksSdoBorrar" disabled type="checkbox" id="borrar_p_ing_min"> Ing. Minorista </label>
                <label class="borrar-texto-checkbox gris user-checkbox"><input class="checksSdoBorrar" disabled type="checkbox" id="borrar_p_ing_may"> Ing. Mayorista </label>
                <label class="borrar-texto-checkbox gris user-checkbox"><input class="checksSdoBorrar" disabled type="checkbox" id="borrar_p_ing_ext"> Ing. Extraord. </label>
                <label class="borrar-texto-checkbox gris user-checkbox"><input class="checksSdoBorrar" disabled type="checkbox" id="borrar_p_egr"> Egreso </label>
                <label class="borrar-texto-checkbox gris user-checkbox"><input class="checksSdoBorrar" disabled type="checkbox" id="borrar_p_egr_ext"> Egreso Extraord. </label>
                <label class="borrar-texto-checkbox gris user-checkbox"><input class="checksSdoBorrar" disabled type="checkbox" id="borrar_p_ret"> Retiros </label>
                <label class="borrar-texto-checkbox gris user-checkbox"><input class="checksSdoBorrar" disabled type="checkbox" id="borrar_p_acob"> A Cobrar </label>
                <label class="borrar-texto-checkbox gris user-checkbox"><input class="checksSdoBorrar" disabled type="checkbox" id="borrar_p_apag"> A Pagar </label>
                <label class="borrar-texto-checkbox gris user-checkbox"><input class="checksSdoBorrar" disabled type="checkbox" id="borrar_p_sfr"> Saldo Final Real </label>
              </div>
            </form>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="borrarUsuarioLaunch(), cargarTablaUsuarios()" 
            >Borrar usuario</button>
             <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>

        </div>
      </div>
    </div>



    <!-- MODAL QUE AVISA QUE NO HAY CONEXIONES CON LA BASE DE DATOS -->
    <div id="noConexionModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title text-center prodContraseniaTextoTitulo textoRojo">Alerta</h4>
          </div>

          <div class="modal-body">
            <div class="prodContraseniaDivPrincipal">
              <span>No se puede conectar con el servidor.</span>
              <br>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>

          </div>

        </div>
      </div>
    </div>

    <!-- MODAL QUE AVISA QUE SE BORRARON LOS DATOS -->

        <div id="usuarioBorradoModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">

              <div class="modal-header">
                <h4 class="modal-title prodContraseniaTextoTitulo">Alerta</h4>
              </div>

              <div class="modal-body">
                <div class="prodContraseniaDivPrincipal">
                  <span>Se borraron los datos del usuario correctamente.</span>
                  <br>
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="cargarTablaUsuarios()">Aceptar</button>
              </div>

            </div>
          </div>
        </div>


 <!-- MODAL QUE AVISA QUE SE CARGARON LOS DATOS -->

        <div id="usuarioCargadoModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">

              <div class="modal-header">
                <h4 class="modal-title prodContraseniaTextoTitulo">Alerta</h4>
              </div>

              <div class="modal-body">
                <div class="prodContraseniaDivPrincipal">
                  <span>El usuario se creó correctamente.</span>
                  <br>
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="cargarTablaUsuarios()">Aceptar</button>
              </div>

            </div>
          </div>
        </div>


   <!-- MODAL QUE AVISA QUE YA EXISTE USER CON ESE NOMBRE -->

        <div id="usuarioYaExisteModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">

              <div class="modal-header">
                <h4 class="modal-title prodContraseniaTextoTitulo">Alerta</h4>
              </div>

              <div class="modal-body">
                <div class="prodContraseniaDivPrincipal">
                  <span>Ya existe un usuario con ese nombre.</span>
                  <br>
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
              </div>

            </div>
          </div>
        </div>


  <!-- MODAL QUE AVISA QUE SE MODIFICARON LOS DATOS -->

        <div id="usuarioModificadoModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">

              <div class="modal-header">
                <h4 class="modal-title prodContraseniaTextoTitulo">Alerta</h4>
              </div>

              <div class="modal-body">
                <div class="prodContraseniaDivPrincipal">
                  <span>Se modificaron los datos de usuario correctamente.</span>
                  <br>
                </div>

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="cargarTablaUsuarios()">Aceptar</button>
              </div>

            </div>
          </div>
        </div>


  </body>

  <script type="text/javascript" src="js/jsAdmin.js"></script>

</html>