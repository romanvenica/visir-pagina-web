-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-04-2019 a las 18:19:18
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `visir`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `id_password` int(11) NOT NULL,
  `password` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`id_password`, `password`) VALUES
(1, 'd033e22ae348aeb5660fc2140aec35850c4da997');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presentismo`
--

CREATE TABLE `presentismo` (
  `id_presentismo` int(10) UNSIGNED NOT NULL,
  `fecha` date NOT NULL,
  `asist_esperada` smallint(6) UNSIGNED NOT NULL,
  `asist_real` smallint(6) UNSIGNED NOT NULL,
  `estado` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `presentismo`
--

INSERT INTO `presentismo` (`id_presentismo`, `fecha`, `asist_esperada`, `asist_real`, `estado`) VALUES
(17, '2018-05-07', 23, 23, 1),
(18, '2018-07-02', 23, 23, 1),
(19, '2018-01-01', 230, 23, 2),
(20, '2018-08-29', 20, 19, 1),
(22, '2018-08-01', 40, 40, 1),
(23, '2018-08-02', 40, 39, 1),
(24, '2018-08-03', 40, 35, 1),
(25, '2018-01-02', 40, 35, 1),
(26, '2018-01-03', 40, 36, 1),
(27, '2018-01-04', 40, 37, 1),
(28, '2018-01-05', 20, 20, 1),
(29, '2018-01-06', 20, 19, 1),
(30, '2018-01-01', 50, 40, 1),
(31, '2018-01-07', 20, 19, 1),
(32, '2018-01-08', 20, 19, 1),
(33, '2018-01-09', 20, 10, 1),
(34, '2018-01-10', 32, 30, 1),
(35, '2018-01-11', 32, 25, 1),
(36, '2018-01-12', 32, 26, 1),
(37, '2018-01-13', 32, 28, 1),
(38, '2018-01-14', 32, 20, 1),
(39, '2018-01-15', 33, 20, 1),
(40, '2018-01-16', 33, 22, 1),
(41, '2018-10-01', 234, 234, 1),
(42, '2018-10-02', 3, 3, 1),
(43, '2018-10-30', 200, 2, 3),
(44, '2018-10-21', 23, 23, 1),
(45, '2018-10-15', 23, 23, 1),
(46, '2018-10-22', 23, 23, 1),
(47, '2018-10-23', 23, 23, 3),
(48, '2018-10-23', 23, 23, 1),
(49, '2018-10-17', 23, 23, 3),
(50, '2018-10-03', 34, 34, 1),
(51, '2018-10-04', 34, 34, 1),
(52, '2018-10-05', 34, 34, 1),
(53, '2018-10-06', 34, 34, 1),
(54, '2018-10-07', 34, 34, 1),
(55, '2018-10-08', 34, 34, 1),
(56, '2018-10-09', 34, 34, 1),
(57, '2018-10-24', 23, 23, 1),
(58, '2018-10-25', 23, 23, 1),
(59, '2018-10-26', 23, 23, 1),
(60, '2018-10-27', 23, 23, 1),
(61, '2018-10-28', 23, 23, 1),
(62, '2018-10-29', 23, 23, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prodcercorte`
--

CREATE TABLE `prodcercorte` (
  `id_prodcercorte` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `compraKG` int(11) NOT NULL,
  `sobraKG` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `prodcercorte`
--

INSERT INTO `prodcercorte` (`id_prodcercorte`, `estado`, `fecha`, `compraKG`, `sobraKG`) VALUES
(1, 1, '2018-12-17', 1, 2),
(2, 2, '2018-12-10', 4, 5),
(3, 3, '2018-12-03', 8, 8),
(4, 2, '2018-12-10', 3, 4),
(5, 3, '2018-12-10', 4, 5),
(6, 1, '2018-12-10', 5, 7),
(7, 1, '2018-12-24', 23, 23),
(8, 1, '2019-03-11', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prodcermedia`
--

CREATE TABLE `prodcermedia` (
  `id_prodcermedia` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `compraKG` int(11) NOT NULL,
  `compraUM` int(11) NOT NULL,
  `ventaKG` int(11) NOT NULL,
  `ventaUM` int(11) NOT NULL,
  `prodCorteKG` int(11) NOT NULL,
  `prodCorteUM` int(11) NOT NULL,
  `sobraKG` int(11) NOT NULL,
  `sobraUM` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `prodcermedia`
--

INSERT INTO `prodcermedia` (`id_prodcermedia`, `estado`, `fecha`, `compraKG`, `compraUM`, `ventaKG`, `ventaUM`, `prodCorteKG`, `prodCorteUM`, `sobraKG`, `sobraUM`) VALUES
(1, 3, '2018-12-10', 1, 5, 2, 6, 3, 7, 4, 8),
(2, 2, '2018-12-03', 7, 7, 7, 7, 7, 7, 7, 7),
(3, 1, '2018-12-03', 9, 9, 9, 99, 9, 9, 9, 9),
(4, 1, '2018-12-24', 1, 5, 2, 6, 3, 7, 4, 8),
(5, 3, '2018-12-31', 2, 12, 4, 14, 6, 16, 8, 18),
(6, 1, '2018-12-31', 1, 5, 2, 6, 3, 7, 4, 8),
(7, 1, '2019-03-11', 1, 5, 2, 6, 3, 7, 4, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prodemb`
--

CREATE TABLE `prodemb` (
  `id` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `produccion` int(11) NOT NULL,
  `sobra` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `prodemb`
--

INSERT INTO `prodemb` (`id`, `estado`, `fecha`, `produccion`, `sobra`) VALUES
(1, 1, '2018-07-02', 0, 0),
(2, 1, '2018-08-06', 0, 0),
(3, 1, '2018-09-03', 0, 0),
(4, 3, '2018-10-01', 0, 0),
(5, 1, '2018-10-29', 0, 0),
(6, 1, '2018-12-31', 5, 6),
(7, 2, '2018-12-03', 23, 23),
(8, 1, '2018-12-24', 5, 5),
(9, 1, '2018-12-03', 23, 23),
(10, 3, '2018-12-17', 5, 6),
(11, 2, '2019-03-11', 5, 6),
(12, 1, '2019-03-11', 5, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prodfiam`
--

CREATE TABLE `prodfiam` (
  `id` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `produccion` int(11) NOT NULL,
  `sobra` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `prodfiam`
--

INSERT INTO `prodfiam` (`id`, `estado`, `fecha`, `produccion`, `sobra`) VALUES
(1, 1, '2018-07-02', 0, 0),
(2, 1, '2018-08-06', 0, 0),
(3, 1, '2018-09-03', 0, 0),
(4, 3, '2018-10-29', 0, 0),
(5, 1, '2018-12-31', 3, 4),
(6, 1, '2018-12-03', 23, 23),
(7, 3, '2018-12-17', 5, 3),
(8, 2, '2018-12-24', 4, 4),
(9, 2, '2018-12-24', 3, 3),
(10, 2, '2018-12-24', 89, 89),
(11, 2, '2018-12-24', 56, 56),
(12, 3, '2018-12-24', 56, 56),
(13, 3, '2018-12-24', 78, 78),
(14, 1, '2019-03-11', 12, 123);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prodpoll`
--

CREATE TABLE `prodpoll` (
  `id` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `compra` int(11) NOT NULL,
  `venta` int(11) NOT NULL,
  `prodCorte` int(11) NOT NULL,
  `sobra` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `prodpoll`
--

INSERT INTO `prodpoll` (`id`, `estado`, `fecha`, `compra`, `venta`, `prodCorte`, `sobra`) VALUES
(13, 2, '2018-12-31', 89, 89, 89, 89),
(14, 1, '2018-12-31', 0, 0, 0, 0),
(15, 2, '2018-12-24', 7, 7, 7, 7),
(16, 1, '2018-12-24', 2, 3, 3, 1),
(17, 1, '2018-12-10', 2, 3, 4, 5),
(18, 3, '2018-12-17', 1, 2, 3, 4),
(19, 1, '2019-03-11', 67, 34, 543, 324);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prodvaccorte`
--

CREATE TABLE `prodvaccorte` (
  `id_prodvaccorte` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `compraCorte` int(30) NOT NULL,
  `sobraCorte` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `prodvaccorte`
--

INSERT INTO `prodvaccorte` (`id_prodvaccorte`, `estado`, `fecha`, `compraCorte`, `sobraCorte`) VALUES
(18, 2, '2018-12-10', 5, 5),
(19, 2, '2018-12-10', 3, 4),
(20, 2, '2018-12-10', 3, 4),
(21, 2, '2018-12-10', 1, 6),
(22, 2, '2018-12-10', 4, 5),
(23, 2, '2018-12-10', 6, 7),
(24, 3, '2018-12-10', 897, 89),
(25, 2, '2018-12-10', 3, 4),
(26, 3, '2018-12-10', 3, 4),
(27, 2, '2018-12-17', 5, 6),
(28, 1, '2018-12-17', 9, 9),
(29, 2, '2018-12-10', 5, 5),
(30, 1, '2018-12-10', 1, 3),
(31, 1, '2019-03-11', 13, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prodvacmedia`
--

CREATE TABLE `prodvacmedia` (
  `id_prodvacmedia` int(11) NOT NULL,
  `estado` int(1) NOT NULL,
  `fecha` date NOT NULL,
  `compraKG` int(30) NOT NULL,
  `compraUM` int(30) NOT NULL,
  `ventaKG` int(30) NOT NULL,
  `ventaUM` int(30) NOT NULL,
  `prodCorteKG` int(30) NOT NULL,
  `prodCorteUM` int(30) NOT NULL,
  `prodVariosKG` int(30) NOT NULL,
  `prodVariosUM` int(30) NOT NULL,
  `sobraKG` int(30) NOT NULL,
  `sobraUM` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `prodvacmedia`
--

INSERT INTO `prodvacmedia` (`id_prodvacmedia`, `estado`, `fecha`, `compraKG`, `compraUM`, `ventaKG`, `ventaUM`, `prodCorteKG`, `prodCorteUM`, `prodVariosKG`, `prodVariosUM`, `sobraKG`, `sobraUM`) VALUES
(30, 2, '2018-12-03', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(31, 2, '2018-12-03', 3, 3, 3, 3, 3, 3, 3, 3, 3, 3),
(32, 1, '2018-12-03', 1, 6, 1, 7, 2, 6, 2, 7, 6, 7),
(33, 2, '2018-12-10', 1, 6, 2, 7, 3, 8, 4, 9, 5, 45),
(34, 2, '2018-12-10', 1, 3, 6, 8, 2, 4, 7, 9, 5, 45),
(35, 2, '2018-12-10', 1, 3, 6, 8, 2, 4, 7, 9, 5, 45),
(36, 2, '2018-12-10', 1, 3, 6, 8, 2, 4, 7, 9, 5, 45),
(37, 2, '2018-12-10', 1, 3, 6, 8, 2, 4, 7, 9, 5, 45),
(38, 3, '2018-12-10', 1, 6, 2, 7, 3, 8, 4, 9, 5, 0),
(39, 2, '2018-12-17', 9, 4, 8, 32, 7, 2, 6, 1, 5, 0),
(40, 3, '2018-12-17', 5, 5, 5, 5, 5, 5, 5, 1, 5, 0),
(41, 1, '2018-12-10', 1, 6, 2, 7, 3, 8, 4, 9, 5, 0),
(42, 1, '2019-03-11', 1, 5, 2, 6, 3, 7, 4, 8, 9, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `saldo`
--

CREATE TABLE `saldo` (
  `id_saldo` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `ing_mayorista` int(11) DEFAULT NULL,
  `ing_minorista` int(11) DEFAULT NULL,
  `egreso` int(11) DEFAULT NULL,
  `retiros` int(11) DEFAULT NULL,
  `a_cobrar` int(11) DEFAULT NULL,
  `a_pagar` int(11) DEFAULT NULL,
  `saldo_final_real` int(11) DEFAULT NULL,
  `ing_ext` int(11) DEFAULT NULL,
  `egreso_ext` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `saldo`
--

INSERT INTO `saldo` (`id_saldo`, `fecha`, `ing_mayorista`, `ing_minorista`, `egreso`, `retiros`, `a_cobrar`, `a_pagar`, `saldo_final_real`, `ing_ext`, `egreso_ext`, `estado`) VALUES
(36, '2018-10-08', NULL, 23, 23123, NULL, NULL, 345, NULL, 0, 0, 1),
(37, '2018-10-01', 23, NULL, NULL, NULL, 23, NULL, NULL, 0, 0, 3),
(38, '2018-10-01', NULL, NULL, NULL, NULL, 23, NULL, NULL, 0, 0, 3),
(39, '2018-10-01', NULL, NULL, NULL, NULL, 23, NULL, NULL, 0, 0, 1),
(40, '2018-10-22', 23, NULL, NULL, NULL, 23, NULL, NULL, 0, 0, 1),
(41, '2019-04-01', NULL, 23, NULL, NULL, NULL, NULL, NULL, 23, NULL, 2),
(42, '2019-04-01', NULL, 23, NULL, NULL, NULL, NULL, NULL, 23, NULL, 2),
(43, '2019-04-01', NULL, 23, NULL, NULL, NULL, NULL, NULL, 44, NULL, 2),
(47, '2019-04-01', 2, 1, 4, 6, 7, 8, 9, 3, 5, 2),
(48, '2019-04-01', 2, 1, 4, 6, 7, 8, 9, 3, 5, 2),
(49, '2019-04-01', 2, 1, 4, 6, 7, 8, 9, 3, 23, 3),
(50, '2019-04-01', 2, 1, 4, 6, 7, 8, 9, 3, 23, 3),
(51, '2019-04-01', NULL, 1, 4, 6, 7, 8, 9, 3, 23, 3),
(52, '2019-04-01', NULL, 1, 4, 6, 7, 8, 9, 3, NULL, 3),
(53, '2019-04-01', NULL, 1, 4, 6, 7, 8, 9, NULL, NULL, 2),
(54, '2019-04-01', 2, 1, 4, 6, 7, 8, 9, 3, NULL, 2),
(55, '2019-04-01', 2, 1, 4, 6, 7, 8, 9, 3, 5, 2),
(56, '2019-04-01', 2, 1, 4, 6, 7, 8, 9, 3, 5, 2),
(57, '2019-04-01', 2, 1, 4, 6, 7, 8, 9, 3, 5, 2),
(58, '2019-04-01', 2, 1, 4, 6, 7, 8, 9, 3, 5, 2),
(59, '2019-04-01', 2, 1, 4, 6, 7, 8, 9, 3, 5, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_user` int(11) NOT NULL,
  `usuario` varchar(15) NOT NULL,
  `password` varchar(40) NOT NULL,
  `recordatorio` varchar(30) NOT NULL,
  `p_acc` tinyint(1) NOT NULL,
  `p_admin` tinyint(1) NOT NULL,
  `p_prod` tinyint(1) NOT NULL,
  `p_rrhh` tinyint(1) NOT NULL,
  `p_sdo` tinyint(1) NOT NULL,
  `p_ing_min` tinyint(1) NOT NULL,
  `p_ing_may` tinyint(1) NOT NULL,
  `p_egr` tinyint(1) NOT NULL,
  `p_ret` tinyint(1) NOT NULL,
  `p_acob` tinyint(1) NOT NULL,
  `p_apag` tinyint(1) NOT NULL,
  `p_sfr` tinyint(1) NOT NULL,
  `p_ing_ext` tinyint(1) NOT NULL,
  `p_egr_ext` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_user`, `usuario`, `password`, `recordatorio`, `p_acc`, `p_admin`, `p_prod`, `p_rrhh`, `p_sdo`, `p_ing_min`, `p_ing_may`, `p_egr`, `p_ret`, `p_acob`, `p_apag`, `p_sfr`, `p_ing_ext`, `p_egr_ext`) VALUES
(3, 'sdfsfd', '27ef11c24a1d336f46c69762b655a1495656820f', 'admin233', 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0),
(9, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(10, 'sadfsadftest', 'bf49c0872a43ea4c5aa49aac31709363a98e7105', 'sdfsadf', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(11, 'ola', '793f970c52ded1276b9264c742f19d1888cbaf73', 'ola', 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0),
(16, 'a', 'e9d71f5ee7c92d6dc9e92ffdad17b8bd49418f98', 'b', 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_password`);

--
-- Indices de la tabla `presentismo`
--
ALTER TABLE `presentismo`
  ADD PRIMARY KEY (`id_presentismo`);

--
-- Indices de la tabla `prodcercorte`
--
ALTER TABLE `prodcercorte`
  ADD PRIMARY KEY (`id_prodcercorte`);

--
-- Indices de la tabla `prodcermedia`
--
ALTER TABLE `prodcermedia`
  ADD PRIMARY KEY (`id_prodcermedia`);

--
-- Indices de la tabla `prodemb`
--
ALTER TABLE `prodemb`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `prodfiam`
--
ALTER TABLE `prodfiam`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `prodpoll`
--
ALTER TABLE `prodpoll`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `prodvaccorte`
--
ALTER TABLE `prodvaccorte`
  ADD PRIMARY KEY (`id_prodvaccorte`);

--
-- Indices de la tabla `prodvacmedia`
--
ALTER TABLE `prodvacmedia`
  ADD PRIMARY KEY (`id_prodvacmedia`);

--
-- Indices de la tabla `saldo`
--
ALTER TABLE `saldo`
  ADD PRIMARY KEY (`id_saldo`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `id_password` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `presentismo`
--
ALTER TABLE `presentismo`
  MODIFY `id_presentismo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT de la tabla `prodcercorte`
--
ALTER TABLE `prodcercorte`
  MODIFY `id_prodcercorte` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `prodcermedia`
--
ALTER TABLE `prodcermedia`
  MODIFY `id_prodcermedia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `prodemb`
--
ALTER TABLE `prodemb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `prodfiam`
--
ALTER TABLE `prodfiam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `prodpoll`
--
ALTER TABLE `prodpoll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `prodvaccorte`
--
ALTER TABLE `prodvaccorte`
  MODIFY `id_prodvaccorte` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT de la tabla `prodvacmedia`
--
ALTER TABLE `prodvacmedia`
  MODIFY `id_prodvacmedia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT de la tabla `saldo`
--
ALTER TABLE `saldo`
  MODIFY `id_saldo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
