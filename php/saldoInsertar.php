<?php

session_start();


include("sesion.php");try 
    {
                // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $llamado = $_POST['llamado'];
        /*Segun el id del input que paso en el ajax defino cual es el dato a traer de bdd, y el indice en la tabla de la bdd que se usa para preguntar si el dato no fue cargado (es null)*/
        switch ($llamado)
        {
            case "ingMinoristaInput":
                $datoATraer = 'ing_minorista';
                $indice = 3;
                break;
            case "ingMayoristaInput":
                $datoATraer = 'ing_mayorista';
                $indice = 2;
                break;
            case "ingExtraordinarioInput":
                $datoATraer = 'ing_ext';
                $indice = 9;
                break; 
            case "egresoInput":
                $datoATraer = 'egreso';
                $indice = 4;
                break; 
            case "egresoExtraordinarioInput":
                $datoATraer = 'egreso_ext';
                $indice = 10;
                break;
            case "retirosInput":
                $datoATraer = 'retiros';
                $indice = 5;
                break;
            case "aCobrarInput":
                $datoATraer = 'a_cobrar';
                $indice = 6;
                break;
            case "aPagarInput":
                $datoATraer = 'a_pagar';
                $indice = 7;
                break;
            case "saldoFinalRealInput":
                $datoATraer = 'saldo_final_real';
                $indice = 8;
                break;
        }
        // prepare sql and bind parameters
        $stmt = $conn->prepare("SELECT * FROM saldo WHERE fecha = :fecha and estado = 1");

        $stmt->bindParam(':fecha', $fecha);
        $fecha = $_POST['fecha'];
        $stmt->execute();
        $row = $stmt->fetch();
        /*Si el row count es 0 (el select no trajo nada) es porque no hay datos cargados en la fecha, por ende puedo insertar normalmente*/
        if ($stmt -> rowCount() == 0) 
        {
            $datoACargar = $_POST['valorACargar'];
            $stmt = $conn->prepare("INSERT INTO saldo ($datoATraer, fecha, estado) VALUES(:datoACargar, :fecha, 1)");
            $stmt->bindParam(':datoACargar', $datoACargar);
            $stmt->bindParam(':fecha', $fecha);
            $stmt->execute();
        }
        /*En caso de que si exista la fecha, pero el dato que queremos cargar sea nulo, se hace un update para agregar el nuevo dato*/
        else if ($row[$indice] == null)
        {
            $datoACargar = $_POST['valorACargar'];
            $stmt = $conn->prepare("UPDATE saldo
                SET $datoATraer = :datoACargar
                WHERE fecha = :fecha AND estado = 1");
            $stmt->bindParam(':fecha', $fecha);
            $stmt->bindParam(':datoACargar', $datoACargar);
            $stmt->execute();
        }
        echo json_encode("ok");
    }

catch(PDOException $e)
    {
        echo $e->getMessage() ;
    }
/*Las conexiones PDO se mantienen abiertas durante el ciclo de vida del objeto PDO*/
/*Asi se cierran los PDO*/
$conn = null;
?>