<?php

session_start();


include("sesion.php");try 
    {

        
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // prepare sql and bind parameters
        $stmt = $conn->prepare("INSERT INTO usuarios (usuario, password, recordatorio, p_acc, p_admin, p_prod, p_rrhh, p_sdo, p_ing_min, p_ing_may, p_egr, p_ret, p_acob, p_apag, p_sfr, p_ing_ext, p_egr_ext)
            VALUES (:usuario, :contrasenia, :recordatorio, 1, :p_admin,  :p_prod,
            :p_rrhh, :p_sdo, :p_ing_min, :p_ing_may, :p_egr, :p_ret, :p_acob, :p_apag, :p_sfr, :p_ing_ext, :p_egr_ext)");

        $stmt->bindParam(':usuario', $usuario);
        $stmt->bindParam(':contrasenia', $contrasenia);
        $stmt->bindParam(':recordatorio', $recordatorio);
        $stmt->bindParam(':p_admin', $p_admin);
        $stmt->bindParam(':p_prod', $p_prod);
        $stmt->bindParam(':p_rrhh', $p_rrhh);
        $stmt->bindParam(':p_sdo', $p_sdo);
        $stmt->bindParam(':p_ing_min', $p_ing_min);
        $stmt->bindParam(':p_ing_may', $p_ing_may);
        $stmt->bindParam(':p_egr', $p_egr);
        $stmt->bindParam(':p_ret', $p_ret);
        $stmt->bindParam(':p_acob', $p_acob);
        $stmt->bindParam(':p_apag', $p_apag);
        $stmt->bindParam(':p_sfr', $p_sfr);
        $stmt->bindParam(':p_ing_ext', $p_ing_ext);
        $stmt->bindParam(':p_egr_ext', $p_egr_ext);



        $usuario = $_POST['usuario'];
        $contrasenia = $_POST['contrasenia'];
        $recordatorio = $_POST['recordatorio'];
        $p_admin = $_POST['p_admin'];
        $p_prod = $_POST['p_prod'];
        $p_rrhh = $_POST['p_rrhh'];
        $p_sdo = $_POST['p_sdo'];
        $p_ing_min = $_POST['p_ing_min'];
        $p_ing_may = $_POST['p_ing_may'];
        $p_egr = $_POST['p_egr'];
        $p_ret = $_POST['p_ret'];
        $p_acob = $_POST['p_acob'];
        $p_apag = $_POST['p_apag'];
        $p_sfr = $_POST['p_sfr'];
        $p_ing_ext = $_POST['p_ing_ext'];
        $p_egr_ext = $_POST['p_egr_ext'];


        //$fecha = "2018-05-02";
        $stmt->execute();
        echo json_encode ("ok");
    }

catch(PDOException $e)
    {
        echo "error" ;
    }

$conn = null;


?>