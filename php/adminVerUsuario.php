<?php

session_start();

include("sesion.php");try 
    {
        
            $stmt = $conn->prepare("SELECT * FROM usuarios WHERE id_user = :id_user");

            $stmt->bindParam(':id_user', $id_user);

            $id_user = $_POST['id_user'];

            $stmt->execute();

            $row = $stmt->fetch();
            echo json_encode ($row);
    }

catch(PDOException $e)
    {
        echo $e->getMessage() ;
    }

/*Las conexiones PDO se mantienen abiertas durante el ciclo de vida del objeto PDO*/
/*Asi se cierran los PDO*/
$conn = null;
?>