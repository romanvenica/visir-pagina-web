<?php

session_start();

include("sesion.php");try 
    {

                // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $tabla = $_POST['tabla'];
        // prepare sql and bind parameters
        $stmt = $conn->prepare("INSERT INTO $tabla (fecha, compraKG, ventaKG, prodCorteKG, prodVariosKG, compraUM, ventaUM, prodCorteUM, prodVariosUM, sobraKG, sobraUM, estado) 
            VALUES (:fecha, :prodCompraKG, :prodVentaKG, :prodCorteKG, :prodVariosKG, :prodCompraUM, :prodVentaUM, :prodCorteUM, :prodVariosUM, :prodSobraKG, :prodSobraUM, 1)");

        $stmt->bindParam(':fecha', $fecha);
        $stmt->bindParam(':prodCompraKG', $compraKG);
        $stmt->bindParam(':prodVentaKG', $ventaKG);
        $stmt->bindParam(':prodCorteKG', $prodCorteKG);
        $stmt->bindParam(':prodVariosKG', $prodVariosKG);
        $stmt->bindParam(':prodCompraUM', $compraUM);
        $stmt->bindParam(':prodVentaUM', $ventaUM);
        $stmt->bindParam(':prodCorteUM', $prodCorteUM);
        $stmt->bindParam(':prodVariosUM', $prodVariosUM);
        $stmt->bindParam(':prodSobraKG', $sobraKG);
        $stmt->bindParam(':prodSobraUM', $sobraUM);

        $fecha = $_POST['fecha'];
        $compraKG = $_POST['compraKG'];
        $ventaKG = $_POST['ventaKG'];
        $prodCorteKG = $_POST['prodCorteKG'];
        $prodVariosKG = $_POST['prodVariosKG'];
        $compraUM = $_POST['compraUM'];
        $ventaUM = $_POST['ventaUM'];
        $prodCorteUM = $_POST['prodCorteUM'];
        $prodVariosUM = $_POST['prodVariosUM'];
        $sobraKG = $_POST['sobraKG'];
        $sobraUM = $_POST['sobraUM'];


        $stmt->execute();
        echo json_encode ("ok");
    }
catch(PDOException $e)
    {
        echo $e ;
    }

$conn = null;
?>