<?php

session_start();

include("sesion.php");

try 
    {

        
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $llamado = $_POST['llamado'];
        /*Setea el dato a traer dependiendo de que boton llamo al ajax*/
        switch ($llamado)
        {
            case "ingMinoristaInput":
                $datoATraer = 'ing_minorista';
                break;
            case "ingMayoristaInput":
                $datoATraer = 'ing_mayorista';
                break;
            case "ingExtraordinarioInput":
                $datoATraer = 'ing_ext';
                break;
            case "egresoInput":
                $datoATraer = 'egreso';
                break;
            case "egresoExtraordinarioInput":
                $datoATraer = 'egreso_ext';
                break;
            case "retirosInput":
                $datoATraer = 'retiros';
                break;
            case "aCobrarInput":
                $datoATraer = 'a_cobrar';
                break;
            case "aPagarInput":
                $datoATraer = 'a_pagar';
                break;
            case "saldoFinalRealInput":
                $datoATraer = 'saldo_final_real';
                break;
        }

        // prepare sql and bind parameters
        $stmt = $conn->prepare("SELECT $datoATraer FROM saldo WHERE fecha = :fecha and estado = 1");

        $stmt->bindParam(':fecha', $fecha);

        $fecha = $_POST['fecha'];

        $stmt->execute();
        $row = $stmt->fetch();
        /*Retorna true en caso de que la consulta no devuelva nada (no hay fecha cargada) o que el dato que queremos cargar sea nulo en esa fecha*/
        if ($stmt -> rowCount() == 0 || $row[0] == null) {
            echo json_encode(true);
        }
        /*Retorna false si la fecha ya tiene ese dato cargado
        (en ese caso habria que modificar, no cargar el dato nuevo)*/
        else
        {
            echo json_encode(false);
        }
    }

catch(PDOException $e)
    {
        echo $e->getMessage() ;
    }

/*Las conexiones PDO se mantienen abiertas durante el ciclo de vida del objeto PDO*/
/*Asi se cierran los PDO*/
$conn = null;


?>