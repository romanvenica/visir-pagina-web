<?php

session_start();

include("sesion.php");try 
    {

                // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // prepare sql and bind parameters
        $stmt = $conn->prepare("UPDATE prodpoll
            SET estado = 2
            WHERE fecha = :fecha AND estado = 1
            ;INSERT INTO prodpoll (fecha, compra, venta, prodCorte, sobra, estado) 
            VALUES (:fecha, :compra, :venta, :prodCorte, :sobra, 1)");

        $stmt->bindParam(':fecha', $fecha);
        $stmt->bindParam(':compra', $compra);
        $stmt->bindParam(':venta', $venta);
        $stmt->bindParam(':sobra', $sobra);
        $stmt->bindParam(':prodCorte', $prodCorte);


        $fecha = $_POST['fecha'];
        $compra = $_POST['compra'];
        $venta = $_POST['venta'];
        $sobra = $_POST['sobra'];
        $prodCorte = $_POST['prodCorte'];


        $stmt->execute();
        echo json_encode ("ok");
    }
catch(PDOException $e)
    {
        echo $e ;
    }

$conn = null;
?>