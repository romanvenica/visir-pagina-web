<?php

session_start();


include("sesion.php");try 
    {

        
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // prepare sql and bind parameters
        $stmt = $conn->prepare("SELECT id_password FROM admin WHERE password = :password");

        $stmt->bindParam(':password', $password);

        $password = $_POST['password'];

        $stmt->execute();

        
        //$fechaResult  = $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $row = $stmt->fetch();
        echo json_encode ($row);
    }

catch(PDOException $e)
    {
        echo $e->getMessage() ;
    }


/*Las conexiones PDO se mantienen abiertas durante el ciclo de vida del objeto PDO*/
/*Asi se cierran los PDO*/
$conn = null;

?>
    
    
