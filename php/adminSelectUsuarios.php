<?php

session_start();

include("sesion.php");try 
    {
        
            $stmt = $conn->prepare("SELECT id_user, usuario, recordatorio FROM usuarios");

            $stmt->execute();

            $row = $stmt->fetchAll();
            echo json_encode ($row);
    }

catch(PDOException $e)
    {
        echo $e->getMessage() ;
    }

/*Las conexiones PDO se mantienen abiertas durante el ciclo de vida del objeto PDO*/
/*Asi se cierran los PDO*/
$conn = null;
?>