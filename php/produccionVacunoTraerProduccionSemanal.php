<?php

session_start();

include("sesion.php");try
    {

        
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Preguntar por contrasenia.
        $stmt = $conn->prepare("SELECT id_password FROM admin WHERE password = :password");

        $stmt->bindParam(':password', $password);

        $password = $_POST['password'];

        $stmt->execute();

        // Si la contrasenia es incorrecta
        if ($stmt -> rowCount() == 0) {
            throw new Exception($error);
        }

        // Si es correcta
        else
        {
             $tabla = $_POST['tabla'];
             $tipo = $_POST['tipo'];

             if ($tabla == 'prodvacmedia') 
             {
                $stmt = $conn->prepare("SELECT compraKG, compraUM, ventaKG, ventaUM, prodCorteKG, prodCorteUM, prodVariosKG, prodVariosUM, sobraKG, sobraUM FROM $tabla WHERE fecha = :fecha AND estado = 1");
             }
             else if ($tabla == 'prodvaccorte')
             {
                $stmt = $conn->prepare("SELECT compraCorte, sobraCorte FROM $tabla WHERE fecha = :fecha AND estado = 1");
             }
             else if ($tabla == 'prodcermedia')
             {
                 $stmt = $conn->prepare("SELECT compraKG, compraUM, ventaKG, ventaUM, prodCorteKG, prodCorteUM, sobraKG, sobraUM FROM $tabla WHERE fecha = :fecha AND estado = 1");
             }
             else if ($tabla == 'prodcercorte')
             {
                 $stmt = $conn->prepare("SELECT compraKG, sobraKG FROM $tabla WHERE fecha = :fecha AND estado = 1");
             }
            else if ($tabla == 'prodpoll')
             {
                 $stmt = $conn->prepare("SELECT compra, venta, prodCorte, sobra FROM $tabla WHERE fecha = :fecha AND estado = 1");
             }
             else if ($tabla == 'prodfiam')
             {
                 $stmt = $conn->prepare("SELECT produccion, sobra FROM $tabla WHERE fecha = :fecha AND estado = 1");
             }
             else if ($tabla == 'prodemb')
             {
                 $stmt = $conn->prepare("SELECT produccion, sobra FROM $tabla WHERE fecha = :fecha AND estado = 1");
             }

             
             $stmt->bindParam(':fecha', $fecha);
             $fecha = $_POST['fecha'];


             $stmt->execute();

        
            //$fechaResult  = $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $row = $stmt->fetch();
            echo json_encode ($row);
        }
    }

catch(PDOException $e)
    {
        echo $e->getMessage() ;
    }

/*Las conexiones PDO se mantienen abiertas durante el ciclo de vida del objeto PDO*/
/*Asi se cierran los PDO*/
$conn = null;


?>