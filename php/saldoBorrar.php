<?php
session_start();
include("sesion.php");try 
    {
                // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $llamado = $_POST['llamado'];
        /*Segun el id del input que paso en el ajax defino cual es el dato a traer de bdd, y el indice en la tabla de la bdd que se usa para preguntar si el dato no fue cargado (es null)*/
        switch ($llamado)
        {
            case "ingMinoristaInput":
                $datoABorrar = 'ing_minorista';
                $indice = 3;
                break;
            case "ingMayoristaInput":
                $datoABorrar = 'ing_mayorista';
                $indice = 2;
                break;
            case "ingExtraordinarioInput":
                $datoABorrar = 'ing_ext';
                $indice = 9;
                break;
            case "egresoInput":
                $datoABorrar = 'egreso';
                $indice = 4;
                break;
            case "egresoExtraordinarioInput":
                $datoABorrar = 'egreso_ext';
                $indice = 10;
                break;
            case "retirosInput":
                $datoABorrar = 'retiros';
                $indice = 5;
                break;
            case "aCobrarInput":
                $datoABorrar = 'a_cobrar';
                $indice = 6;
                break;
            case "aPagarInput":
                $datoABorrar = 'a_pagar';
                $indice = 7;
                break;
            case "saldoFinalRealInput":
                $datoABorrar = 'saldo_final_real';
                $indice = 8;
                break;
        }
        // prepare sql and bind parameters
        $stmt = $conn->prepare("SELECT * FROM saldo WHERE fecha = :fecha and estado = 1");
        $stmt->bindParam(':fecha', $fecha);
        $fecha = $_POST['fecha'];
        $stmt->execute();
        $row = $stmt->fetch();
        /*Si el row count es 0 (el select no trajo nada) es porque no hay datos cargados en la fecha, por ende puedo insertar normalmente*/
        $stmt = $conn->prepare("UPDATE saldo
        SET estado = 3
        WHERE fecha = :fecha AND estado = 1");
        $stmt->bindParam(':fecha', $fecha);
        $stmt->execute();
        $stmt = $conn->prepare("INSERT INTO saldo VALUES(0, :fecha, :row2, :row3, :row4, :row5, :row6, :row7, :row8, :row9, :row10, 1 )");

        $row2 = $row[2];
        $row3 = $row[3];
        $row4 = $row[4];
        $row5 = $row[5];
        $row6 = $row[6];
        $row7 = $row[7];
        $row8 = $row[8];
        $row9 = $row[9];
        $row10 = $row[10];

        $stmt->bindParam(':row2', $row2);
        $stmt->bindParam(':row3', $row3);
        $stmt->bindParam(':row4', $row4);
        $stmt->bindParam(':row5', $row5);
        $stmt->bindParam(':row6', $row6);
        $stmt->bindParam(':row7', $row7);
        $stmt->bindParam(':row8', $row8);
        $stmt->bindParam(':row9', $row9);
        $stmt->bindParam(':row10', $row10);
        $stmt->bindParam(':fecha', $fecha);
        $stmt->execute();

        $stmt = $conn->prepare("UPDATE saldo
            SET $datoABorrar = null
            WHERE fecha = :fecha AND estado = 1");
        $stmt->bindParam(':fecha', $fecha);
        $stmt->execute();
        echo json_encode("ok");
    }
catch(PDOException $e)
    {
        echo $e->getMessage();
    }
/*Las conexiones PDO se mantienen abiertas durante el ciclo de vida del objeto PDO*/
/*Asi se cierran los PDO*/
$conn = null;
?>