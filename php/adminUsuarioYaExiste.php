<?php

session_start();

include("sesion.php");try 
    {
        
            $stmt = $conn->prepare("SELECT usuario FROM usuarios
                WHERE usuario = :usuario");

            $stmt->bindParam(':usuario', $usuario);
            $usuario = $_POST['usuario'];

            $stmt->execute();

            $row = $stmt->fetch();
            if ($row[0] == $usuario) 
            {
            echo encode(false);
            }
            else
            {
            echo json_encode(true);
            }
    }

catch(PDOException $e)
    {
        echo $e->getMessage() ;
    }

/*Las conexiones PDO se mantienen abiertas durante el ciclo de vida del objeto PDO*/
/*Asi se cierran los PDO*/
$conn = null;
?>