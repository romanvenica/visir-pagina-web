<?php

session_start();

include("sesion.php");try 
    {

        
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Preguntar por contrasenia.
        $stmt = $conn->prepare("SELECT id_password FROM admin WHERE password = :password");

        $stmt->bindParam(':password', $password);

        $password = $_POST['password'];

        $stmt->execute();

        // Si la contrasenia es incorrecta
        if ($stmt -> rowCount() == 0) {
            throw new Exception($error);
        }

        // Si es correcta
        else
        {
            $stmt = $conn->prepare("SELECT asist_esperada, asist_real FROM presentismo WHERE fecha = :fecha and estado = 1");

            $stmt->bindParam(':fecha', $fecha);

            $fecha = $_POST['fecha'];

            $stmt->execute();

            $row = $stmt->fetch();
            echo json_encode ($row);
        }
    }

catch(PDOException $e)
    {
        echo $e->getMessage() ;
    }

/*Las conexiones PDO se mantienen abiertas durante el ciclo de vida del objeto PDO*/
/*Asi se cierran los PDO*/
$conn = null;


?>