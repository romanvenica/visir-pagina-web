<?php

session_start();

include("sesion.php");try 
    {

        
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Preguntar por contrasenia.
        $stmt = $conn->prepare("SELECT id_password FROM admin WHERE password = :password");

        $stmt->bindParam(':password', $password);

        $password = $_POST['password'];

        $stmt->execute();

        // Si la contrasenia es incorrecta
        if ($stmt -> rowCount() == 0) {
            throw new Exception($error);
        }

        // Si es correcta
        else
        {
            $llamado = $_POST['llamado'];
            /*Segun el id del input que paso en el ajax defino cual es el dato a traer de bdd, y el indice en la tabla de la bdd que se usa para preguntar si el dato no fue cargado (es null)*/
            switch ($llamado)
            {
                case "ingMinoristaInput":
                    $datoATraer = 'ing_minorista';
                    $indice = 3;
                    break;
                case "ingMayoristaInput":
                    $datoATraer = 'ing_mayorista';
                    $indice = 2;
                    break;
                case "ingExtraordinarioInput":
                    $datoATraer = 'ing_ext';
                    $indice = 9;
                    break;
                case "egresoInput":
                    $datoATraer = 'egreso';
                    $indice = 4;
                    break;
                case "egresoExtraordinarioInput":
                    $datoATraer = 'egreso_ext';
                    $indice = 10;
                    break;
                case "retirosInput":
                    $datoATraer = 'retiros';
                    $indice = 5;
                    break;
                case "aCobrarInput":
                    $datoATraer = 'a_cobrar';
                    $indice = 6;
                    break;
                case "aPagarInput":
                    $datoATraer = 'a_pagar';
                    $indice = 7;
                    break;
                case "saldoFinalRealInput":
                    $datoATraer = 'saldo_final_real';
                    $indice = 8;
                    break;
            }
            $stmt = $conn->prepare("SELECT * FROM saldo WHERE fecha = :fecha and estado = 1");

            $stmt->bindParam(':fecha', $fecha);
            $fecha = $_POST['fecha'];
            $stmt->execute();
            $row = $stmt->fetch();

            // Si no hay datos
            if ($stmt -> rowCount() == 0) 
            {
                throw new Exception($error);
            }
            else if ($row[$indice] == null)
            {
                throw new Exception($error);
            }
            else
            {
                echo json_encode ($row[$indice]);
            }

        }
    }

catch(PDOException $e)
    {
        echo $e->getMessage() ;
    }

/*Las conexiones PDO se mantienen abiertas durante el ciclo de vida del objeto PDO*/
/*Asi se cierran los PDO*/
$conn = null;


?>