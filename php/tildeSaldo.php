<?php

session_start();


include("sesion.php");try 
    {
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $fecha = $_POST['fecha'];
        $id_user = $_POST['id_user'];

        // Primero pide todos los permisos
        $stmt = $conn->prepare("SELECT p_ing_min, p_ing_may, p_egr, p_ret, p_acob, p_apag, p_sfr, p_ing_ext, p_egr_ext
            FROM usuarios
            WHERE id_user = :id_user");

        $stmt->bindParam(':id_user', $id_user);
        $stmt->execute();
        $rowPermisos = $stmt->fetch();

        // Aca busca todos los datos
        $stmt = $conn->prepare("SELECT ing_minorista, ing_mayorista, ing_ext, egreso, egreso_ext, retiros, a_cobrar, a_pagar, saldo_final_real
            FROM saldo
            WHERE fecha = :fecha AND estado = 1");

        $stmt->bindParam(':fecha', $fecha);
        $stmt->execute();
        $rowTildes = $stmt->fetch();
        $resultado = array();


        for ($i=0; $i < (sizeof($rowTildes)/2); $i++)
        { 
            if ($rowPermisos[$i] == 1) {
                if ($rowTildes[$i] != null)
                {
                   array_push($resultado, true); 
                }
                else if ($rowTildes[$i] == null)
                {
                    array_push($resultado, false);
                }
            }
        }
        echo json_encode($resultado);

    }

catch(PDOException $e)
    {
        echo "error" ;
    }
$conn = null;
?>