<?php

session_start();


include("sesion.php");try 
    {
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $fecha = $_POST['fecha'];
        $listaTildes = array();

        $stmt = $conn->prepare("SELECT * FROM prodvacmedia
            WHERE fecha = :fecha AND estado = 1");
        $stmt->bindParam(':fecha', $fecha);
        $stmt->execute();
        
        $row = $stmt->fetch();
        if ($row == false) {
            array_push($listaTildes, 0);
        }
        else
        {
            array_push($listaTildes, 1);
        }

        $stmt = $conn->prepare("SELECT * FROM prodvaccorte
            WHERE fecha = :fecha AND estado = 1");
        $stmt->bindParam(':fecha', $fecha);
        $stmt->execute();
        
        $row = $stmt->fetch();
        if ($row == false) {
            array_push($listaTildes, 0);
        }
        else
        {
            array_push($listaTildes, 1);
        }

        $stmt = $conn->prepare("SELECT * FROM prodcermedia
            WHERE fecha = :fecha AND estado = 1");
        $stmt->bindParam(':fecha', $fecha);
        $stmt->execute();
        
        $row = $stmt->fetch();
        if ($row == false) {
            array_push($listaTildes, 0);
        }
        else
        {
            array_push($listaTildes, 1);
        }

        $stmt = $conn->prepare("SELECT * FROM prodcercorte
            WHERE fecha = :fecha AND estado = 1");
        $stmt->bindParam(':fecha', $fecha);
        $stmt->execute();
        
        $row = $stmt->fetch();
        if ($row == false) {
            array_push($listaTildes, 0);
        }
        else
        {
            array_push($listaTildes, 1);
        }

        $stmt = $conn->prepare("SELECT * FROM prodpoll
            WHERE fecha = :fecha AND estado = 1");
        $stmt->bindParam(':fecha', $fecha);
        $stmt->execute();
        
        $row = $stmt->fetch();
        if ($row == false) {
            array_push($listaTildes, 0);
        }
        else
        {
            array_push($listaTildes, 1);
        }

        $stmt = $conn->prepare("SELECT * FROM prodemb
            WHERE fecha = :fecha AND estado = 1");
        $stmt->bindParam(':fecha', $fecha);
        $stmt->execute();
        
        $row = $stmt->fetch();
        if ($row == false) {
            array_push($listaTildes, 0);
        }
        else
        {
            array_push($listaTildes, 1);
        }

        $stmt = $conn->prepare("SELECT * FROM prodfiam
            WHERE fecha = :fecha AND estado = 1");
        $stmt->bindParam(':fecha', $fecha);
        $stmt->execute();
        
        $row = $stmt->fetch();
        if ($row == false) {
            array_push($listaTildes, 0);
        }
        else
        {
            array_push($listaTildes, 1);
        }

        echo json_encode($listaTildes);



    }

catch(PDOException $e)
    {
        echo $e ;
    }
$conn = null;
?>