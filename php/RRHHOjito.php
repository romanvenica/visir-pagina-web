<?php

session_start();

include("sesion.php");try 
    {

                // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // prepare sql and bind parameters
        $stmt = $conn->prepare("SELECT * FROM presentismo
            WHERE MONTH(fecha) = :mes AND YEAR(fecha) = :anio
            ORDER BY fecha ASC");

        $stmt->bindParam(':mes', $mes);
        $stmt->bindParam(':anio', $anio);

        $mes = $_POST['mes'];
        $anio = $_POST['anio'];

        //$fecha = "2018-05-02";
        $stmt->execute();

        $row = $stmt->fetchAll();
        echo json_encode ($row);
    }

catch(PDOException $e)
    {
        echo "error" ;
    }

$conn = null;


?>