<?php

session_start();

/*
$$servername = "localhost";
$$username = "root";
$$password = "";
$$dbname = "visir";
*/

include("sesion.php");

try 
    {

        
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // prepare sql and bind parameters
        $stmt = $conn->prepare("DELETE FROM usuarios 
            WHERE id_user = :id_user");

        $stmt->bindParam(':id_user', $id_user);
        $id_user = $_POST['id_user'];

        $stmt->execute();
        echo json_encode("ok");
    }

catch(PDOException $e)
    {
        echo "error" ;
    }

$conn = null;


?>