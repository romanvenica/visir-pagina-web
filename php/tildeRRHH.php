<?php

session_start();


include("sesion.php");try 
    {
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $fecha = $_POST['fecha'];

        $stmt = $conn->prepare("SELECT * FROM presentismo
            WHERE fecha = :fecha AND estado = 1");
        $stmt->bindParam(':fecha', $fecha);
        $stmt->execute();
        
        $row = $stmt->fetch();
        if ($row == false) {
            echo json_encode(false);
        }
        else
        {
            echo json_encode (true);
        }
    }

catch(PDOException $e)
    {
        echo "error" ;
    }
$conn = null;
?>