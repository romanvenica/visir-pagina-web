<?php

session_start();

include("sesion.php");try 
    {
        
            $stmt = $conn->prepare("SELECT usuario, id_user FROM usuarios
                WHERE usuario = :usuario");

            $stmt->bindParam(':usuario', $usuario);

            $usuario = $_POST['usuario'];
            $id_user = $_POST['id_user'];


            $stmt->execute();

            $row = $stmt->fetch();


            if ($row[0] == $usuario && $row[1] == $id_user) 
            {
            echo json_encode($row);
            }
            else if ($row[0] != $usuario)
            {
            echo json_encode($row);
            }
            else
            {
            echo (false);
            }
    }

catch(PDOException $e)
    {
        echo $e->getMessage() ;
    }

/*Las conexiones PDO se mantienen abiertas durante el ciclo de vida del objeto PDO*/
/*Asi se cierran los PDO*/
$conn = null;
?>