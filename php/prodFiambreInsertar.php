<?php

session_start();

include("sesion.php");

try 
    {

                // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // prepare sql and bind parameters
        $stmt = $conn->prepare("INSERT INTO prodfiam (fecha, produccion, sobra, estado) 
            VALUES (:fecha, :produccion, :sobra, 1)");

        $stmt->bindParam(':fecha', $fecha);
        $stmt->bindParam(':produccion', $produccion);
        $stmt->bindParam(':sobra', $sobra);


        $fecha = $_POST['fecha'];
        $produccion = $_POST['produccion'];
        $sobra = $_POST['sobra'];

        $stmt->execute();
        echo json_encode ("ok");
    }
catch(PDOException $e)
    {
        echo $e ;
    }

$conn = null;
?>