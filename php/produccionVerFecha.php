<?php

session_start();

include("sesion.php");try 
    {

        
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $tipoDeCarga = $_POST['tipoDeCarga'];

        /*Depende que clase de carga llamo a la funcion se fija si existe la fecha en la BDD*/
        switch ($tipoDeCarga)
        {
            case "vacuno":
                $datoATraer = 'prodvac';
                break;
            case "pollo":
                $datoATraer = 'prodpoll';
                break;
            case "cerdo":
                $datoATraer = 'prodcer';
                break;
            case "embutido":
                $datoATraer = 'prodemb';
                break;
            case "fiambre":
                $datoATraer = 'prodfiam';
                break;
        }

        // prepare sql and bind parameters
        $stmt = $conn->prepare("SELECT fecha FROM $datoATraer WHERE fecha = :fecha AND estado = 1");

        $stmt->bindParam(':fecha', $fecha);

        $fecha = $_POST['fecha'];

        $stmt->execute();

        // Esto no se que onda.
        //$fechaResult  = $stmt->setFetchMode(PDO::FETCH_ASSOC);
        
        // fetchAll para que traiga muchos datos :v
        $row = $stmt->fetch();
        echo json_encode ($row);
    }

catch(PDOException $e)
    {
        echo ($e);
    }

/*Las conexiones PDO se mantienen abiertas durante el ciclo de vida del objeto PDO*/
/*Asi se cierran los PDO*/
$conn = null;


?>