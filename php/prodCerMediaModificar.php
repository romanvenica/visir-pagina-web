<?php

session_start();

include("sesion.php");try 
    {

                // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // prepare sql and bind parameters
        $stmt = $conn->prepare("UPDATE prodcermedia
            SET estado = 2
            WHERE fecha = :fecha AND estado = 1
            ;INSERT INTO prodcermedia (fecha, compraKG, ventaKG, prodCorteKG, compraUM, ventaUM, prodCorteUM, sobraKG, sobraUM, estado) 
            VALUES (:fecha, :prodCompraKG, :prodVentaKG, :prodCorteKG, :compraUM, :ventaUM, :prodCorteUM, :sobraKG, :sobraUM, 1)");

        $stmt->bindParam(':fecha', $fecha);
        $stmt->bindParam(':prodCompraKG', $compraKG);
        $stmt->bindParam(':prodCorteKG', $prodCorteKG);
        $stmt->bindParam(':compraUM', $compraUM);
        $stmt->bindParam(':ventaUM', $ventaUM);
        $stmt->bindParam(':prodCorteUM', $prodCorteUM);
        $stmt->bindParam(':sobraKG', $sobraKG);
        $stmt->bindParam(':sobraUM', $sobraUM);
        $stmt->bindParam(':prodVentaKG', $ventaKG);

        $fecha = $_POST['fecha'];
        $compraKG = $_POST['compraKG'];
        $prodCorteKG = $_POST['prodCorteKG'];
        $compraUM = $_POST['compraUM'];
        $ventaUM = $_POST['ventaUM'];
        $prodCorteUM = $_POST['prodCorteUM'];
        $sobraKG = $_POST['sobraKG'];
        $sobraUM = $_POST['sobraUM'];
        $ventaKG = $_POST['ventaKG'];

        $stmt->execute();
        echo json_encode ("ok");
    }
catch(PDOException $e)
    {
        echo $e ;
    }

$conn = null;
?>
    