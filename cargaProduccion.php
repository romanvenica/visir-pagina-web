<?php
session_start();
$p_prod = $_SESSION["p_prod"];
if ($p_prod != 1)
{
  header('Location: index.php');
  die();
} 
?>


<!DOCTYPE html>
<html>
<head>
  <link rel="icon" href="resources/pyramid.png">
  <script src="js/jquery-3.2.1.js"></script>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="css/estilos.css">
  <script type="text/javascript" src="js/sha.js"></script>
  <script type="text/javascript" src="js/bootstrap.js"></script>
  <title>Carga de la produccion</title>
</head>
<body>

  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="bienvenido.php">Visir</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
          
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav pull-right">

            <?php 

            if ($_SESSION["p_admin"] == 1) 
            {
              echo
              "
              <li>
              <a href='admin.php'>Admin</a>
              </li>
              "
              ;
            }

            if ($_SESSION["p_prod"] == 1) 
            {
              echo
              "
              <li>
              <a href='cargaProduccion.php'>Produccion</a>
              </li>
              "
              ;
            }

            if ($_SESSION["p_sdo"] == 1) 
            {
              echo
              "
              <li>
              <a href='cargaDatos.php'>Saldo</a>
              </li>
              "
              ;
            }
            
            

            if ($_SESSION["p_rrhh"] == 1) 
            {
              echo
              "
              <li>
              <a href='cargaRRHH.php'>RRHH</a>
              </li>
              "
              ;
            }

            ?>
            <a href="logout.php">
              <button class="btn btn-danger navbar-btn" class="active">Cerrar Sesion</button>
            </a>

          </ul>
        </div>
      </div>
  </nav>


  <div class="container-fluid col-lg-6 col-lg-offset-3">
    <div class="row">
        <div class="panel panel-primary filterable">

            <div class="panel-heading">
                <h5 class="panel-title ">
                  PRODUCCION
                </h5>
            </div>

            <div class="panel-body prodDivBody">

                <!-- El div de la fecha -->
                <div class="prodDivFechaExt">
                    <div class="prodDivFechaInt">
                          <div class="prodDivFechaInt2">
                              <span class="prodTextoFecha">
                                Fecha:
                              </span>

                              <input type="date" class="prodFecha" id="produccionFecha">
                              <span id="prodSpanFechaIncorrecta" class="spanFechaIncorrecta oculto">
                                *Seleccione un dia lunes.
                              </span>

                              <span id="prodSpanCamposIncorrectos" class="spanCamposIncorrectos oculto">
                                *Ingrese numeros enteros.
                              </span>
                          </div>
                    </div>
                </div>

                <!-- ############################## CONTENEDOR PRINCIPAL ############################## -->
                <div class="prodDivDatos">








                  
                  <!-- ############################## TARJETA CARGA DATOS VACUNO ############################## -->

                  <div class="cargaDatos">

                    <div class="tituloPrimario">
                      MEDIA RES ENTERA
                      <object type="image/svg+xml" id="" data="resources/check.svg" class="prodCheck oculto svgIcon"></object>
                      <object type="image/svg+xml" id="" data="resources/cancel.svg" class="prodCancel oculto svgIcon"></object>
                    </div>

                    <div class="cuerpoPrimario">

                      <div class="cuerpoCampos">

                        <div class="campoTitulos">
                          <div class="campoTitulo">COMPRA</div>
                          <div class="campoTitulo">VENTA</div>
                          <div class="campoTitulo">PROD. CORTE</div>
                          <div class="campoTitulo">PROD. VARIOS</div>
                          <div class="campoTitulo">SOBRA</div>
                        </div>

                        <div class="campoEntradas">
                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoKG">KG:</span><br>
                              <input type="text" class="inputKG inputValidar vacmedia"><br>
                              <span class="textoUM">UM:</span><br>
                              <input type="text" class="inputUM inputValidar vacmedia">
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoKG">KG:</span><br>
                              <input type="text" class="inputKG inputValidar vacmedia"><br>
                              <span class="textoUM">UM:</span><br>
                              <input type="text" class="inputUM inputValidar vacmedia">
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoKG">KG:</span><br>
                              <input type="text" class="inputKG inputValidar vacmedia"><br>
                              <span class="textoUM">UM:</span><br>
                              <input type="text" class="inputUM inputValidar vacmedia">
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoKG">KG:</span><br>
                              <input type="text" class="inputKG inputValidar vacmedia"><br>
                              <span class="textoUM">UM:</span><br>
                              <input type="text" class="inputUM inputValidar vacmedia">
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoKG">KG:</span><br>
                              <input type="text" class="inputKG inputValidar vacmedia"><br>
                              <span class="textoUM">UM:</span><br>
                              <input type="text" class="inputUM inputValidar vacmedia">
                            </div>
                          </div>

                        </div>
                      </div>

                      <div class="cuerpoBotones">
                        <button type="button" class="btn btn-primary botonProd" onclick="validarCargarProduccion('vacmedia')">Cargar</button>
                        <button type="button" class="btn btn-warning botonProd" onclick="validarModificarProduccion('vacmedia')">Modificar</button>
                        <button type="button" class="btn btn-danger botonProd" onclick="validarBorrarProduccion('vacmedia')">Eliminar</button>
                      </div>

                    </div>


                    <div class="tituloSecundario  ">
                      RES CORTES
                      <object type="image/svg+xml" id="" data="resources/check.svg" class="prodCheck oculto svgIcon"></object>
                      <object type="image/svg+xml" id="" data="resources/cancel.svg" class="prodCancel oculto svgIcon"></object>
                    </div>

                    <div class="cuerpoSecundario">
                      <div class="cuerpoCampos">
                        <div class="campoTitulos">
                          <div class="campoTitulo">COMPRA</div>
                          <div class="campoTitulo">SOBRA</div>
                          <div class="campoTitulo"></div>
                        </div>

                        <div class="campoEntradas">

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoKG">KG:</span><br>
                              <input type="text" class="inputKG inputValidar vaccorte">
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoKG">KG:</span><br>
                              <input type="text" class="inputKG inputValidar vaccorte">
                            </div>
                          </div>

                          <div class="campoEntrada"></div>
                          <div class="campoEntrada"></div>
                          <div class="campoEntrada"></div>
                        </div>
                      </div>

                      <div class="cuerpoBotones">
                        <button type="button" class="btn btn-primary botonProd" onclick="validarCargarProduccion('vaccorte')">Cargar</button>
                        <button type="button" class="btn btn-warning botonProd" onclick="validarModificarProduccion('vaccorte')">Modificar</button>
                        <button type="button" class="btn btn-danger botonProd" onclick="validarBorrarProduccion('vaccorte')">Eliminar</button>
                      </div>
                    </div>


                  </div>

                  <!-- ############################## TARJETA CARGA DATOS VACUNO - FIN ############################## -->







                  <!-- ############################## TARJETA CARGA DATOS CERDO ############################## -->


                  <div class="cargaDatos">

                    <div class="tituloPrimario">
                      CERDO MEDIAS
                      <object type="image/svg+xml" id="" data="resources/check.svg" class="prodCheck oculto svgIcon"></object>
                      <object type="image/svg+xml" id="" data="resources/cancel.svg" class="prodCancel oculto svgIcon"></object>
                    </div>

                    <div class="cuerpoPrimario">

                      <div class="cuerpoCampos">
                        
                        <div class="campoTitulos">
                          <div class="campoTitulo">COMPRA</div>
                          <div class="campoTitulo">VENTA</div>
                          <div class="campoTitulo">PROD. CORTE</div>
                          <div class="campoTitulo">SOBRA</div>
                          <div class="campoTitulo"></div>
                        </div>

                        <div class="campoEntradas">
                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoKG">KG:</span><br>
                              <input type="text" class="inputKG inputValidar cermedia"><br>
                              <span class="textoUM">UM:</span><br>
                              <input type="text" class="inputUM inputValidar cermedia">
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoKG">KG:</span><br>
                              <input type="text" class="inputKG inputValidar cermedia"><br>
                              <span class="textoUM">UM:</span><br>
                              <input type="text" class="inputUM inputValidar cermedia">
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoKG">KG:</span><br>
                              <input type="text" class="inputKG inputValidar cermedia"><br>
                              <span class="textoUM">UM:</span><br>
                              <input type="text" class="inputUM inputValidar cermedia">
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoKG">KG:</span><br>
                              <input type="text" class="inputKG inputValidar cermedia"><br>
                              <span class="textoUM">UM:</span><br>
                              <input type="text" class="inputUM inputValidar cermedia">
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                            </div>
                          </div>

                        </div>
                      </div>

                      <div class="cuerpoBotones">
                        <button type="button" class="btn btn-primary botonProd" onclick="validarCargarProduccion('cermedia')">Cargar</button>
                        <button type="button" class="btn btn-warning botonProd" onclick="validarModificarProduccion('cermedia')">Modificar</button>
                        <button type="button" class="btn btn-danger botonProd" onclick="validarBorrarProduccion('cermedia')">Eliminar</button>
                      </div>

                    </div>

                    <div class="tituloSecundario  ">
                      CERDO CORTES
                      <object type="image/svg+xml" id="" data="resources/check.svg" class="prodCheck oculto svgIcon"></object>
                      <object type="image/svg+xml" id="" data="resources/cancel.svg" class="prodCancel oculto svgIcon"></object>
                    </div>

                    <div class="cuerpoSecundario">
                      <div class="cuerpoCampos">
                        <div class="campoTitulos">
                          <div class="campoTitulo">COMPRA</div>
                          <div class="campoTitulo">SOBRA</div>
                          <div class="campoTitulo"></div>
                        </div>

                        <div class="campoEntradas">

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoKG">KG:</span><br>
                              <input type="text" class="inputKG inputValidar cercorte">
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoKG">KG:</span><br>
                              <input type="text" class="inputKG inputValidar cercorte">
                            </div>
                          </div>

                          <div class="campoEntrada"></div>
                          <div class="campoEntrada"></div>
                          <div class="campoEntrada"></div>
                        </div>
                      </div>

                      <div class="cuerpoBotones">
                        <button type="button" class="btn btn-primary botonProd" onclick="validarCargarProduccion('cercorte')">Cargar</button>
                        <button type="button" class="btn btn-warning botonProd" onclick="validarModificarProduccion('cercorte')">Modificar</button>
                        <button type="button" class="btn btn-danger botonProd" onclick="validarBorrarProduccion('cercorte')">Eliminar</button>
                      </div>
                    </div>

                  </div>


                  <!-- ############################## TARJETA CARGA DATOS CERDO - FIN ############################## -->







                <!-- ############################## TARJETA CARGA DATOS POLLO ############################## -->


                  <div class="cargaDatosSimple">

                    <div class="tituloPrimario">
                      POLLO
                      <object type="image/svg+xml" id="" data="resources/check.svg" class="prodCheck oculto svgIcon"></object>
                      <object type="image/svg+xml" id="" data="resources/cancel.svg" class="prodCancel oculto svgIcon"></object>
                    </div>

                    <div class="cuerpoPrimario">

                      <div class="cuerpoCampos">
                        
                        <div class="campoTitulos">
                          <div class="campoTitulo">COMPRA</div>
                          <div class="campoTitulo">VENTA</div>
                          <div class="campoTitulo">PROD. CORTE</div>
                          <div class="campoTitulo">SOBRA</div>
                          <div class="campoTitulo"></div>
                        </div>

                        <div class="campoEntradas">
                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoUM">UM:</span><br>
                              <input type="text" class="inputUM inputValidar pollo">
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoUM">UM:</span><br>
                              <input type="text" class="inputUM inputValidar pollo">
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoUM">UM:</span><br>
                              <input type="text" class="inputUM inputValidar pollo">
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoUM">UM:</span><br>
                              <input type="text" class="inputUM inputValidar pollo">
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                            </div>
                          </div>

                        </div>

                      </div>

                      <div class="cuerpoBotones">
                        <button type="button" class="btn btn-primary botonProd" onclick="validarCargarProduccion('pollo')">Cargar</button>
                        <button type="button" class="btn btn-warning botonProd" onclick="validarModificarProduccion('pollo')">Modificar</button>
                        <button type="button" class="btn btn-danger botonProd" onclick="validarBorrarProduccion('pollo')">Eliminar</button>
                      </div>

                    </div>

                  </div>


                <!-- ############################## TARJETA CARGA DATOS POLLO - FIN ############################## -->

                





                <!-- ############################## TARJETA CARGA DATOS EMBUTIDO ############################## -->


                  <div class="cargaDatosSimple">

                    <div class="tituloPrimario">
                      EMBUTIDO
                      <object type="image/svg+xml" id="" data="resources/check.svg" class="prodCheck oculto svgIcon"></object>
                      <object type="image/svg+xml" id="" data="resources/cancel.svg" class="prodCancel oculto svgIcon"></object>
                    </div>

                    <div class="cuerpoPrimario">

                      <div class="cuerpoCampos">
                        
                        <div class="campoTitulos">
                          <div class="campoTitulo">PRODUCCION</div>
                          <div class="campoTitulo">SOBRA</div>
                          <div class="campoTitulo"></div>
                          <div class="campoTitulo"></div>
                          <div class="campoTitulo"></div>
                        </div>

                        <div class="campoEntradas">
                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoKG">KG:</span><br>
                              <input type="text" class="inputKG inputValidar embutido"><br>
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoKG">KG:</span><br>
                              <input type="text" class="inputKG inputValidar embutido"><br>
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                            </div>
                          </div>

                        </div>
                      </div>

                      <div class="cuerpoBotones">
                        <button type="button" class="btn btn-primary botonProd" onclick="validarCargarProduccion('embutido')">Cargar</button>
                        <button type="button" class="btn btn-warning botonProd" onclick="validarModificarProduccion('embutido')">Modificar</button>
                        <button type="button" class="btn btn-danger botonProd" onclick="validarBorrarProduccion('embutido')">Eliminar</button>
                      </div>
                    </div>

                  </div>


                <!-- ############################## TARJETA CARGA DATOS EMBUTIDO - FIN ############################## -->









                <!-- ############################## TARJETA CARGA DATOS FIAMBRE ############################## -->


                  <div class="cargaDatosSimple">

                    <div class="tituloPrimario">
                      FIAMBRE
                      <object type="image/svg+xml" id="" data="resources/check.svg" class="prodCheck oculto svgIcon"></object>
                      <object type="image/svg+xml" id="" data="resources/cancel.svg" class="prodCancel oculto svgIcon"></object>
                    </div>

                    <div class="cuerpoPrimario">

                      <div class="cuerpoCampos">
                        
                        <div class="campoTitulos">
                          <div class="campoTitulo">PRODUCCION</div>
                          <div class="campoTitulo">SOBRA</div>
                          <div class="campoTitulo"></div>
                          <div class="campoTitulo"></div>
                          <div class="campoTitulo"></div>
                        </div>

                        <div class="campoEntradas">
                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoKG">KG:</span><br>
                              <input type="text" class="inputKG inputValidar fiambre"><br>
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                              <span class="textoKG">KG:</span><br>
                              <input type="text" class="inputKG inputValidar fiambre"><br>
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                            </div>
                          </div>

                          <div class="campoEntrada">
                            <div class="campoEntradaInterior">
                            </div>
                          </div>

                        </div>
                      </div>

                      <div class="cuerpoBotones">
                        <button type="button" class="btn btn-primary botonProd" onclick="validarCargarProduccion('fiambre')">Cargar</button>
                        <button type="button" class="btn btn-warning botonProd" onclick="validarModificarProduccion('fiambre')">Modificar</button>
                        <button type="button" class="btn btn-danger botonProd" onclick="validarBorrarProduccion('fiambre')">Eliminar</button>
                      </div>

                  </div>


                <!-- ############################## TARJETA CARGA DATOS FIAMBRE - FIN ############################## -->

                <!-- ############################## FIN PANTALLA PRINCIPAL ############################## -->

            </div>
        </div>
    </div>
  </div>





 
<!-- ################################### MODALES RES ENTERA  -->

  <!--  MODAL CARGAR ENTERO  -->
  <div id="modalCargarEntero" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title" id="titulitoCargar">
            <span id="prodCargaFecha">...</span>
          </h4>

        </div>

        
        <div class="modal-body modal-cargar">

          <div id="cargarTituloTabla" class="cargarTituloTablaModal ">
            Media Res Entera
          </div>

          <div class="cuerpoPrimario">

            <div class="cuerpoCamposModal">
              <div class="campoTitulos">
                <div class="campoTitulo">COMPRA</div>
                <div class="campoTitulo">VENTA</div>
                <div class="campoTitulo">PROD. CORTE</div>
                <div class="campoTitulo">PROD. VARIOS</div>
                <div class="campoTitulo">SOBRA</div>
              </div>

              <div class="campoEntradas">
                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG vacmediaModal" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM vacmediaModal" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG vacmediaModal" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM vacmediaModal" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG vacmediaModal" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM vacmediaModal" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG vacmediaModal" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM vacmediaModal" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG vacmediaModal" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM vacmediaModal" disabled>
                  </div>
                </div>

              </div>

            </div>
            <!-- FIN CAMPOS CUERPOS -->

          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="cargarProduccionLaunch('vacmedia')">Cargar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="inputOcultoCargar">
        </div>
      </div>
    </div>
  </div>

  <!--  MODAL EDITAR ENTERO  -->
  <div id="modalModificarEntero" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">
            <span id="prodModificarTitulo"></span>
             Media Res Entera - Modificar Datos -
            <span id="fechaModalModificarVacMedia"></span>
          </h4>
        </div>
        <div class="modal-body modal-cargar">
          <div class="prod-tabla-carga modal-titulo-carga">

            <div id="modificarTituloTabla" class="cargarTituloTabla tituloModalModificar tituloPrimario">
              Datos Anteriores
            </div>
            <div>
              <div class="cuerpoCamposModal">
                <div class="campoTitulos">
                  <div class="campoTitulo">COMPRA</div>
                  <div class="campoTitulo">VENTA</div>
                  <div class="campoTitulo">PROD. CORTE</div>
                  <div class="campoTitulo">PROD. VARIOS</div>
                  <div class="campoTitulo">SOBRA</div>
                </div>

                <div class="campoEntradas">
                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCamposAnteriorMedia" disabled>
                      <span class="textoUM">UM:</span>
                      <input type="text" class="inputUM modCamposAnteriorMedia" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCamposAnteriorMedia" disabled>
                      <span class="textoUM">UM:</span>
                      <input type="text" class="inputUM modCamposAnteriorMedia" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCamposAnteriorMedia" disabled>
                      <span class="textoUM">UM:</span>
                      <input type="text" class="inputUM modCamposAnteriorMedia" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCamposAnteriorMedia" disabled>
                      <span class="textoUM">UM:</span>
                      <input type="text" class="inputUM modCamposAnteriorMedia" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCamposAnteriorMedia" disabled>
                      <span class="textoUM">UM:</span>
                      <input type="text" class="inputUM modCamposAnteriorMedia" disabled>
                    </div>
                  </div>

                </div>

              </div>
            </div>

          </div>
        </div>

        <div class="modal-body modal-cargar">
          <div class="prod-tabla-carga modal-titulo-carga">

            <div id="cargarTituloTabla" class="cargarTituloTabla tituloModalModificar tituloPrimario">
              Datos Nuevos
            </div>

            <div>
              <div class="cuerpoCamposModal">
                <div class="campoTitulos">
                  <div class="campoTitulo">COMPRA</div>
                  <div class="campoTitulo">VENTA</div>
                  <div class="campoTitulo">PROD. CORTE</div>
                  <div class="campoTitulo">PROD. VARIOS</div>
                  <div class="campoTitulo">SOBRA</div>
                </div>

                <div class="campoEntradas">
                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCargarCamposMedia" disabled>
                      <span class="textoUM">UM:</span>
                      <input type="text" class="inputUM modCargarCamposMedia" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCargarCamposMedia" disabled>
                      <span class="textoUM">UM:</span>
                      <input type="text" class="inputUM modCargarCamposMedia" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCargarCamposMedia" disabled>
                      <span class="textoUM">UM:</span>
                      <input type="text" class="inputUM modCargarCamposMedia" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCargarCamposMedia" disabled>
                      <span class="textoUM">UM:</span>
                      <input type="text" class="inputUM modCargarCamposMedia" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCargarCamposMedia" disabled>
                      <span class="textoUM">UM:</span>
                      <input type="text" class="inputUM modCargarCamposMedia" disabled>
                    </div>
                  </div>

                </div>

              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="modificarProduccionLaunch('vacmedia')">Cargar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="inputOcultoModificar">
        </div>

      </div>
    </div>
  </div>

  <!--  MODAL BORRAR ENTERO  -->
  <div id="modalBorrarEntero" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title" id="fechaBorrarMedia">
            Borrar Datos?
            <span id="prodCargaFecha">...</span>
          </h4>

        </div>

          
        <div class="modal-body modal-cargar">

          <div id="cargarTituloTabla" class="cargarTituloTablaModal">
            Media Res Entera
          </div>

          <div class="cuerpoPrimario">

            <div class="cuerpoCamposModal">
              <div class="campoTitulos">
                <div class="campoTitulo">COMPRA</div>
                <div class="campoTitulo">VENTA</div>
                <div class="campoTitulo">PROD. CORTE</div>
                <div class="campoTitulo">PROD. VARIOS</div>
                <div class="campoTitulo">SOBRA</div>
              </div>

              <div class="campoEntradas">
                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG borrarCamposMedia" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM borrarCamposMedia" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG borrarCamposMedia" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM borrarCamposMedia" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG borrarCamposMedia" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM borrarCamposMedia" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG borrarCamposMedia" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM borrarCamposMedia" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG borrarCamposMedia" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM borrarCamposMedia" disabled>
                  </div>
                </div>

              </div>

            </div>
            <!-- FIN CAMPOS CUERPOS -->

          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="borrarProduccionLaunch('vacmedia')">Borrar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="inputOcultoBorrar">
        </div>
      </div>
    </div>
  </div>

<!-- ################################### MODALES RES ENTERA (FIN) -->



<!-- ################################### MODALES RES CORTES  -->

  <!-- ############### MODAL CARGAR CORTES ############### -->
  <div id="modalCargarCortes" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title" id="titulitoCorte">
            <span id="prodCargaFecha">...</span>
          </h4>

        </div>

          
        <div class="modal-body modal-cargar">

          <div id="cargarTituloTabla" class="cargarTituloTablaModal">
            Res Cortes
          </div>

          <div class="cuerpoSecundario">

            <div class="cuerpoCamposModal">

              <div class="campoTitulos">
                <div class="campoTitulo">COMPRA</div>
                <div class="campoTitulo">SOBRA</div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
              </div>

              <div class="campoEntradas">

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG vaccorteModal" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG vaccorteModal" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                </div>

                <div class="campoEntrada">
                </div>

                <div class="campoEntrada">
                </div>
              </div>
              <!-- Fin campos entradas -->

            </div>
            <!-- Fin cuerpo campos modal -->
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="cargarProduccionLaunch('vaccorte')">Cargar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="inputOcultoCargar">
        </div>
      </div>
    </div>
  </div>

  <!-- ############### MODAL EDITAR CORTES ############### -->
  <div id="modalModificarCortes" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">
            <span id="prodModificarTitulo"></span>
            Res Cortes - Modificar Datos -
            <span id="fechaModalModificarVacCorte"></span>
          </h4>
        </div>
        <div class="modal-body modal-cargar">
          <div class="prod-tabla-carga modal-titulo-carga">

            <div id="modificarTituloTabla" class="cargarTituloTabla tituloModalModificar tituloPrimario">
              Datos Anteriores
            </div>

            <div class="cuerpoCamposModal">

              <div class="campoTitulos">
                <div class="campoTitulo">COMPRA</div>
                <div class="campoTitulo">SOBRA</div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
              </div>

              <div class="campoEntradas">

                <div class="campoEntradaModificarCorte">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG modCamposAnteriorCorte" disabled>
                  </div>
                </div>

                <div class="campoEntradaModificarCorte">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG modCamposAnteriorCorte" disabled>
                  </div>
                </div>

                <div class="campoEntradaModificarCorte">
                </div>

                <div class="campoEntradaModificarCorte">
                </div>

                <div class="campoEntradaModificarCorte">
                </div>
              </div>

            </div>
            <!-- Fin Cuerpo Campos Modal -->


          </div>
        </div>

        <div class="modal-body modal-cargar">
          <div class="prod-tabla-carga modal-titulo-carga">

            <div id="cargarTituloTabla" class="cargarTituloTabla tituloModalModificar tituloPrimario">
              Datos Nuevos
            </div>

            <div class="cuerpoCamposModal">

              <div class="campoTitulos">
                <div class="campoTitulo">COMPRA</div>
                <div class="campoTitulo">SOBRA</div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
              </div>

              <div class="campoEntradas">

                <div class="campoEntradaModificarCorte">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG modCargarCamposCorte" disabled>
                  </div>
                </div>

                <div class="campoEntradaModificarCorte">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG modCargarCamposCorte" disabled>
                  </div>
                </div>

                <div class="campoEntradaModificarCorte">
                </div>

                <div class="campoEntradaModificarCorte">
                </div>

                <div class="campoEntradaModificarCorte">
                </div>
              </div>

            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="modificarProduccionLaunch('vaccorte')">Cargar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="inputOcultoModificar">
        </div>

      </div>
    </div>
  </div>

  <!-- ############### MODAL BORRAR CORTES ############### -->
  <div id="modalBorrarCortes" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title" id="fechaBorrarCorte">
            <span id="prodCargaFecha"></span>
          </h4>

        </div>

        <div class="modal-body modal-cargar">

          <div id="cargarTituloTabla" class="cargarTituloTablaModal">
            Res Cortes
          </div>

          <div class="cuerpoSecundario">

            <div class="cuerpoCamposModal">

              <div class="campoTitulos">
                <div class="campoTitulo">COMPRA</div>
                <div class="campoTitulo">SOBRA</div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
              </div>

              <div class="campoEntradas">

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG borrarCamposCorte" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG borrarCamposCorte" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                </div>

                <div class="campoEntrada">
                </div>

                <div class="campoEntrada">
                </div>
              </div>
              <!-- Fin campos entradas -->

            </div>
            <!-- Fin cuerpo campos modal -->
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="borrarProduccionLaunch('vaccorte')">Borrar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="inputOcultoBorrar">
        </div>
      </div>
    </div>
  </div>

<!-- ################################### MODALES RES CORTES (FIN) -->




<!-- ################################### MODALES CERDO MEDIA -->

  <!-- ############### MODAL CARGAR CERDO MEDIA ############### -->
  <div id="modalCargarMediaCerdo" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title" id="titulitoCerdoMedia">
            <span id="">...</span>
          </h4>

        </div>

          
        <div class="modal-body modal-cargar">

          <div id="" class="cargarTituloTablaModal">
            Cerdo Media
          </div>

          <div class="cuerpoSecundario">

            <div class="cuerpoCamposModal">

              <div class="campoTitulos">
                <div class="campoTitulo">COMPRA</div>
                <div class="campoTitulo">VENTA</div>
                <div class="campoTitulo">PROD.CORTE</div>
                <div class="campoTitulo">SOBRA</div>
                <div class="campoTitulo"></div>
              </div>

              <div class="campoEntradas">

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG cermediaModal" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM cermediaModal" disabled> 
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG cermediaModal" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM cermediaModal" disabled> 
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG cermediaModal" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM cermediaModal" disabled> 
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG cermediaModal" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM cermediaModal" disabled> 
                  </div>
                </div>

                <div class="campoEntrada">
                </div>
              </div>
              <!-- Fin campos entradas -->

            </div>
            <!-- Fin cuerpo campos modal -->
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="cargarProduccionLaunch('cermedia')">Cargar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="">
        </div>
      </div>
    </div>
  </div>

  <!-- ############### MODAL EDITAR CERDO MEDIA ############### -->
  <div id="modalModificarMediaCerdo" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">
            <span id=""></span>
            Cerdo Media - Modificar Datos -
            <span id="fechaModalModificarCerdo">asdf</span>
          </h4>
        </div>


        <div class="modal-body modal-cargar">
          <div class="prod-tabla-carga modal-titulo-carga">

            <div id="" class="cargarTituloTabla tituloModalModificar tituloPrimario">
              Datos Anteriores
            </div>

            <div class="cuerpoCamposModal">

              <div class="campoTitulos">
                <div class="campoTitulo">COMPRA</div>
                <div class="campoTitulo">VENTA</div>
                <div class="campoTitulo">PROD. CORTE</div>
                <div class="campoTitulo">SOBRA</div>
                <div class="campoTitulo"></div>
              </div>

              <div class="campoEntradas">

                <div class="campoEntrada">
                  <div class="campoEntradaInteriorModalModificar">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG modCamposAnteriorMediaCerdo" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM modCamposAnteriorMediaCerdo" disabled> 
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInteriorModalModificar">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG modCamposAnteriorMediaCerdo" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM modCamposAnteriorMediaCerdo" disabled> 
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInteriorModalModificar">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG modCamposAnteriorMediaCerdo" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM modCamposAnteriorMediaCerdo" disabled> 
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInteriorModalModificar">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG modCamposAnteriorMediaCerdo" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM modCamposAnteriorMediaCerdo" disabled> 
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInteriorModalModificar">
                  </div>
                </div>

              </div>

            </div>

          </div>
        </div>

        <div class="modal-body modal-cargar">
          <div class="prod-tabla-carga modal-titulo-carga">

            <div id="" class="cargarTituloTabla tituloModalModificar tituloPrimario">
              Datos Nuevos
            </div>

            <div class="cuerpoCamposModal">
              <div class="campoTitulos">
                <div class="campoTitulo">COMPRA</div>
                <div class="campoTitulo">VENTA</div>
                <div class="campoTitulo">PROD. CORTE</div>
                <div class="campoTitulo">SOBRA</div>
                <div class="campoTitulo"></div>
              </div>

              <div class="campoEntradas">

                <div class="campoEntrada">
                  <div class="campoEntradaInteriorModalModificar">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG modCargarCamposMediaCerdo" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM modCargarCamposMediaCerdo" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInteriorModalModificar">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG modCargarCamposMediaCerdo" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM modCargarCamposMediaCerdo" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInteriorModalModificar">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG modCargarCamposMediaCerdo" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM modCargarCamposMediaCerdo" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInteriorModalModificar">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG modCargarCamposMediaCerdo" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM modCargarCamposMediaCerdo" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInteriorModalModificar">
                  </div>
                </div>

              </div>

            </div>

          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="modificarProduccionLaunch('cermedia')">Cargar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="">
        </div>

      </div>
    </div>
  </div>  


  <!-- ############### MODAL BORRAR CERDO BORRAR ############### -->
  <div id="modalBorrarMediaCerdo" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title" id="">
            <span id="fechaBorrarMediaCerdo"></span>
          </h4>

        </div>

        <div class="modal-body modal-cargar">

          <div id="" class="cargarTituloTablaModal">
            Cerdo Media
          </div>

          <div class="cuerpoSecundario">

            <div class="cuerpoCamposModal">

              <div class="campoTitulos">
                <div class="campoTitulo">COMPRA</div>
                <div class="campoTitulo">VENTA</div>
                <div class="campoTitulo">PROD. CORTE</div>
                <div class="campoTitulo">SOBRA</div>
                <div class="campoTitulo"></div>
              </div>

              <div class="campoEntradas">

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG borrarCamposMediaCerdo" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM borrarCamposMediaCerdo" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG borrarCamposMediaCerdo" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM borrarCamposMediaCerdo" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG borrarCamposMediaCerdo" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM borrarCamposMediaCerdo" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG borrarCamposMediaCerdo" disabled>
                    <span class="textoUM">UM:</span>
                    <input type="text" class="inputUM borrarCamposMediaCerdo" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                  </div>
                </div>
              </div>
              <!-- Fin campos entradas -->

            </div>
            <!-- Fin cuerpo campos modal -->
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="borrarProduccionLaunch('cermedia')">Borrar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="">
        </div>

      </div>
    </div>
  </div>

<!-- ################################### MODALES CERDO CORTES (FIN) -->





<!-- ################################### MODALES CERDO CORTES -->


  <!-- ############### MODAL CARGAR CERDO CORTE ############### -->
  <div id="modalCargarCortesCerdo" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title" id="titulitoCerdoCorte">
            <span id="">Modal cerdo</span>
          </h4>

        </div>

          
        <div class="modal-body modal-cargar">

          <div id="" class="cargarTituloTablaModal">
            Cerdo Corte
          </div>

          <div class="cuerpoSecundario">

            <div class="cuerpoCamposModal">

              <div class="campoTitulos">
                <div class="campoTitulo">COMPRA</div>
                <div class="campoTitulo">SOBRA</div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
              </div>

              <div class="campoEntradas">

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG cercorteModal" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG cercorteModal" disabled>
                  </div>
                </div>

                <div class="campoEntrada">

                </div>

                <div class="campoEntrada">
                </div>

                <div class="campoEntrada">
                </div>
              </div>
              <!-- Fin campos entradas -->

            </div>
            <!-- Fin cuerpo campos modal -->
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="cargarProduccionLaunch('cercorte')">Cargar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="">
        </div>
      </div>
    </div>
  </div>


  <!-- ############### MODAL MODIICAR CERDO CORTE ############### -->
  <div id="modalModificarCorteCerdo" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">
            <span id=""></span>
            Cerdo Cortes - Modificar Datos -
            <span id="fechaModalModificarCerdoCorte"></span>
          </h4>
        </div>

        <div class="modal-body modal-cargar">
          <div class="prod-tabla-carga modal-titulo-carga">

            <div id="" class="cargarTituloTabla tituloModalModificar tituloPrimario">
              Datos Anteriores
            </div>

              <div class="cuerpoCamposModal">

                <div class="campoTitulos">
                  <div class="campoTitulo">COMPRA</div>
                  <div class="campoTitulo">SOBRA</div>
                  <div class="campoTitulo"></div>
                  <div class="campoTitulo"></div>
                  <div class="campoTitulo"></div>
                </div>

                <div class="campoEntradas">

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCamposAnteriorCorteCerdo" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCamposAnteriorCorteCerdo" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

              </div>
            </div>

          </div>
        </div>

        <div class="modal-body modal-cargar">
          <div class="prod-tabla-carga modal-titulo-carga">

            <div id="" class="cargarTituloTabla tituloModalModificar tituloPrimario">
              Datos Nuevos
            </div>

    
              <div class="cuerpoCamposModal">
                <div class="campoTitulos">
                  <div class="campoTitulo"></div>
                  <div class="campoTitulo"></div>
                  <div class="campoTitulo"></div>
                  <div class="campoTitulo"></div>
                  <div class="campoTitulo"></div>
                </div>

                <div class="campoEntradas">

                  <div class="campoEntrada">
                     
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCargarCamposCorteCerdo" disabled>
                    </div>
                  
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoUM">KG:</span>
                      <input type="text" class="inputUM modCargarCamposCorteCerdo" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

                </div>

              </div>

          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="modificarProduccionLaunch('cercorte')">Cargar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="">
        </div>

      </div>
    </div>
  </div>  


  <!-- ############### MODAL BORRAR CERDO CORTE ############### -->
  <div id="modalBorrarCorteCerdo" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title" id="">
            <span id="fechaBorrarCorteCerdo"></span>
          </h4>

        </div>

        <div class="modal-body modal-cargar">

          <div id="" class="cargarTituloTablaModal">
            Cerdo Corte
          </div>

          <div class="cuerpoSecundario">

            <div class="cuerpoCamposModal">

              <div class="campoTitulos">
                <div class="campoTitulo">COMPRA</div>
                <div class="campoTitulo">SOBRA</div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
              </div>

              <div class="campoEntradas">

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG borrarCamposCorteCerdo" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG borrarCamposCorteCerdo" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                  </div>
                </div>
              </div>
              <!-- Fin campos entradas -->

            </div>
            <!-- Fin cuerpo campos modal -->
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="borrarProduccionLaunch('cercorte')">Borrar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="">
        </div>
      </div>
    </div>
  </div>

<!-- ################################### MODALES CERDO MEDIAS (FIN) -->




<!-- ################################### MODALES POLLO  -->

  <!-- ############### MODAL CARGAR  ############### -->
  <div id="modalCargarPollo" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title" id="titulitoPollo">
            <span id="">...</span>
          </h4>

        </div>

          
        <div class="modal-body modal-cargar">

          <div id="" class="cargarTituloTablaModal">
            Pollo
          </div>

          <div class="cuerpoSecundario">

            <div class="cuerpoCamposModal">

              <div class="campoTitulos">
                <div class="campoTitulo">Compra</div>
                <div class="campoTitulo">Venta</div>
                <div class="campoTitulo">Prod. corte</div>
                <div class="campoTitulo">Sobra</div>
                <div class="campoTitulo"></div>
              </div>

              <div class="campoEntradas">

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG polloModal" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">  
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG polloModal" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG polloModal" disabled>
                  </div>
                </div>


                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG polloModal" disabled>
                  </div>
                </div>


                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                  </div>
                </div>

              </div>
              <!-- Fin campos entradas -->

            </div>
            <!-- Fin cuerpo campos modal -->
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="cargarProduccionLaunch('pollo')">Cargar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="inputOcultoModificar">
        </div>

      </div>
    </div>
  </div>

  <!-- ############### MODAL EDITAR  ############### -->
  <div id="modalModificarPollo" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">
            <span id=""></span>
            Pollo - Modificar Datos -
            <span id="fechaModalModificarPollo"></span>
          </h4>
        </div>
        <div class="modal-body modal-cargar">
          <div class="prod-tabla-carga modal-titulo-carga">

            <div id="" class="cargarTituloTabla tituloModalModificar tituloPrimario">
              Datos Anteriores
            </div>

              <div class="cuerpoCamposModal">

                <div class="campoTitulos">
                  <div class="campoTitulo">Compra</div>
                  <div class="campoTitulo">Venta</div>
                  <div class="campoTitulo">Prod. corte</div>
                  <div class="campoTitulo">Sobra</div>
                  <div class="campoTitulo"></div>
                </div>

                <div class="campoEntradas">

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCamposAnteriorPoll" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                       <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCamposAnteriorPoll" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                       <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCamposAnteriorPoll" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                       <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCamposAnteriorPoll" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

                </div>

              </div>

          </div>
        </div>

        <div class="modal-body modal-cargar">
          <div class="prod-tabla-carga modal-titulo-carga">

            <div id="" class="cargarTituloTabla tituloModalModificar tituloPrimario">
              Datos Nuevos
            </div>

              <div class="cuerpoCamposModal">
                <div class="campoTitulos">
                  <div class="campoTitulo">Compra</div>
                  <div class="campoTitulo">Venta</div>
                  <div class="campoTitulo">Prod. corte</div>
                  <div class="campoTitulo">Sobra</div>
                  <div class="campoTitulo"></div>
                </div>

                <div class="campoEntradas">

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCargarCamposPoll" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                       <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCargarCamposPoll" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                       <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCargarCamposPoll" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                       <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCargarCamposPoll" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="modificarProduccionLaunch('pollo')">Cargar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="">
        </div>

      </div>
    </div>
  </div>  


  <!-- ############### MODAL BORRAR ############### -->
  <div id="modalBorrarPollo" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title" id="">
            <span id="fechaBorrarPollo"></span>
          </h4>

        </div>

        <div class="modal-body modal-cargar">

          <div id="" class="cargarTituloTablaModal">
            Pollo
          </div>

          <div class="cuerpoSecundario">

            <div class="cuerpoCamposModal">

              <div class="campoTitulos">
                <div class="campoTitulo">Compra</div>
                <div class="campoTitulo">Venta</div>
                <div class="campoTitulo">Prod. corte</div>
                <div class="campoTitulo">Sobra</div>
                <div class="campoTitulo"></div>
              </div>

              <div class="campoEntradas">

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG borrarCamposPollo" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG borrarCamposPollo" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG borrarCamposPollo" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG borrarCamposPollo" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                  </div>
                </div>
              </div>
              <!-- Fin campos entradas -->

            </div>
            <!-- Fin cuerpo campos modal -->
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="borrarProduccionLaunch('pollo')">Borrar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="inputOcultoBorrar">
        </div>
      </div>
    </div>
  </div>

<!-- ################################### MODALES POLLO (FIN) -->








<!-- ################################### MODALES EMBUTIDO  -->

  <!-- ############### MODAL CARGAR  ############### -->
  <div id="modalCargarEmbutido" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title" id="titulitoEmbutido">
            <span id="">...</span>
          </h4>

        </div>

          
        <div class="modal-body modal-cargar">

          <div id="" class="cargarTituloTablaModal">
            Embutido
          </div>

          <div class="cuerpoSecundario">

            <div class="cuerpoCamposModal">

              <div class="campoTitulos">
                <div class="campoTitulo">Produccion</div>
                <div class="campoTitulo">Sobra</div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
              </div>

              <div class="campoEntradas">

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG embutidoModal" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                    <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG embutidoModal" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                </div>

                <div class="campoEntrada">
                </div>

                <div class="campoEntrada">
                </div>
              </div>
              <!-- Fin campos entradas -->

            </div>
            <!-- Fin cuerpo campos modal -->
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="cargarProduccionLaunch('embutido')">Cargar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="">
        </div>
      </div>
    </div>
  </div>

  <!-- ############### MODAL EDITAR  ############### -->
  <div id="modalModificarEmbutido" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">
            <span id=""></span>
            Embutido - Modificar Datos -
            <span id="fechaModalModificarEmbutido"></span>
          </h4>
        </div>
        <div class="modal-body modal-cargar">
          <div class="prod-tabla-carga modal-titulo-carga">

            <div id="" class="cargarTituloTabla tituloModalModificar tituloPrimario">
              Datos Anteriores
            </div>

              <div class="cuerpoCamposModal">
                <div class="campoTitulos">
                  <div class="campoTitulo">Produccion</div>
                  <div class="campoTitulo">Sobra</div>
                  <div class="campoTitulo"></div>
                  <div class="campoTitulo"></div>
                  <div class="campoTitulo"></div>
                </div>

                <div class="campoEntradas">

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCamposAnteriorEmbutido" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCamposAnteriorEmbutido" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

                </div>

              </div>

          </div>
        </div>

        <div class="modal-body modal-cargar">
          <div class="prod-tabla-carga modal-titulo-carga">

            <div id="" class="cargarTituloTabla tituloModalModificar tituloPrimario">
              Datos Nuevos
            </div>

              <div class="cuerpoCamposModal">
                <div class="campoTitulos">
                  <div class="campoTitulo">Produccion</div>
                  <div class="campoTitulo">Sobra</div>
                  <div class="campoTitulo"></div>
                  <div class="campoTitulo"></div>
                  <div class="campoTitulo"></div>
                </div>

                <div class="campoEntradas">

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCargarCamposEmbutido" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCargarCamposEmbutido" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

                </div>

              </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="modificarProduccionLaunch('embutido')">Cargar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="inputOcultoModificar">
        </div>

      </div>
    </div>
  </div>  


  <!-- ############### MODAL BORRAR ############### -->
  <div id="modalBorrarEmbutido" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title" id="">
            <span id="fechaBorrarEmbutido"></span>
          </h4>

        </div>

        <div class="modal-body modal-cargar">

          <div id="" class="cargarTituloTablaModal">
            Embutido
          </div>

          <div class="cuerpoSecundario">

            <div class="cuerpoCamposModal">

              <div class="campoTitulos">
                <div class="campoTitulo">Produccion</div>
                <div class="campoTitulo">Sobra</div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
              </div>

              <div class="campoEntradas">

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG borrarCamposEmbutido" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG borrarCamposEmbutido" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                  </div>
                </div>
              </div>
              <!-- Fin campos entradas -->

            </div>
            <!-- Fin cuerpo campos modal -->
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="borrarProduccionLaunch('embutido')">Borrar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="inputOcultoBorrar">
        </div>
      </div>
    </div>
  </div>

<!-- ################################### MODALES EMBUTIDO (FIN) -->








<!-- ################################### MODALES FIAMBRE  -->

  <!-- ############### MODAL CARGAR  ############### -->
  <div id="modalCargarFiambre" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title" id="titulitoFiambre">
            <span id="">...</span>
          </h4>

        </div>

          
        <div class="modal-body modal-cargar">

          <div id="" class="cargarTituloTablaModal">
            Fiambre
          </div>

          <div class="cuerpoSecundario">

            <div class="cuerpoCamposModal">

              <div class="campoTitulos">
                <div class="campoTitulo">Produccion</div>
                <div class="campoTitulo">Sobra</div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
              </div>

              <div class="campoEntradas">

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG fiambreModal" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                    <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span><br>
                    <input type="text" class="inputKG fiambreModal" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                </div>

                <div class="campoEntrada">
                </div>

                <div class="campoEntrada">
                </div>
              </div>
              <!-- Fin campos entradas -->

            </div>
            <!-- Fin cuerpo campos modal -->
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="cargarProduccionLaunch('fiambre')">Cargar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="">
        </div>
      </div>
    </div>
  </div>

  <!-- ############### MODAL EDITAR  ############### -->
  <div id="modalModificarFiambre" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">
            <span id=""></span>
            Fiambre - Modificar Datos -
            <span id="fechaModalModificarFiambre"></span>
          </h4>
        </div>
        <div class="modal-body modal-cargar">
          <div class="prod-tabla-carga modal-titulo-carga">

            <div id="" class="cargarTituloTabla tituloModalModificar tituloPrimario">
              Datos Anteriores
            </div>



              <div class="cuerpoCamposModal">
                <div class="campoTitulos">
                  <div class="campoTitulo">Produccion</div>
                  <div class="campoTitulo">Sobra</div>
                  <div class="campoTitulo"></div>
                  <div class="campoTitulo"></div>
                  <div class="campoTitulo"></div>
                </div>

                <div class="campoEntradas">

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCamposAnteriorFiambre" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCamposAnteriorFiambre" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

                </div>

              </div>

          </div>
        </div>

        <div class="modal-body modal-cargar">
          <div class="prod-tabla-carga modal-titulo-carga">

            <div id="" class="cargarTituloTabla tituloModalModificar tituloPrimario">
              Datos Nuevos
            </div>

              <div class="cuerpoCamposModal">
                <div class="campoTitulos">
                  <div class="campoTitulo">Produccion</div>
                  <div class="campoTitulo">Sobra</div>
                  <div class="campoTitulo"></div>
                  <div class="campoTitulo"></div>
                  <div class="campoTitulo"></div>
                </div>

                <div class="campoEntradas">

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCargarCamposFiambre" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                      <span class="textoKG">KG:</span>
                      <input type="text" class="inputKG modCargarCamposFiambre" disabled>
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

                  <div class="campoEntrada">
                    <div class="campoEntradaInteriorModalModificar">
                    </div>
                  </div>

                </div>

              </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="modificarProduccionLaunch('fiambre')">Cargar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="inputOcultoModificar">
        </div>

      </div>
    </div>
  </div>  


  <!-- ############### MODAL BORRAR ############### -->
  <div id="modalBorrarFiambre" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title" id="">
            <span id="fechaBorrarFiambre"></span>
          </h4>

        </div>

        <div class="modal-body modal-cargar">

          <div id="" class="cargarTituloTablaModal">
            Fiambre
          </div>

          <div class="cuerpoSecundario">

            <div class="cuerpoCamposModal">

              <div class="campoTitulos">
                <div class="campoTitulo">Produccion</div>
                <div class="campoTitulo">Sobra</div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
                <div class="campoTitulo"></div>
              </div>

              <div class="campoEntradas">

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG borrarCamposFiambre" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                    <span class="textoKG">KG:</span>
                    <input type="text" class="inputKG borrarCamposFiambre" disabled>
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                  </div>
                </div>

                <div class="campoEntrada">
                  <div class="campoEntradaInterior">
                  </div>
                </div>
              </div>
              <!-- Fin campos entradas -->

            </div>
            <!-- Fin cuerpo campos modal -->
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="borrarProduccionLaunch('fiambre')">Borrar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <input type="hidden" id="inputOcultoBorrar">
        </div>
      </div>
    </div>
  </div>

<!-- ################################### MODALES FIAMBRE (FIN) -->






<!-- ################################### MODALES ALTA BAJA MODIFICACION  -->

  <!-- MODAL QUE AVISA QUE SE CARGARON LOS DATOS -->
  <div id="produccionCargadaModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title prodContraseniaTextoTitulo">Alerta</h4>
        </div>

        <div class="modal-body">
          <div class="prodContraseniaDivPrincipal">
            <span>Datos cargados correctamente.</span>
            <br>
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
        </div>

      </div>
    </div>
  </div>

  <!-- MODAL QUE AVISA QUE SE MODIFICARON LOS DATOS -->
  <div id="produccionModificadaModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title prodContraseniaTextoTitulo">Alerta</h4>
        </div>

        <div class="modal-body">
          <div class="prodContraseniaDivPrincipal">
            <span>Datos modificados correctamente.</span>
            <br>
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
        </div>

      </div>
    </div>
  </div>

  <!-- MODAL QUE AVISA QUE SE BORRARON LOS DATOS -->
  <div id="produccionBorradaModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title prodContraseniaTextoTitulo">Alerta</h4>
        </div>

        <div class="modal-body">
          <div class="prodContraseniaDivPrincipal">
            <span>Se borraron los datos de produccion correctamente.</span>
            <br>
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
        </div>

      </div>
    </div>
  </div>

<!-- ################################### MODALES ALTA BAJA MODIFICACION (FIN)  -->




<!-- ################################### MODALES OTROS  -->

  <!-- MODAL DE CONTRASEÑA MODIFICAR-->
  <div id="contraseniaModificar" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title prodContraseniaTextoTitulo">Requiere autorizacion</h4>
        </div>

        <div class="modal-body">
          <div class="prodContraseniaDivPrincipal">
            <span>Contraseña:</span>
            <br>
            <input type="password" class="inputCentrado" id="prodContrInput">
          </div>
          <div class="prodContraseniaDivMensaje">
            <span class="prodContraseniaSpanMsj oculto">
              Contraseña incorrecta
            </span>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="validarContraseniaModificar()">Aceptar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>

      </div>
    </div>
  </div>

  <!-- MODAL DE CONTRASEÑA BORRAR-->
  <div id="contraseniaBorrar" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title prodContraseniaTextoTitulo">Requiere autorizacion</h4>
        </div>

        <div class="modal-body">
          <div class="prodContraseniaDivPrincipal">
            <span>Contraseña:</span>
            <br>
            <input type="password" class="inputCentrado" id="prodContrInputBorrar">
          </div>
          <div class="prodContraseniaDivMensaje">
            <span class="prodContraseniaSpanMsjBorrar oculto">
              Contraseña incorrecta
            </span>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="validarContraseniaBorrar()">Aceptar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>

      </div>
    </div>
  </div>

  <!-- MODAL QUE AVISA QUE NO HAY CONEXIONES CON LA BASE DE DATOS -->
  <div id="noConexionModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title text-center prodContraseniaTextoTitulo textoRojo">Alerta</h4>
        </div>

        <div class="modal-body">
          <div class="prodContraseniaDivPrincipal">
            <span>No se puede conectar con el servidor.</span>
            <br>
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
        </div>

      </div>
    </div>
  </div>

  <!-- MODAL QUE AVISA QUE HAY FECHA CARGADA -->
  <div id="fechaExistenteModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title prodContraseniaTextoTitulo">Alerta</h4>
        </div>
        <div class="modal-body">
          <div class="prodContraseniaDivPrincipal">
            <span>Ya hay un registro cargado en esa fecha.</span>
            <br>
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
        </div>

      </div>
    </div>
  </div>

  <!-- MODAL QUE AVISA QUE NO HAY FECHA CARGADA -->
  <div id="sinFechaModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title prodContraseniaTextoTitulo">Alerta</h4>
        </div>

        <div class="modal-body">
          <div class="prodContraseniaDivPrincipal">
            <span>No se ha encontrado registro cargado en esa fecha.</span>
            <br>
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
        </div>

      </div>
    </div>
  </div>

<!-- ################################### MODALES OTROS (FIN)  -->


</body>
<script type="text/javascript" src="js/jsProd.js"></script>
</html>