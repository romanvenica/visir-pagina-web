<?php
session_start();
$p_acc = $_SESSION["p_acc"];
if ($p_acc != 1)
{
  header('Location: index.php');
  die();
} 
?>


<!DOCTYPE html>
<html>
<head>
  <link rel="icon" href="resources/pyramid.png">
	<script src="js/jquery-3.2.1.js"></script>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/estilos.css">
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<title>Visir</title>
</head> 
<body>

    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="bienvenido.php">Visir</a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav pull-right">

              <?php 

              if ($_SESSION["p_admin"] == 1) 
              {
              	echo
              	"
              	<li>
                <a href='admin.php'>Admin</a>
              	</li>
              	"
              	;
              }

              if ($_SESSION["p_prod"] == 1) 
              {
              	echo
              	"
              	<li>
                <a href='cargaProduccion.php'>Produccion</a>
              	</li>
              	"
              	;
              }

              if ($_SESSION["p_sdo"] == 1) 
              {
              	echo
              	"
              	<li>
                <a href='cargaDatos.php'>Saldo</a>
              	</li>
              	"
              	;
              }
              
              

              if ($_SESSION["p_rrhh"] == 1) 
              {
              	echo
              	"
              	<li>
                <a href='cargaRRHH.php'>RRHH</a>
              	</li>
              	"
              	;
              }

              ?>
              <a href="logout.php">
                <button class="btn btn-danger navbar-btn" class="active">Cerrar Sesion</button>
              </a>

            </ul>
          </div>
        </div>
    </nav>


  <div class="container">
     <div class="jumbotron bdoJumbotron">
       <h1>Bienvenido.</h1> 
       <br>
       <p>Tablero de control Visir.</p> 
     </div>
   </div> 




	<!-- <div class="container">
  
      <div class="side left"></div>
      <div class="side front"></div>
      <div class="side right"></div>
      <div class="side back"></div>
      
      <div class="shadow"></div>
  </div> -->









</body>

</html>