ponerValidacionACampos();
cargarTilde();

/*######################## FUNCIONES DE ALTA ########################*/

function cargarTilde()
{
	var fecha = document.getElementById("rrhhFechaInput");

	fecha.onchange = function()
	{

	// Si tocaste el boton aceptar sin seleccionar fecha salta un error, con esto lo escondes.
	document.getElementById("rrhhErrorSpanFecha").classList.add('oculto');
	// Con esto escondes el cuadro rojo del campo fecha.
	document.getElementById("rrhhFechaInput").classList.remove("campoInvalido");

	var fechaF = new Date(fecha.value);
	var fechaTil = addDays(fechaF, 1);
	var fechaTilde = String(fechaTil.getFullYear()) +"/"+ String(fechaTil.getMonth()+1) +"/"+ String(fechaTil.getDate());
	$.ajax({
	type: "POST",
	url: "php/tildeRRHH.php",
	dataType: "json",
	data: {'fecha' : fechaTilde},
	success: function (result) 
	{
		if (result == true) 
		{
			document.getElementById("rrhhIconCancel").classList.add("oculto");
			document.getElementById("rrhhIconCheck").classList.remove("oculto");
		}
		else
		{
			document.getElementById("rrhhIconCheck").classList.add("oculto");
			document.getElementById("rrhhIconCancel").classList.remove("oculto");
		}
  	},
  		error: function (xhr, status, error) {
		  	console.log(xhr);
		  	console.log(status);
		  	console.log(error);
		  	$("#noConexionModal").modal();}
		})
	}
}

function modalCrearCargar()
{
	var fecha = document.getElementById("rrhhFechaInput").value;
	var asistenciaEsperada = document.getElementById("inputRRHHEsperada").value;
	var asistenciaReal = document.getElementById("inputRRHHReal").value;
	var fechaF = addDays(new Date(fecha),1);
	var fechaLatina = fechaF.getDate() + "/" + (fechaF.getMonth()+1) + "/" + fechaF.getFullYear();
	document.getElementById("fechaElegida").innerHTML = fechaLatina;
	document.getElementById("asistenciaEsperadaElegida").innerHTML = asistenciaReal;
	document.getElementById("asistenciaRealElegida").innerHTML =  asistenciaEsperada;
	$("#cargarDatos").modal();
}


function cargarAsistencia(fecha, asistenciaReal, asistenciaEsperada)
{
	$.ajax({
    type: "POST",
	url: "php/insertAsistencia.php",
	dataType: "json",
	data: {'fecha' : fecha, 'asistenciaReal' : asistenciaReal, 'asistenciaEsperada' : asistenciaEsperada},
	success: function (result) 
	{
		document.getElementById("rrhhIconCheck").classList.remove("oculto");
    	document.getElementById("rrhhIconCancel").classList.add("oculto");
		$("#asistenciaCargadaModal").modal();
    },
    error: function (xhr, status, error) {
    	$("#noConexionModal").modal();
    }
    });
}

function cargarAsistenciaLaunch()
{
	var fecha = document.getElementById("rrhhFechaInput").value;
	var asistenciaEsperada = document.getElementById("inputRRHHEsperada").value;
	var asistenciaReal = document.getElementById("inputRRHHReal").value;
	cargarAsistencia(fecha, asistenciaReal, asistenciaEsperada);
}

function verFechaCargar(fecha)
{
	$.ajax({
    type: "POST",
	url: "php/rrhhVerFecha.php",
	dataType: "json",
	data: {'fecha' : fecha},
	success: function (result) {
		if (result == false) 
		{
			modalCrearCargar();
		}
		else
		{
			$("#fechaExistenteModal").modal();
		}
     },
    error: function (xhr, status, error) {
		$("#noConexionModal").modal();
    }
    });
}

/*######################## FUNCIONES MODIFICACION ########################*/

function verFechaModificar(fecha)
{
	$.ajax({
    type: "POST",
	url: "php/rrhhVerFecha.php",
	dataType: "json",
	data: {'fecha' : fecha},
	success: function (result) {
		if (result == false) 
		{
			$("#sinFechaModal").modal();
		}
		else
		{
			$("#contraseniaModificar").modal();
		}
     },
    error: function (xhr, status, error) {
    	$("#noConexionModal").modal();
    }
    });
}


function validarContraseniaModificar()
{

	/*Input con la contrasenia*/
	var inputContrasenia = document.getElementById("prodContrInput");

	/*Hashea la contrasenia*/
	var contraseniaHash = sha1(document.getElementById("prodContrInput").value);

	/*Mensaje de error*/
	var spanErrorMsj = document.getElementsByClassName("prodContraseniaSpanMsj");

	$.ajax({
    type: "POST",
	url: "php/validarAdmin.php",
	dataType: "json",
	data: {'password' : contraseniaHash},
	success: function (result) {
		if (result == false) 
		{
			spanErrorMsj[0].classList.remove("oculto");
		}
		else
		{
			/*Esconce del mensaje de error, por si estaba.*/
			spanErrorMsj[0].classList.add("oculto");

			/*Cierra el modal*/
			$("#contraseniaModificar").modal("hide");


			/*Agarra la fecha y la formatea para despues pedir los datos a la BDD*/
			var fecha = document.getElementById("rrhhFechaInput");
			var fechaF = addDays(new Date(fecha.value),1);
			var fechaLatina = fechaF.getDate() + "/" + (fechaF.getMonth()+1) + "/" + fechaF.getFullYear();

			/*Funcion que completa los campos y llama al modal*/
			modalCrearModificar(contraseniaHash);

			// Borro lo que quedaba en el campo, sino se autocompleta.
			}
     },
    error: function (xhr, status, error) {
		$("#noConexionModal").modal();
    }
    });
	inputContrasenia.value = "";
}


function modalCrearModificar(contraseniaHash)
{

	// El PHP tiene que validar otra vez la contrasenia antes de devolver la consulta. 

	// Llama a un ajax y pide los datos en la BDD
	// Si no funciona tira el mensaje de error y se frena la funcion
	// Si funciona pone todos los campos
	var fecha = document.getElementById("rrhhFechaInput").value;
	var fechaF = addDays(new Date(fecha),1);
	var fechaLatina = fechaF.getDate() + "/" + (fechaF.getMonth()+1) + "/" + fechaF.getFullYear();
	var asistenciaEsperada = document.getElementById("inputRRHHEsperada").value;
	var asistenciaReal = document.getElementById("inputRRHHReal").value;

	$.ajax({
	    type: "POST",
		url: "php/rrhhTraerFecha.php",
		dataType: "json",
		data: {'fecha' : fecha, 'password' : contraseniaHash},
		success: function (result) 
		{
			var asist_esperada = result.asist_esperada;
			var asist_real = result.asist_real;
			document.getElementById("rrhhTituloFechaModificar").innerHTML = fechaLatina;
			document.getElementById("rrhhEsperadaFechaModificar").innerHTML = asistenciaEsperada;
			document.getElementById("rrhhRealFechaModificar").innerHTML =  asistenciaReal;
			document.getElementById("rrhhAsistenciaEsperadaAnterior").innerHTML = asist_esperada;
			document.getElementById("rrhhAsistenciaRealAnterior").innerHTML =  asist_real;
			$("#editarDatos").modal();
			
	     },
	    error: function (xhr, status, error) {
    		$("#noConexionModal").modal();
    	}
    });
}


function modificarAsistencia(fecha, asistenciaReal, asistenciaEsperada)
{
	$.ajax({
    type: "POST",
	url: "php/modificarAsistencia.php",
	dataType: "json",
	data: {'fecha' : fecha, 'asistenciaReal' : asistenciaReal, 'asistenciaEsperada' : asistenciaEsperada},
	success: function (result) 
	{
		$("#asistenciaCargadaModal").modal();
    },
    error: function (xhr, status, error) {
    	$("#noConexionModal").modal();
    }
    });
}

function modificarAsistenciaLaunch()
{
	var fecha = document.getElementById("rrhhFechaInput").value;
	var asistenciaEsperada = document.getElementById("inputRRHHEsperada").value;
	var asistenciaReal = document.getElementById("inputRRHHReal").value;
	modificarAsistencia(fecha, asistenciaReal, asistenciaEsperada);
}

/*######################## FUNCIONES BAJA ########################*/

function modalCrearBorrar(contrasenia)
{
	var fecha = document.getElementById("rrhhFechaInput").value;
	var fechaF = addDays(new Date(fecha),1);
	var fechaLatina = fechaF.getDate() + "/" + (fechaF.getMonth()+1) + "/" + fechaF.getFullYear();

	$.ajax({
	    type: "POST",
		url: "php/rrhhTraerFecha.php",
		dataType: "json",
		data: {'fecha' : fecha, 'password' : contrasenia},
		success: function (result) 
		{
			var asist_esperada = result.asist_esperada;
			var asist_real = result.asist_real;
			document.getElementById("rrhhFechaActualBorrar").innerHTML = fechaLatina;
			document.getElementById("rrhhAsistenciaActualEsperadaBorrar").innerHTML = asist_esperada;
			document.getElementById("rrhhAsistenciaRealActualBorrar").innerHTML =  asist_real;
			$("#borrarDatos").modal();
			
	     },
	    error: function (xhr, status, error) {
    		$("#noConexionModal").modal();
    	}
    });

}

function verFechaBorrar(fecha)
{
	$.ajax({
	    type: "POST",
		url: "php/rrhhVerFecha.php",
		dataType: "json",
		data: {'fecha' : fecha},
		success: function (result) {
			if (result == false) 
			{
				$("#sinFechaModal").modal();
			}
			else
			{
				$("#contraseniaBorrar").modal();
			}
	     },
	    error: function (xhr, status, error) {
			$("#noConexionModal").modal();
	    }
    });
}
/*SI NO SELECCIONO FECHA MUESTRA ERROR*/

function validarBorrar()
{
	var inputFecha = document.getElementById("rrhhFechaInput");
	var errorFecha = document.getElementById("rrhhErrorSpanFecha");
	if (!inputFecha.value) 
		{
			inputFecha.classList.add("campoInvalido");
			errorFecha.classList.remove("oculto");
		}
		else
		{
			inputFecha.classList.remove("campoInvalido");
			errorFecha.classList.add("oculto");
			verFechaBorrar(inputFecha.value);
		}
}


function validarContraseniaBorrar()
{

	/*Input con la contrasenia*/
	var inputContrasenia = document.getElementById("prodContrInputBorrar");

	/*Hashea la contrasenia*/
	var contraseniaHash = sha1(document.getElementById("prodContrInputBorrar").value);

	/*Mensaje de error*/
	var spanErrorMsj = document.getElementsByClassName("prodContraseniaSpanMsj");

	$.ajax({
    type: "POST",
	url: "php/validarAdmin.php",
	dataType: "json",
	data: {'password' : contraseniaHash},
	success: function (result) {
		if (result == false) 
		{
			spanErrorMsj[1].classList.remove("oculto");
		}
		else
		{
			/*Esconce del mensaje de error, por si estaba.*/
			spanErrorMsj[0].classList.add("oculto");

			/*Cierra el modal*/
			$("#contraseniaBorrar").modal("hide");


			/*Agarra la fecha y la formatea para despues pedir los datos a la BDD*/
			var fecha = document.getElementById("rrhhFechaInput");
			var fechaF = addDays(new Date(fecha.value),1);
			var fechaLatina = fechaF.getDate() + "/" + (fechaF.getMonth()+1) + "/" + fechaF.getFullYear();

			/*Funcion que completa los campos y llama al modal*/
			modalCrearBorrar(contraseniaHash);
		}
     },
    error: function (xhr, status, error) {
		$("#noConexionModal").modal();
    	}
    });
	/*Borron contraseña que sino queda en el campo*/
	inputContrasenia.value = "";
}

//Aca se ejecutan las consultas SQL para "borrar"//
function borrarAsistencia(fecha)
{
	$.ajax({
    type: "POST",
	url: "php/borrarAsistencia.php",
	dataType: "json",
	data: {'fecha' : fecha},
	success: function (result) 
	{
		document.getElementById("rrhhIconCheck").classList.add("oculto");
    	document.getElementById("rrhhIconCancel").classList.remove("oculto");
		$("#asistenciaBorradaModal").modal();
    },
    error: function (xhr, status, error) {
    	$("#noConexionModal").modal();
    }
    });
}

function borrarAsistenciaLaunch()
{
	var fecha = document.getElementById("rrhhFechaInput").value;
	var asistenciaEsperada = document.getElementById("inputRRHHEsperada").value;
	var asistenciaReal = document.getElementById("inputRRHHReal").value;
	borrarAsistencia(fecha);
}


/*######################## FUNCIONES GLOBLAES ########################*/

/*Esta funcion valida los campos al tocar los botones de fecha y modificacion
depende cual presionó llama a dos funciones distintas*/
function validarRRHH(modal)
{
	var inputEsp = document.getElementById("inputRRHHEsperada");
	var inputReal = document.getElementById("inputRRHHReal");
	var msgErrorProporcion = document.getElementById("sdoErrorSpanProporcion");

	var arrayCamposNumeros = document.getElementsByClassName("inputRRHH");
	var reg = /^[0-9]{1,15}$/;
	var msgErrorNum = document.getElementById("sdoErrorSpanCampos");

	var inputFecha = document.getElementById("rrhhFechaInput");
	var errorFecha = document.getElementById("rrhhErrorSpanFecha");

	/*Valida que la asistencia real no sea mayor a la esperada*/
	if (parseInt(inputEsp.value) < parseInt(inputReal.value)) 
	{
		msgErrorProporcion.classList.remove("oculto");
	}
	else
	{
		/*valida que ningun campo este en blanco*/
		msgErrorProporcion.classList.add("oculto");
		if (inputReal.value == "" || inputEsp.value == "") 
		{
			msgErrorNum.classList.remove("oculto");
		}
		else
		{
			/*Valida que los campos tengan numeros*/
			match1 = inputEsp.value.match(reg);
			if (!match1) 
			{
				msgErrorNum.classList.remove("oculto");
			}
			else
			{
				match2 = inputReal.value.match(reg);
				if (!match2) 
				{
					msgErrorNum.classList.remove("oculto");
				}
				else
				{
					/*Valida la fecha*/
					if (!inputFecha.value) 
					{
						inputFecha.classList.add("campoInvalido");
						errorFecha.classList.remove("oculto");
					}
					else
					{
						inputFecha.classList.remove("campoInvalido");
						errorFecha.classList.add("oculto");
						msgErrorNum.classList.add("oculto");
						
						if (modal == '#cargarDatos')
						{
							verFechaCargar(inputFecha.value);
						}
						else
						{

							verFechaModificar(inputFecha.value);
						}
						
					}
				}
			}
		}
	}
}

/*######################## FUNCIONES QUE PONE VALIDACIONES A LOS CAMPOS EN TIEMPO REAL ########################*/

/*Valida que solo haya numeros enteros y puntos*/
function ponerValidacionACampos()
{
	var arrayCamposNumeros = document.getElementsByClassName("inputRRHH");
	var msgErrorNum = document.getElementById("sdoErrorSpanCampos");
	var reg = /^[0-9]{1,15}$/;
	for (var i = 0; i<arrayCamposNumeros.length; i++)
	{
		arrayCamposNumeros[i].onkeyup = function()
		{
			var match = this.value.match(reg);
			if (!match) 
			{
				if(this.value == "")
				{
	    			this.classList.remove("campoInvalido");
	    			msgErrorNum.classList.add("oculto");
				}

				else
				{
	    			this.classList.add("campoInvalido");
	    			msgErrorNum.classList.remove("oculto");
				}
			}
			else
			{
	    		this.classList.remove("campoInvalido");
    			msgErrorNum.classList.add("oculto");

			}

		}
	}
}

function addDays(date, days) {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}   

//Para ver una tabla con los datos del mes actual elegido
function verDatos()
{
	var fecha = addDays(new Date(document.getElementById("rrhhFechaInput").value),1);
	mes = fecha.getMonth() + 1;
	anio = fecha.getFullYear();
	$.ajax({
    type: "POST",
	url: "php/RRHHOjito.php",
	dataType: "json",
	data: {'mes' : mes, 'anio' : anio},
	success: function (result) 
	{
		$("#registrosMesModal").modal();
		$(".filitasUsuario").remove();
		var registros = result;
		$(registros).each(function()
		{
			var fila = '<tr class="filitasUsuario">'+
			'<td class="filtrable">'+this.asist_esperada+'</td>'+
			'<td class="filtrable">'+this.asist_real+'</td>'+
			'<td class="filtrable">'+this.fecha+'</td>';
			$("#tablaRRHH").append(fila);
		})


    },
    error: function (xhr, status, error) {
    	$("#noConexionModal").modal();
    }
    });
}

