filtrarTabla();
cargarTablaUsuarios();

// Funcion para que el enter 
$("form").bind("keypress", function(e) {
   if (e.keyCode == 13) {
      your_custom_submit_function();
      return false; // ignore the default event
   }
});

function filtrarTabla(){
  $("#inputAdminFiltro").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".filitasUsuario").filter(function() {
      $(this).toggle($(this).children().filter($(".filtrable")).text().toLowerCase().indexOf(value) > -1)
    });
  });
};


$("#modalModificarUsuario").modal();

function limpiarForm()
{
	document.getElementById("formAgregarUser").reset();
}


function cargarTablaUsuarios()
{
	$.ajax({
    type: "POST",
	url: "php/adminSelectUsuarios.php",
	dataType: "json",
	data: null,
	success: function (result)
	{
		$(".filitasUsuario").remove();
		var usuarios = result
		$(usuarios).each(function()
		{
			var fila = '<tr class="filitasUsuario">'+
			'<td class="filtrable">'+this.usuario+'</td>'+
			'<td class="filtrable">'+this.recordatorio+'</td>'+
			'<td class="esteNo"><button class="boton-tabla btn btn-primary" onclick="verDatos('+this.id_user+')">Ver</button><button class="boton-tabla botonito btn-warning" onclick="modDatos('+this.id_user+')">Modificar</button><button class="boton-tabla botonito btn-danger" onclick="borrarDatos('+this.id_user+')">Eliminar</button></td></tr>';
			$("#tablaUsuarios").append(fila);
		})
		document.getElementById("inputAdminFiltro").value = "";
    },
    error: function (xhr, status, error) {
    	$("#noConexionModal").modal();
    }
    });
};


function agregarUsuario(usuario, contrasenia, recordatorio, p_admin,
	p_prod, p_rrhh, p_sdo, p_ing_min, p_ing_may, p_ing_ext, p_egr, p_egr_ext, p_ret, p_acob, p_apag, p_sfr)
{
	$.ajax({
    type: "POST",
	url: "php/adminInsertarUsuario.php",
	dataType: "json",
	data: {'usuario' : usuario,
	'contrasenia' : contrasenia,
	 'recordatorio' : recordatorio,
	 'p_admin' : p_admin,
	 'p_prod' : p_prod,
	 'p_rrhh' : p_rrhh,
	 'p_sdo' : p_sdo,
	 'p_ing_min' : p_ing_min,
	 'p_ing_may' : p_ing_may,
	 'p_ing_ext' : p_ing_ext,
	 'p_egr' : p_egr,
	 'p_egr_ext' : p_egr_ext,
	 'p_ret' : p_ret,
	 'p_acob' : p_acob,
	 'p_apag' : p_apag,
	 'p_sfr' : p_sfr},
	success: function (result) 
	{
		$("#usuarioCargadoModal").modal();
    },
    error: function (xhr, status, error, result) {
    	$("#noConexionModal").modal();
    }
    });
    document.getElementById("formAgregarUser").reset();
};

function agregarUsuarioLaunch(){
	var usuario = document.getElementById("usuario").value;
	var contrasenia = sha1(document.getElementById("contrasenia").value);
	var recordatorio = document.getElementById("recordatorio").value;
	var p_adminChckBox = document.getElementById("p_admin");
	var p_prodChckBox = document.getElementById("p_prod");
	var p_rrhhChckBox = document.getElementById("p_rrhh");
	var p_sdoChckBox = document.getElementById("p_sdo");
	var p_ing_minChckBox = document.getElementById("p_ing_min");
	var p_ing_mayChckBox = document.getElementById("p_ing_may");
	var p_ing_extChckBox = document.getElementById("p_ing_ext");
	var p_egrChckBox = document.getElementById("p_egr");
	var p_egrExtChckBox = document.getElementById("p_egr_ext");
	var p_retChckBox = document.getElementById("p_ret");
	var p_acobChckBox = document.getElementById("p_acob");
	var p_apagChckBox = document.getElementById("p_apag");
	var p_sfrChckBox = document.getElementById("p_sfr");

	var p_admin = 0;
	var p_prod = 0;
	var p_rrhh = 0;
	var p_sdo = 0;
	var p_ing_min = 0;
	var p_ing_may = 0;
	var p_ing_ext = 0;
	var p_egr = 0;
	var p_egr_ext = 0;
	var p_ret = 0;
	var p_acob = 0;
	var p_apag = 0;
	var p_sfr = 0;

	if (p_adminChckBox.checked){
		p_admin = 1;
	}	

	if (p_prodChckBox.checked){
		p_prod = 1;

	}	
	if (p_rrhhChckBox.checked){
		p_rrhh = 1;

	}	
	if (p_sdoChckBox.checked){
		p_sdo = 1;

	}	
	if (p_ing_minChckBox.checked){
		p_ing_min = 1;

	}	
	if (p_ing_mayChckBox.checked){
		p_ing_may = 1;

	}	
	if (p_ing_extChckBox.checked){
		p_ing_ext = 1;

	}	
	if (p_egrChckBox.checked){
		p_egr = 1;

	}		
	if (p_egrExtChckBox.checked){
		p_egr_ext = 1;
	}	

	if (p_retChckBox.checked){
		p_ret = 1;
	}	

	if (p_acobChckBox.checked){
		p_acob = 1;

	}	
	if (p_apagChckBox.checked){
		p_apag = 1;

	}	
	if (p_sfrChckBox.checked){
		p_sfr = 1;
	}

	agregarUsuario(usuario, contrasenia, recordatorio, p_admin, p_prod, p_rrhh, p_sdo, p_ing_min, p_ing_may, p_ing_ext, p_egr, p_egr_ext, p_ret, p_acob, p_apag, p_sfr);
}
/*VALIDACIONES DE USUARIOS YA EXISTENTES*/
function checkearUsuario()
{
	var usuario = document.getElementById("usuario").value;
	var contrasenia = document.getElementById("contrasenia").value;
	var msgErrorCampos = document.getElementById("spanErrorCargarCampos");
	var msgErrorExiste = document.getElementById("spanErrorCargarExiste");

	// Se fija que haya aunque sea un caracter en los campos de user y password que no sea en blanco
	if (!/\S/.test(usuario) || !/\S/.test(contrasenia)) 
	{
		msgErrorExiste.classList.add("oculto");
		msgErrorCampos.classList.remove("oculto");
	}
	else
	{
		$.ajax({
	    type: "POST",
		url: "php/adminUsuarioYaExiste.php",
		dataType: "json",
		data: {'usuario' : usuario},
		success: function (result) 
		{
			msgErrorExiste.classList.add("oculto");
			msgErrorCampos.classList.add("oculto");
			agregarUsuarioLaunch();
			$('#agregarUsuario').modal('hide');
	    },
	    error: function (xhr, status, error) {
			msgErrorCampos.classList.add("oculto");
			msgErrorExiste.classList.remove("oculto");
	    	//$("#usuarioYaExisteModal").modal();
	    }
	    });
	}
}	

function checkearUsuarioModificar()
{
	var id_user = document.getElementById("formModificarIdOculto").value;

	var usuario = document.getElementById("modUsuario").value;
	var contrasenia = document.getElementById("modContrasenia").value;

	var checkContrasenia = document.getElementById("checkNuevaContra");

	var msgErrorCampos = document.getElementById("spanErrorModificarCampos");
	var msgErrorExiste = document.getElementById("spanErrorModificarExiste");

	// Ve que el campo usuario no este en blanco
	if (/\S/.test(usuario))
	{
		console.log("usuario validado");
		msgErrorCampos.classList.add("oculto");	
		if (checkContrasenia.checked == true) 
		{	
			// Ve si el campo contrasenia esta en blanco
			if (!/\S/.test(contrasenia))
			{
				var test = /^\s+$/.test(contrasenia);
				console.log(test);
				console.log(contrasenia);
				console.log("campo contra blanco")
				msgErrorExiste.classList.add("oculto");
				msgErrorCampos.classList.remove("oculto");
			}
			else
			{
				msgErrorCampos.classList.add("oculto");
				$.ajax(
				{
				    type: "POST",
					url: "php/adminUsuarioYaExisteModificar.php",
					dataType: "json",
					data: {'id_user' : id_user, 'usuario' : usuario},
					success: function (result) 
					{
						msgErrorExiste.classList.add("oculto");
						modificarQueCosa();
						$("#modalModUsuario").modal("hide");
						console.log(result);
				    },
				    error: function (xhr, status, error,result) 
				    {
						msgErrorExiste.classList.remove("oculto");
				    	//$("#usuarioYaExisteModal").modal();
				    	console.log(xhr);
				    }
			    });
			}
		}
		else
		{
			msgErrorCampos.classList.add("oculto");
			$.ajax(
			{
			    type: "POST",
				url: "php/adminUsuarioYaExisteModificar.php",
				dataType: "json",
				data: {'id_user' : id_user, 'usuario' : usuario},
				success: function (result) 
				{
					msgErrorExiste.classList.add("oculto");
					modificarQueCosa();
					$("#modalModUsuario").modal("hide");
					console.log(result);
			    },
			    error: function (xhr, status, error,result) 
			    {
					msgErrorExiste.classList.remove("oculto");
			    	//$("#usuarioYaExisteModal").modal();
			    	console.log(xhr);
			    }
		    });
		}
	}
	else
	{
		msgErrorExiste.classList.add("oculto");
		msgErrorCampos.classList.remove("oculto");	
	}
}

/*	// Se fija que haya aunque sea un caracter en los campos de user y password
	var msgError = document.getElementById("spanErrorAdminCargar");
	if (!/\S/.test(usuario) || !/\S/.test(contrasenia)) 
	{
		//msgError.classList.remove("oculto");
		msgError.style.display = null;
	}	
	else
	{
		("epa")
		if (true) 
		{
			//mensaje error ya existe el nombre
		}
		else
		{
			cargarTablaUsuarios()
			agregarUsuario(usuario, contrasenia, recordatorio, p_admin, p_prod, p_rrhh, p_sdo, p_ing_min, p_ing_may, p_egr, p_ret, p_acob, p_apag, p_sfr);
			$("#agregarUsuario").modal("hide");
		}
	}
}
*/

/*Se fija si el boton check de saldo esta activo o no para activar o desactivar los campos referentes a esos campos
en la modal*/
function checkSaldoAgregar()
{
	var checkSdo = document.getElementById("p_sdo");
	var campos = document.getElementsByClassName("checksSdoAgregar");
	var texto = document.getElementsByClassName("agregar-texto-checkbox");

	if (p_sdo.checked == true)
	{
		for (var i = campos.length - 1; i >= 0; i--) {
			campos[i].disabled = false;
			texto[i].classList.remove("gris");
		}
	}
	else
	{
		for (var i = campos.length - 1; i >= 0; i--) {
			campos[i].disabled = true;
			texto[i].classList.add("gris");
		}
	}	
};

function checkSaldoModificar()
{
	var checkSdo = document.getElementById("mod_p_sdo");
	var campos = document.getElementsByClassName("checksSdoMod");
	var texto = document.getElementsByClassName("mod-texto-checkbox")

	if (mod_p_sdo.checked == true)
	{
		for (var i = campos.length - 1; i >= 0; i--) {
			campos[i].disabled = false;
			texto[i].classList.remove("gris");
		}
	}
	else
	{
		for (var i = campos.length - 1; i >= 0; i--) {
			campos[i].disabled = true;
			texto[i].classList.add("gris");
		}
	}	
};


// Funcion que trae los datos desde la BDD cuando se hace click en el boton VER y 
// Arma el modal
function verDatos(id_user)
{
	$.ajax({
    type: "POST",
	url: "php/adminVerUsuario.php",
	dataType: "json",
	data: {'id_user' : id_user},
	success: function (result) 
	{
		// Completa los datos de la modal
		document.getElementById("verUsuario").innerHTML = result.usuario;
		document.getElementById("verRecordatorio").innerHTML = result.recordatorio;
		document.getElementById("ver_p_admin").checked = parseInt(result.p_admin);
		document.getElementById("ver_p_prod").checked = parseInt(result.p_prod);
		document.getElementById("ver_p_rrhh").checked = parseInt(result.p_rrhh);
		document.getElementById("ver_p_sdo").checked = parseInt(result.p_sdo);
		document.getElementById("ver_p_ing_min").checked = parseInt(result.p_ing_min);
		document.getElementById("ver_p_ing_may").checked = parseInt(result.p_ing_may);
		document.getElementById("ver_p_ing_ext").checked = parseInt(result.p_ing_ext);
		document.getElementById("ver_p_egr").checked = parseInt(result.p_egr);
		document.getElementById("ver_p_egr_ext").checked = parseInt(result.p_egr_ext);
		document.getElementById("ver_p_ret").checked = parseInt(result.p_ret);
		document.getElementById("ver_p_acob").checked = parseInt(result.p_acob);
		document.getElementById("ver_p_apag").checked = parseInt(result.p_apag);
		document.getElementById("ver_p_sfr").checked = parseInt(result.p_sfr);

		$("#modalVerUsuario").modal();
    },
    error: function (xhr, status, error, result) {
    	$("#noConexionModal").modal();
    }
    });
}

function modDatos(id_user)
{
	document.getElementById("formModificarUser").reset();
	// Si clickeó el chechbox antes queda activado, con esto lo vuelve a su estado natural
	document.getElementById("modContrasenia").disabled = true;
	
	var campos = document.getElementsByClassName("checksSdoMod");
	var texto = document.getElementsByClassName("mod-texto-checkbox");
	$.ajax({
    type: "POST",
	url: "php/adminVerUsuario.php",
	dataType: "json",
	data: {'id_user' : id_user},
	success: function (result) 
	{
		// Completa los datos de la modal
		document.getElementById("modUsuario").value = result.usuario;
		document.getElementById("modRecordatorio").value = result.recordatorio;
		document.getElementById("mod_p_admin").checked = parseInt(result.p_admin);
		document.getElementById("mod_p_prod").checked = parseInt(result.p_prod);
		document.getElementById("mod_p_rrhh").checked = parseInt(result.p_rrhh);
		document.getElementById("mod_p_sdo").checked = parseInt(result.p_sdo);
		if (parseInt(result.p_sdo) == 1) 
		{
			for (var i = campos.length - 1; i >= 0; i--) 
            {
				campos[i].disabled = false;
				texto[i].classList.remove("gris");
			}
		}
		else
		{
			for (var i = campos.length - 1; i >= 0; i--) 
    		{
				campos[i].disabled = true;
				texto[i].classList.add("gris");
			}
		}
		document.getElementById("mod_p_ing_min").checked = parseInt(result.p_ing_min);
		document.getElementById("mod_p_ing_may").checked = parseInt(result.p_ing_may);
		document.getElementById("mod_p_ing_ext").checked = parseInt(result.p_ing_ext);
		document.getElementById("mod_p_egr").checked = parseInt(result.p_egr);
		document.getElementById("mod_p_egr_ext").checked = parseInt(result.p_egr_ext);
		document.getElementById("mod_p_ret").checked = parseInt(result.p_ret);
		document.getElementById("mod_p_acob").checked = parseInt(result.p_acob);
		document.getElementById("mod_p_apag").checked = parseInt(result.p_apag);
		document.getElementById("mod_p_sfr").checked = parseInt(result.p_sfr);
		document.getElementById("formModificarIdOculto").value = id_user;
		$("#modalModUsuario").modal();
    },
    error: function (xhr, status, error, result) {
    	$("#noConexionModal").modal();
    }
    });
}

function borrarDatos(id_user)
{
	$.ajax({
    type: "POST",
	url: "php/adminVerUsuario.php",
	dataType: "json",
	data: {'id_user' : id_user},
	success: function (result) 
	{
		// Completa los datos de la modal
		document.getElementById("borrarUsuario").innerHTML = result.usuario;
		document.getElementById("borrarRecordatorio").innerHTML = result.recordatorio;
		document.getElementById("borrar_p_admin").checked = parseInt(result.p_admin);
		document.getElementById("borrar_p_prod").checked = parseInt(result.p_prod);
		document.getElementById("borrar_p_rrhh").checked = parseInt(result.p_rrhh);
		document.getElementById("borrar_p_sdo").checked = parseInt(result.p_sdo);
		document.getElementById("borrar_p_ing_min").checked = parseInt(result.p_ing_min);
		document.getElementById("borrar_p_ing_may").checked = parseInt(result.p_ing_may);
		document.getElementById("borrar_p_ing_ext").checked = parseInt(result.p_ing_ext);
		document.getElementById("borrar_p_egr").checked = parseInt(result.p_egr);
		document.getElementById("borrar_p_egr_ext").checked = parseInt(result.p_egr_ext);
		document.getElementById("borrar_p_ret").checked = parseInt(result.p_ret);
		document.getElementById("borrar_p_acob").checked = parseInt(result.p_acob);
		document.getElementById("borrar_p_apag").checked = parseInt(result.p_apag);
		document.getElementById("borrar_p_sfr").checked = parseInt(result.p_sfr);
		document.getElementById("formBorrarIdOculto").value = id_user;
		$("#modalBorrarUsuario").modal();
    },
    error: function (xhr, status, error, result) {
    	$("#noConexionModal").modal();
    }
    });
}

// Por si toca el boton de poner nueva contrasenia al modal de modificar
function nuevaContrasenia()
{

	if (document.getElementById("checkNuevaContra").checked) 
	{
		document.getElementById("modContrasenia").disabled = false;
	}
	else
	{
		document.getElementById("modContrasenia").disabled = true;
	}
}

function modificarUsuarioContraseniaLaunch()
{
	var contrasenia = sha1(document.getElementById("modContrasenia").value);
	var usuario = document.getElementById("modUsuario").value;
	var recordatorio = document.getElementById("modRecordatorio").value;
	var p_adminChckBox = document.getElementById("mod_p_admin");
	var p_prodChckBox = document.getElementById("mod_p_prod");
	var p_rrhhChckBox = document.getElementById("mod_p_rrhh");
	var p_sdoChckBox = document.getElementById("mod_p_sdo");
	var p_ing_minChckBox = document.getElementById("mod_p_ing_min");
	var p_ing_mayChckBox = document.getElementById("mod_p_ing_may");
	var p_ing_extChckBox = document.getElementById("mod_p_ing_ext");
	var p_egrChckBox = document.getElementById("mod_p_egr");
	var p_egrExtChckBox = document.getElementById("mod_p_egr_ext");
	var p_retChckBox = document.getElementById("mod_p_ret");
	var p_acobChckBox = document.getElementById("mod_p_acob");
	var p_apagChckBox = document.getElementById("mod_p_apag");
	var p_sfrChckBox = document.getElementById("mod_p_sfr");
	var id_user = document.getElementById("formModificarIdOculto").value;

	var p_admin = 0;
	var p_prod = 0;
	var p_rrhh = 0;
	var p_sdo = 0;
	var p_ing_min = 0;
	var p_ing_may = 0;
	var p_ing_ext = 0;
	var p_egr = 0;
	var p_egr_ext = 0;
	var p_ret = 0;
	var p_acob = 0;
	var p_apag = 0;
	var p_sfr = 0;


	if (p_adminChckBox.checked){
		p_admin = 1;
	}	

	if (p_prodChckBox.checked){
		p_prod = 1;

	}	
	if (p_rrhhChckBox.checked){
		p_rrhh = 1;

	}	
	if (p_sdoChckBox.checked){
		p_sdo = 1;

	}	
	if (p_ing_minChckBox.checked){
		p_ing_min = 1;

	}	
	if (p_ing_mayChckBox.checked){
		p_ing_may = 1;

	}		
	if (p_ing_extChckBox.checked){
		p_ing_ext = 1;

	}	
	if (p_egrChckBox.checked){
		p_egr = 1;

	}		
	if (p_egrExtChckBox.checked){
		p_egr_ext = 1;

	}	
	if (p_retChckBox.checked){
		p_ret = 1;

	}	
	if (p_acobChckBox.checked){
		p_acob = 1;

	}	
	if (p_apagChckBox.checked){
		p_apag = 1;

	}	
	if (p_sfrChckBox.checked){
		p_sfr = 1;
	}
	modificarUsuarioContrasenia(usuario, contrasenia, recordatorio, p_admin, p_prod, p_rrhh, p_sdo, p_ing_min, p_ing_may, p_ing_ext, p_egr, p_egr_ext, p_ret, p_acob, p_apag, p_sfr, id_user);
}

function modificarUsuarioContrasenia(usuario, contrasenia, recordatorio, p_admin,
	p_prod, p_rrhh, p_sdo, p_ing_min, p_ing_may, p_ing_ext, p_egr, p_egr_ext, p_ret, p_acob, p_apag, p_sfr, id_user)
{
	$.ajax({
    type: "POST",
	url: "php/adminModificarUsuarioContrasenia.php",
	dataType: "json",
	data: {'usuario' : usuario,
	'contrasenia' : contrasenia,
	 'recordatorio' : recordatorio,
	 'p_admin' : p_admin,
	 'p_prod' : p_prod,
	 'p_rrhh' : p_rrhh,
	 'p_sdo' : p_sdo,
	 'p_ing_min' : p_ing_min,
	 'p_ing_may' : p_ing_may,
	 'p_ing_ext' : p_ing_ext,
	 'p_egr' : p_egr,
	 'p_egr_ext' : p_egr_ext,
	 'p_ret' : p_ret,
	 'p_acob' : p_acob,
	 'p_apag' : p_apag,
	 'p_sfr' : p_sfr,
	 'id_user' : id_user},
	success: function (result) 
	{
		$("#usuarioModificadoModal").modal();
    },
    error: function (xhr, status, error, result) {
    	$("#noConexionModal").modal();
    }
    });
    document.getElementById("formModificarUser").reset();
};


function modificarUsuarioLaunch()
{
	var usuario = document.getElementById("modUsuario").value;
	var recordatorio = document.getElementById("modRecordatorio").value;
	var p_adminChckBox = document.getElementById("mod_p_admin");
	var p_prodChckBox = document.getElementById("mod_p_prod");
	var p_rrhhChckBox = document.getElementById("mod_p_rrhh");
	var p_sdoChckBox = document.getElementById("mod_p_sdo");
	var p_ing_minChckBox = document.getElementById("mod_p_ing_min");
	var p_ing_mayChckBox = document.getElementById("mod_p_ing_may");
	var p_ing_extChckBox = document.getElementById("mod_p_ing_ext");
	var p_egrChckBox = document.getElementById("mod_p_egr");
	var p_egrExtChckBox = document.getElementById("mod_p_egr_ext");
	var p_retChckBox = document.getElementById("mod_p_ret");
	var p_acobChckBox = document.getElementById("mod_p_acob");
	var p_apagChckBox = document.getElementById("mod_p_apag");
	var p_sfrChckBox = document.getElementById("mod_p_sfr");
	var id_user = document.getElementById("formModificarIdOculto").value;

	var p_admin = 0;
	var p_prod = 0;
	var p_rrhh = 0;
	var p_sdo = 0;
	var p_ing_min = 0;
	var p_ing_may = 0;
	var p_ing_ext = 0;
	var p_egr = 0;
	var p_egr_ext = 0;
	var p_ret = 0;
	var p_acob = 0;
	var p_apag = 0;
	var p_sfr = 0;


	if (p_adminChckBox.checked){
		p_admin = 1;
	}	

	if (p_prodChckBox.checked){
		p_prod = 1;

	}	
	if (p_rrhhChckBox.checked){
		p_rrhh = 1;

	}	
	if (p_sdoChckBox.checked){
		p_sdo = 1;

	}	
	if (p_ing_minChckBox.checked){
		p_ing_min = 1;

	}	
	if (p_ing_mayChckBox.checked){
		p_ing_may = 1;

	}	
	if (p_ing_extChckBox.checked){
		p_ing_ext = 1;

	}	
	if (p_egrChckBox.checked){
		p_egr = 1;

	}	
	if (p_egrExtChckBox.checked){
		p_egr_ext = 1;

	}	
	if (p_retChckBox.checked){
		p_ret = 1;

	}	
	if (p_acobChckBox.checked){
		p_acob = 1;

	}	
	if (p_apagChckBox.checked){
		p_apag = 1;

	}	
	if (p_sfrChckBox.checked){
		p_sfr = 1;
	}
	modificarUsuario(usuario, recordatorio, p_admin, p_prod, p_rrhh, p_sdo, p_ing_min, p_ing_may, p_ing_ext, p_egr, p_egr_ext, p_ret, p_acob, p_apag, p_sfr, id_user);
}

function modificarUsuario(usuario, recordatorio, p_admin,
	p_prod, p_rrhh, p_sdo, p_ing_min, p_ing_may, p_ing_ext, p_egr, p_egr_ext, p_ret, p_acob, p_apag, p_sfr, id_user)
{
	$.ajax({
    type: "POST",
	url: "php/adminModificarUsuario.php",
	dataType: "json",
	data: {'usuario' : usuario,
	 'recordatorio' : recordatorio,
	 'p_admin' : p_admin,
	 'p_prod' : p_prod,
	 'p_rrhh' : p_rrhh,
	 'p_sdo' : p_sdo,
	 'p_ing_min' : p_ing_min,
	 'p_ing_may' : p_ing_may,
	 'p_ing_ext' : p_ing_ext,
	 'p_egr' : p_egr,
	 'p_egr_ext' : p_egr_ext,
	 'p_ret' : p_ret,
	 'p_acob' : p_acob,
	 'p_apag' : p_apag,
	 'p_sfr' : p_sfr,
	 'id_user' : id_user},
	success: function (result) 
	{
		$("#usuarioModificadoModal").modal();
    },
    error: function (xhr, status, error, result) {
    	$("#noConexionModal").modal();
    	console.log(xhr)
    	console.log(error)
    	console.log()
    }
    });
    document.getElementById("formModificarUser").reset();
};


function modificarQueCosa()
{
	var checkeado = document.getElementById("checkNuevaContra").checked;
	(checkeado);
	if (checkeado == true)
	{
		modificarUsuarioContraseniaLaunch();
	}
	else if (checkeado == false)
	{
		modificarUsuarioLaunch();
	}
}


function borrarUsuario(id_user)
{
	$.ajax({
    type: "POST",
	url: "php/adminBorrarUsuario.php",
	dataType: "json",
	data: {'id_user' : id_user},
	success: function (result) 
	{
		$("#usuarioBorradoModal").modal();
    },
    error: function (xhr, status, error, result) {
    	$("#noConexionModal").modal();
    }
    });
};

function borrarUsuarioLaunch()
{
	var id_user = document.getElementById("formBorrarIdOculto").value;
	borrarUsuario(id_user);
}
