ponerValidacionAFecha();
ponerValidacionACampos();


/*Valida que sea lunes*/
function ponerValidacionAFecha()
{
	var fecha = document.getElementById("sdoFechaInput");
	var fechaError = document.getElementById("sdoErrorSpanFecha");
	fecha.onchange = function()
	{
		var fechaF = new Date(fecha.value);
		/*La funcion getDay() retorna como primer dia el lunes por ser un pais de habla hispana*/
		if (fechaF.getDay() != 0) 
		{
    		fecha.classList.add("campoInvalido");
			fechaError.classList.remove("oculto");
		
		}
		else
		{
    		fecha.classList.remove("campoInvalido");
			fechaError.classList.add("oculto");
			cargarTildes();
		}
	}
}

/*Valida que solo haya numeros enteros y puntos*/
function ponerValidacionACampos()
{
	var arrayCamposNumeros = document.getElementsByClassName("input-campo");
	var msgError = document.getElementById("sdoErrorSpanCampos");
	var reg = /^[0-9.]{1,15}$/;
	for (var i = 0; i<arrayCamposNumeros.length; i++)
	{
		arrayCamposNumeros[i].onkeyup = function()
		{
			var match = this.value.match(reg);
			if (!match) 
			{
				if(this.value == "")
				{
	    			this.classList.remove("campoInvalido");
	    			msgError.classList.add("oculto");
				}

				else
				{
	    			this.classList.add("campoInvalido");
	    			msgError.classList.remove("oculto");

				}
			}
			else
			{
	    		this.classList.remove("campoInvalido");
    			msgError.classList.add("oculto");

			}
		}
	}
}

/* 
2 Funciones, una que valida el campo que se le pasa por parametro y otra que valida la fecha.
 */

function validarFecha(fecha)
{
	if (fecha.getDay() != 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function validarCampo(campo)
{
	var valor = campo.value;
	var reg = /^[0-9.]{1,15}$/;
	var match = valor.match(reg);
	if (!match)
	{
		return false;
	}
	else
	{
		return true;
	}
}

/*Toma como parametro el id del input del dato que estamos ingresando,
y valida los campos*/
function cargarDatos(llamado)
{
	var fecha = document.getElementById("sdoFechaInput");
	var fechaDes = fecha.value;
	var campo = document.getElementById(llamado);
	var valorACargar = document.getElementById(llamado).value;
	var fechaError = document.getElementById("sdoErrorSpanFecha");
	var msgError = document.getElementById("sdoErrorSpanCampos");

	// Hay que formatear la fecha antes de validarla.
	var fechaF = new Date(fecha.value);
	if (!validarFecha(fechaF))
	{
		/*Remarco los errores de la fecha*/
		fecha.classList.add("campoInvalido");
		fechaError.classList.remove("oculto");
	}
	else
	{
		/*Oculto los errores de la fecha*/
		fecha.classList.remove("campoInvalido");
		fechaError.classList.add("oculto");

		if (!validarCampo(campo))
		{
			campo.classList.add("campoInvalido");
			msgError.classList.remove("oculto");
		}
		else
		{
			campo.classList.remove("campoInvalido");
			msgError.classList.add("oculto");
			/*Llama a una funcion pasandole por parametro "llamado"*/
			validarFechaAjax(fechaDes, llamado);

			//$("#"+llamado+"Modal").modal();
		}
	}
}

function validarFechaAjax(fecha, llamado)
{
	$.ajax({
    type: "POST",
	url: "php/saldoVerFecha.php",
	dataType: "json",
	data: {'fecha' : fecha, 'llamado' : llamado},
	success: function (result) 
	{
		if (result == true) 
		{
			var valorACargar = document.getElementById(llamado).value;
			// insertarValorAjax(fecha, valorACargar, llamado);
			lanzarModalCarga(fecha, llamado);
		}
		else
		{
			$("#fechaExistenteModal").modal();
		}
    },
    error: function (xhr, status, error) {
    	$("#noConexionModal").modal();
    }
    });
}
/*Funcion para insertar en la tabla saldo despues de validaciones*/

/*Funcion para validar que existe una fecha y su registro para modificar*/

function validarFechaAjaxModificar(fecha, valorACargar, llamado)
{
	$.ajax({
    type: "POST",
	url: "php/saldoVerFecha.php",
	dataType: "json",
	data: {'fecha' : fecha, 'llamado' : llamado ,'valorACargar' : valorACargar},
	success: function (result) 
	{
		if (result == true) 
		{
			("No hay fecha para modificar");

		}
		else
		{

			modificarValorAjax(fecha, valorACargar, llamado);
		}
    },
    error: function (xhr, status, error) {
    	$("#noConexionModal").modal();
    }
    });
}


/*Modifica un registro de saldo*/
function modificarValorAjax(fecha, valorACargar, llamado)
{
	$.ajax({
	    type: "POST",
		url: "php/saldoModificar.php",
		dataType: "json",
		data: {'fecha' : fecha, 'valorACargar' : valorACargar, 'llamado' : llamado},
		success: function (result) 
		{
			$("#saldoModificadoModal").modal();
			return result;
	    },
	    error: function (xhr, status, error, result) {
	    	$("#noConexionModal").modal();
	    }
    })
}

function modificarValorAjaxLauncher()
{
	var fecha = document.getElementById("sdoFechaInput").value;
	var llamado = document.getElementById("inputOcultoModificarSaldo").value;
	var valorACargar = document.getElementById("sdoModificarSpanValorNuevo").innerHTML;
	modificarValorAjax(fecha, valorACargar, llamado);
}


/*Inserta valores en saldo*/

function insertarValorAjaxLauncher()
{
	var fecha = document.getElementById("sdoFechaInput").value;
	var llamado = document.getElementById("inputOcultoInsertarSaldo").value;
	var valorACargar = document.getElementById("sdoCargarSpanValor").innerHTML;
	insertarValorAjax(fecha, valorACargar, llamado);
}

//ESTA ES LA FUNCION QUE SE CONECTA CON LA BASE DE DATOS
function insertarValorAjax(fecha, valorACargar, llamado)
{
	$.ajax({
	    type: "POST",
		url: "php/saldoInsertar.php",
		dataType: "json",
		data: {'fecha' : fecha, 'valorACargar' : valorACargar, 'llamado' : llamado},
		success: function (result) 
		{
			var x = document.getElementById(llamado).parentNode.parentNode;
			console.log(x);
			var xPadre = x.getElementsByClassName("div-campo-botones")[0];
			console.log(xPadre);
			xPadre.getElementsByClassName("saldoCancel")[0].classList.add("oculto");
			xPadre.getElementsByClassName("saldoCheck")[0].classList.remove("oculto");

			$("#saldoCargadoModal").modal();
	    },
	    error: function (xhr, status, error, result)
	    {
	    	$("#noConexionModal").modal();
	    }
    })
}


/*Configuracion de modal de carga*/
/*Hay un solo modal que se configura segun quien lo llama*/
function lanzarModalCarga(fecha, llamado)
{
	switch(llamado) 
	{
    case "ingMinoristaInput":
    	var textoDeCarga = "Ingreso Minorista:"
        break;
    case "ingMayoristaInput":
    	var textoDeCarga = "Ingreso Mayorista:"
        break;
    case "ingExtraordinarioInput":
    	var textoDeCarga = "Ingreso Extraord:"
        break;
    case "egresoInput":
    	var textoDeCarga = "Egreso:"
        break;
    case "egresoExtraordinarioInput":
    	var textoDeCarga = "Egreso Extraord:"
        break;
    case "retirosInput":
    	var textoDeCarga = "Retiros:"
        break;
    case "aCobrarInput":
    	var textoDeCarga = "A Cobrar:"
        break;
    case "aPagarInput":
    	var textoDeCarga = "A Pagar:"
        break;
    case "saldoFinalRealInput":
    	var textoDeCarga = "Saldo Final Real:"
        break;
	}
	var fechaF = addDays(new Date(fecha),1);
	var fechaLatina = fechaF.getDate() + "/" + (fechaF.getMonth()+1) + "/" + fechaF.getFullYear();
	document.getElementById("sdoCargarSpanFecha").innerHTML = fechaLatina;

	/*Para sacar el valor del input que lo llama*/
	var campo = document.getElementById(llamado);
	document.getElementById("sdoCargarSpanValor").innerHTML = campo.value;
	document.getElementById("inputOcultoInsertarSaldo").value = llamado;

	/*Para poner el texto correspondiente en el modal de carga*/	
	document.getElementById("sdoCargaSpanTexto").innerHTML = textoDeCarga;

	$("#cargaModal").modal();
}


/*########################### FUNCIONES DE MODIFICAR ###########################*/


function modificarDatos(llamado)
{
	var fecha = document.getElementById("sdoFechaInput");
	var fechaDes = fecha.value;
	var campo = document.getElementById(llamado);
	var valorACargar = document.getElementById(llamado).value;
	var fechaError = document.getElementById("sdoErrorSpanFecha");
	var msgError = document.getElementById("sdoErrorSpanCampos");

	// Hay que formatear la fecha antes de validarla.
	var fechaF = new Date(fecha.value);
	if (!validarFecha(fechaF))
	{
		/*Remarco los errores de la fecha*/
		fecha.classList.add("campoInvalido");
		fechaError.classList.remove("oculto");
	}
	else
	{
		/*Oculto los errores de la fecha*/
		fecha.classList.remove("campoInvalido");
		fechaError.classList.add("oculto");

		if (!validarCampo(campo))
		{
			campo.classList.add("campoInvalido");
			msgError.classList.remove("oculto");
		}
		else
		{
			campo.classList.remove("campoInvalido");
			msgError.classList.add("oculto");
			validarFechaAjaxModificar(fechaDes, llamado);


			//$("#"+llamado+"Modal").modal();
		}
	}
}


function eliminarDatos(llamado)
{
	var fecha = document.getElementById("sdoFechaInput");
	var fechaDes = fecha.value;
	var fechaError = document.getElementById("sdoErrorSpanFecha");
	var msgError = document.getElementById("sdoErrorSpanCampos");
	validarFechaAjaxBorrar(fechaDes, llamado);
}


/*Funcion para validar la existencia de un registro a borrar*/

function validarFechaAjaxBorrar(fecha, llamado)
{
	$.ajax({
    type: "POST",
	url: "php/saldoVerFecha.php",
	dataType: "json",
	data: {'fecha' : fecha, 'llamado' : llamado},
	success: function (result) 
	{
		if (result == true) 
		{
			$("#borrarFechaNoValidadaModal").modal();
		}
		else
		{
			borrarValorAjax(fecha, llamado);
		}
    },
    error: function (xhr, status, error) {
    	$("noConexionModal").modal();
    }
    });
}


/*Valida que los campos sean correctos*/
function validarFechaAjaxModificar(fecha, llamado)
{
	$.ajax({
    type: "POST",
	url: "php/saldoVerFecha.php",
	dataType: "json",
	data: {'fecha' : fecha, 'llamado' : llamado},
	success: function (result) 
	{
		if (result == true) 
		{
			// Deberia saltar el modal de que no hay datos cargados en esa fecha
			$("#borrarFechaNoValidadaModal").modal();
		}
		else
		{
			// Al llamar al modal la variable "llamado" queda en la nada, la asignamos a un input oculto para no usar
			// Variables globales hu3 hu4 hu5 hu6
			document.getElementById("inputOcultoModificarSaldo").value = llamado;
			$("#contraseniaModificar").modal();
		}
    },
    error: function (xhr, status, error) {
    	$("#noConexionModal").modal();
    }
    });
}


function validarContraseniaModificar()
{

	/*Input con la contrasenia*/
	var inputContrasenia = document.getElementById("sdoContrInputModificar");

	/*Hashea la contrasenia*/
	var contraseniaHash = sha1(document.getElementById("sdoContrInputModificar").value);

	/*Mensaje de error*/
	var spanErrorMsj = document.getElementsByClassName("prodContraseniaSpanMsj");
	$.ajax({
    type: "POST",
	url: "php/validarAdmin.php",
	dataType: "json",
	data: {'password' : contraseniaHash},
	success: function (result) {
		if (result == false) 
		{
			spanErrorMsj[0].classList.remove("oculto");
		}
		else
		{
			/*Esconce del mensaje de error, por si estaba.*/
			spanErrorMsj[0].classList.add("oculto");

			/*Cierra el modal*/
			$("#contraseniaModificar").modal("hide");

			/*Funcion que completa los campos y llama al modal*/
			modalCrearModificar(contraseniaHash);

			// Borro lo que quedaba en el campo, sino se autocompleta.
			}
     },
    error: function (xhr, status, error) {
		$("#noConexionModal").modal();
    }
    });
	inputContrasenia.value = "";
}


function modalCrearModificar(contraseniaHash)
{
	// El PHP tiene que validar otra vez la contrasenia antes de devolver la consulta. 

	// Llama a un ajax y pide los datos en la BDD
	// Si no funciona tira el mensaje de error y se frena la funcion
	// Si funciona pone todos los campos

	var llamado = document.getElementById("inputOcultoModificarSaldo").value;
	var fecha = document.getElementById("sdoFechaInput").value;

	//var asistenciaEsperada = document.getElementById("inputRRHHEsperada").value;
	//var asistenciaReal = document.getElementById("inputRRHHReal").value;

	$.ajax({
	    type: "POST",
		url: "php/saldoTraerPorFecha.php",
		dataType: "json",
		data: {'fecha' : fecha, 'password' : contraseniaHash, 'llamado' : llamado},
		success: function (result) 
		{
			lanzarModalModificar(llamado, result);
			/*
			document.getElementById("rrhhTituloFechaModificar").innerHTML = fecha;
			document.getElementById("rrhhEsperadaFechaModificar").innerHTML = asistenciaEsperada;
			document.getElementById("rrhhRealFechaModificar").innerHTML =  asistenciaReal;
			document.getElementById("rrhhAsistenciaEsperadaAnterior").innerHTML = asist_esperada;
			document.getElementById("rrhhAsistenciaRealAnterior").innerHTML =  asist_real;
			$("#editarDatos").modal();
			*/
	     },
	    error: function (xhr, status, error) {
	    	(xhr);
			("error, deja de joder macho");
    		$("#noConexionModal").modal();
    	}
    });
}

function lanzarModalModificar(llamado, result)
{
	switch(llamado) 
	{
    case "ingMinoristaInput":
    	var textoDeCarga = "Ingreso Minorista:"
        break;
    case "ingMayoristaInput":
    	var textoDeCarga = " Ingreso Mayorista:"
        break;
    case "ingExtraordinarioInput":
    	var textoDeCarga = " Ingreso Extraord:"
        break;
    case "egresoInput":
    	var textoDeCarga = "Egreso:"
        break;
    case "egresoExtraordinarioInput":
    	var textoDeCarga = "Egreso Extraord:"
        break;
    case "retirosInput":
    	var textoDeCarga = "Retiros:"
        break;
    case "aCobrarInput":
    	var textoDeCarga = "A Cobrar:"
        break;
    case "aPagarInput":
    	var textoDeCarga = "A Pagar:"
        break;
    case "saldoFinalRealInput":
    	var textoDeCarga = "Saldo Final Real:"
        break;
	}

	// Agarro la fecha - La formateo como fecha - La formateo como fecha Latina
	var fecha = document.getElementById("sdoFechaInput").value;
	var fechaF = addDays(new Date(fecha),1);
	var fechaLatina = fechaF.getDate() + "/" + (fechaF.getMonth()+1) + "/" + fechaF.getFullYear();
	document.getElementById("sdoModificarSpanFecha").innerHTML = fechaLatina;

	/*Para poner el texto correspondiente en el modal de carga*/	
	document.getElementById("sdoModifiarSpanTextoAnterior").innerHTML = textoDeCarga;
	document.getElementById("sdoModifiarSpanTextoNuevo").innerHTML = textoDeCarga;


	/*Para poner el valor traido de la base de datos*/
	document.getElementById("sdoModificarSpanValorAnterior").innerHTML = result;

	/*Para sacar el valor del input que lo llama*/
	var campo = document.getElementById(llamado);
	document.getElementById("sdoModificarSpanValorNuevo").innerHTML = campo.value;
	$("#modificarModal").modal();
}






/*########################### FUNCIONES DE BORRAR ###########################*/

function eliminarDatos(llamado)
{
	var fecha = document.getElementById("sdoFechaInput");
	var fechaF = new Date(fecha.value);
	var fechaError = document.getElementById("sdoErrorSpanFecha");
	
	if (!validarFecha(fechaF))
	{
		/*Remarco los errores de la fecha*/
		fecha.classList.add("campoInvalido");
		fechaError.classList.remove("oculto");
	}
	else
	{
	/*Saco los errores*/
	fecha.classList.remove("campoInvalido");
	fechaError.classList.add("oculto");

	var fechaDes = fecha.value;
	var msgError = document.getElementById("sdoErrorSpanCampos");
	validarFechaAjaxBorrar(fechaDes, llamado);
	}
}


/*Funcion para validar la existencia de un registro a borrar*/

function validarFechaAjaxBorrar(fecha, llamado)
{
	$.ajax({
    type: "POST",
	url: "php/saldoVerFecha.php",
	dataType: "json",
	data: {'fecha' : fecha, 'llamado' : llamado},
	success: function (result) 
	{
		if (result == true) 
		{
			$("#borrarFechaNoValidadaModal").modal();
		}
		else
		{
			document.getElementById("inputOcultoBorrarSaldo").value = llamado;
			$("#contraseniaBorrar").modal();
			//Pedir contrasenia admin
			//Despues llamar a la modal
			// Recien ahi eliminar
			// borrarValorAjax(fecha, llamado);
		}
    },
    error: function (xhr, status, error) {
    	$("noConexionModal").modal();
    }
    });
}

/*"Borra" el dato*/
function borrarValorAjax(fecha, llamado)
{
	$.ajax({
	    type: "POST",
		url: "php/saldoBorrar.php",
		dataType: "json",
		data: {'fecha' : fecha, 'llamado' : llamado},
		success: function (result) 
		{
			var x = document.getElementById(llamado).parentNode.parentNode;
			var xPadre = x.getElementsByClassName("div-campo-botones")[0];
			xPadre.getElementsByClassName("saldoCancel")[0].classList.remove("oculto");
			xPadre.getElementsByClassName("saldoCheck")[0].classList.add("oculto");

			$("#saldoBorradoModal").modal();
	    },
	    error: function (xhr, status, error, result) {
	    	$("#noConexionModal").modal();
	    }
    })
}



function validarContraseniaBorrar()
{

	/*Input con la contrasenia*/
	var inputContrasenia = document.getElementById("sdoContrInputBorrar");

	/*Hashea la contrasenia*/
	var contraseniaHash = sha1(document.getElementById("sdoContrInputBorrar").value);

	/*Mensaje de error*/
	var spanErrorMsj = document.getElementById("prodContraseniaSpanMsjBorrar");

	$.ajax({
    type: "POST",
	url: "php/validarAdmin.php",
	dataType: "json",
	data: {'password' : contraseniaHash},
	success: function (result) {
		if (result == false) 
		{
			spanErrorMsj.classList.remove("oculto");
		}
		else
		{
			/*Esconce del mensaje de error, por si estaba.*/
			spanErrorMsj.classList.add("oculto");

			/*Cierra el modal*/
			$("#contraseniaBorrar").modal("hide");

			/*Funcion que completa los campos y llama al modal*/
			modalCrearBorrar(contraseniaHash);
			// Borro lo que quedaba en el campo, sino se autocompleta.
			}
     },
    error: function (xhr, status, error) {
		$("#noConexionModal").modal();
    }
    });
	inputContrasenia.value = "";
}


function modalCrearBorrar(contraseniaHash)
{
	// El PHP tiene que validar otra vez la contrasenia antes de devolver la consulta. 

	// Llama a un ajax y pide los datos en la BDD
	// Si no funciona tira el mensaje de error y se frena la funcion
	// Si funciona pone todos los campos

	var llamado = document.getElementById("inputOcultoBorrarSaldo").value;
	var fecha = document.getElementById("sdoFechaInput").value;

	//var asistenciaEsperada = document.getElementById("inputRRHHEsperada").value;
	//var asistenciaReal = document.getElementById("inputRRHHReal").value;

	$.ajax({
	    type: "POST",
		url: "php/saldoTraerPorFecha.php",
		dataType: "json",
		data: {'fecha' : fecha, 'password' : contraseniaHash, 'llamado' : llamado},
		success: function (result) 
		{
			lanzarModalBorrar(llamado, result);
			/*
			document.getElementById("rrhhTituloFechaModificar").innerHTML = fecha;
			document.getElementById("rrhhEsperadaFechaModificar").innerHTML = asistenciaEsperada;
			document.getElementById("rrhhRealFechaModificar").innerHTML =  asistenciaReal;
			document.getElementById("rrhhAsistenciaEsperadaAnterior").innerHTML = asist_esperada;
			document.getElementById("rrhhAsistenciaRealAnterior").innerHTML =  asist_real;
			$("#editarDatos").modal();
			*/
	     },
	    error: function (xhr, status, error) {
    		$("#noConexionModal").modal();
    	}
    });
}

function lanzarModalBorrar(llamado, result)
{
	switch(llamado) 
	{
    case "ingMinoristaInput":
    	var textoDeCarga = "Ingreso Minorista:"
        break;
    case "ingMayoristaInput":
    	var textoDeCarga = " Ingreso Mayorista:"
        break;
    case "ingExtraordinarioInput":
    	var textoDeCarga = " Ingreso Extraord:"
        break;
    case "egresoInput":
    	var textoDeCarga = "Egreso:"
        break;
    case "egresoExtraordinarioInput":
    	var textoDeCarga = "Egreso Extraord:"
        break;
    case "retirosInput":
    	var textoDeCarga = "Retiros:"
        break;
    case "aCobrarInput":
    	var textoDeCarga = "A Cobrar:"
        break;
    case "aPagarInput":
    	var textoDeCarga = "A Pagar:"
        break;
    case "saldoFinalRealInput":
    	var textoDeCarga = "Saldo Final Real:"
        break;
	}


	// Agarro la fechaaaad - La formateo como fecha - La formateo como fecha Latina
	var fecha = document.getElementById("sdoFechaInput").value;
	var fechaF = addDays(new Date(fecha),1);
	var fechaLatina = fechaF.getDate() + "/" + (fechaF.getMonth()+1) + "/" + fechaF.getFullYear();
	document.getElementById("sdoBorrarSpanFecha").innerHTML = fechaLatina;

	/*Para poner el texto correspondiente en el modal de carga*/	
	document.getElementById("sdoBorrarSpanTexto").innerHTML = textoDeCarga;


	/*Para poner el valor traido de la badse de datos*/
	document.getElementById("sdoBorrarSpanValor").innerHTML = result;
	$("#borrarModal").modal();
}



function borrarValorAjaxLauncher()
{
	var fecha = document.getElementById("sdoFechaInput").value;
	var llamado = document.getElementById("inputOcultoBorrarSaldo").value;
	var valorACargar = document.getElementById("sdoBorrarSpanValor").innerHTML;
	borrarValorAjax(fecha, llamado);
}

function cargarTildes()
{
	var fecha = document.getElementById("sdoFechaInput");
	var fechaF = new Date(fecha.value);
	var fechaTil = addDays(fechaF, 1);
	var fechaTilde = String(fechaTil.getFullYear()) +"/"+ String(fechaTil.getMonth()+1) +"/"+ String(fechaTil.getDate());
	// TILDES
	//Un AJAX por cada consulta a las 5 tablas produccion
	//para saber si hay un dato cargado en la fecha para los tildes

	$.ajax({
		 type: "POST",
		url: "php/tildeSaldo.php",
		dataType: "json",
	data: {'fecha' : fechaTilde, 'id_user' : id_user},
	success: function (result) 
	{
		// Si todos los datos son false te manda una lista con UN SOLO false adentro, quien sabe porque.
		if (result.length == 1 && result[0] == false) 
		{
			var cancels = document.getElementsByClassName("saldoCancel");
			var checks = document.getElementsByClassName("saldoCheck");
			for(var i = 0; i < checks.length; i++)
			{
				cancels[i].classList.remove("oculto");
				checks[i].classList.add("oculto");
			}
		}

		// Si aunque sea un solo dato es true te arma la lista con los 7 trues o falses.
		else
		{
			var botones = document.getElementsByClassName("div-campo-botones");
			for(var i=0; i< result.length; i++)
			{
				if (result[i] == true) 
				{
					botones[i].getElementsByClassName("saldoCancel")[0].classList.add("oculto");
					botones[i].getElementsByClassName("saldoCheck")[0].classList.remove("oculto");
				}
				else
				{
					botones[i].getElementsByClassName("saldoCancel")[0].classList.remove("oculto");
					botones[i].getElementsByClassName("saldoCheck")[0].classList.add("oculto");
				}
			}
		}	
  	},
  	error: function (xhr, status, error) 
  	{
	  	console.log(xhr);
	  	console.log(status);
	  	console.log(error);
	  	$("#noConexionModal").modal();}
  	})
}

function addDays(date, days) {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}   