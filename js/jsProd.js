ponerValidacionACampos();
ponerValidacionAFecha();



/*Valida que sea lunes*/
function ponerValidacionAFecha()
{
	var fecha = document.getElementById("produccionFecha");
	fecha.onchange = function()
	{
		var fechaF = new Date(fecha.value);
		var fechaTil = addDays(fechaF, 1);
		var fechaTilde = String(fechaTil.getFullYear()) +"/"+ String(fechaTil.getMonth()+1) +"/"+ String(fechaTil.getDate());
		/*La funcion getDay() retorna como primer dia el lunes por ser un pais de habla hispana*/
		if (fechaF.getDay() != 0) 
		{
    		fecha.classList.add("campoInvalido");
			prodSpanFechaIncorrecta.classList.remove("oculto");
		}
		else
		{
    		fecha.classList.remove("campoInvalido");
			prodSpanFechaIncorrecta.classList.add("oculto");
			produccionOjito()
			//cargarTildes();
		}
	}
}


/*Valida que solo haya numeros enteros y puntos*/
function ponerValidacionACampos()
{
	var arrayCamposNumeros = document.getElementsByClassName("inputValidar");
	for (var i = 0; i<arrayCamposNumeros.length; i++)
	{
		arrayCamposNumeros[i].onkeyup = function()
		{
			var msgError = document.getElementById("prodSpanCamposIncorrectos");
			var reg = /^[0-9.]{1,15}$/;
			var match = this.value.match(reg);
			if (!match) 
			{
				if(this.value == "")
				{
	    			this.classList.remove("campoInvalido");
	    			msgError.classList.add("oculto");
				}
				else
				{
	    			this.classList.add("campoInvalido");
	    			msgError.classList.remove("oculto");
				}
			}
			else
			{
	    		this.classList.remove("campoInvalido");
    			msgError.classList.add("oculto");

			}
		}
	}
}



////////// *FUNCIONES DE CARGA DE DATOS* //////////

/*Valida que los campos pertinentes al boton de cargar sean correctos*/
function validarCargarProduccion(tipoDeCarga)
{
	//Setea la variable cada vez que llama a la funcion
	var validado = true;
	
	//Valido la fecha
	var fecha = document.getElementById("produccionFecha");
	var fechaF = new Date(fecha.value);

	if (fechaF.getDay() != 0) 
	{
		fecha.classList.add("campoInvalido");
		prodSpanFechaIncorrecta.classList.remove("oculto");
		var validado = false;	
	}

	//Valido los campos que sean numeros 
	else 
	{

		var valores = document.getElementsByClassName(tipoDeCarga);
		var msgError = document.getElementById("prodSpanCamposIncorrectos");
		var reg = /^[0-9.]{1,15}$/;

		/*Valido cada uno de los campos*/
		for (var i = 0; i < valores.length; i++) 
		{
			var match = valores[i].value.match(reg);
			
			// Con este match pueden quedar vacios los campos.
			//if (!match && valores[i].value != "")
			
			// Con este match no pueden quedar vacios
			if (!match)
			{
				valores[i].classList.add("campoInvalido");
				msgError.classList.remove("oculto");
				var validado = false;
				break;
			}
		}
	}

	if (validado == true)
	{
		verFechaCargarProduccion(tipoDeCarga);
	}
}


//VALIDACIONES DE EXISTENCIA DE LA FECHA EN BDD//
function verFechaCargarProduccion(tipoDeCarga)
{
	var fecha = document.getElementById("produccionFecha").value;
	var fechaPosta = addDays(fecha, 1);
	var fechaPostota = fechaPosta.getFullYear() + "/" + (fechaPosta.getMonth()+1) + "/" + fechaPosta.getDate();

	$.ajax({
    type: "POST",
	url: "php/prodExisteFecha.php",
	dataType: "json",
	data: {'fecha' : fechaPostota, 'tipoDeCarga' : tipoDeCarga},
	success: function (result) {
		// Aca se empiezan a bifurcar las funciones globales, si no existe fecha y se puede cargar manda a que se creen distintos modales dependiendo
		// Quien la llamo
		if (result == false) 
		{
			if (tipoDeCarga == 'vacmedia') 
			{
				mediaModalCargar(tipoDeCarga);
			}
			else if (tipoDeCarga == 'vaccorte')
			{
				corteModalCargar(tipoDeCarga);
			}
			else if (tipoDeCarga == 'cermedia')
			{
				cerMediaModalCargar(tipoDeCarga);
			}
			else if (tipoDeCarga == 'cercorte')
			{
				cerCorteModalCargar(tipoDeCarga);
			}
			else if (tipoDeCarga == 'pollo')
			{
				polloModalCargar(tipoDeCarga);
			}
			else if (tipoDeCarga == 'fiambre')
			{
				fiambreModalCargar(tipoDeCarga);
			}
			else if (tipoDeCarga == 'embutido')
			{
				embutidoModalCargar(tipoDeCarga);
			}
			
		}
		else
		{
			$("#fechaExistenteModal").modal();
		}
     },
    error: function (xhr, status, error) {
  
    	$("#noConexionModal").modal();
    }
    });
}


// FUNCION QUE FORMATEA EL MODAL.
function crearModalCargar(tipoDeCarga)
{

	//// SE PONE LA FECHA ////

	// Span donde se va a poner la fecha
	var fechaModalCargar = document.getElementById("prodCargaFecha");

	// Fecha ingresada y formateada para lograr el objetivo maligno final de convertirla en una fecha DD/MM/YYYY
	var fechaACargar = document.getElementById("produccionFecha");
	var fechaF = addDays(new Date(fechaACargar.value),1);

	// Conversion al catolicismo, digo, a fecha latina DD/MM/YYYY
	var fechaLatina = fechaF.getDate() + "/" + (fechaF.getMonth()+1) + "/" + fechaF.getFullYear();

	fechaModalCargar.innerHTML=fechaLatina;


	//// SE PONE EL TITULO ////

	var tituloModalCargar = document.getElementById("cargarTituloTabla");

	tituloModalCargar.innerHTML = tipoDeCarga.toUpperCase();


	//// SE PONEN LOS CAMPOS ////

	// Campos que se van a completar
	var camposModalCargar = document.getElementsByClassName("cargarCampos");

	// Valores a cargar
	var valoresACargar = document.getElementsByClassName(tipoDeCarga);

	// Pongo los campos en el modal
	for (var i = 0; i < valoresACargar.length; i++) 
	{
		camposModalCargar[i].value = valoresACargar[i].value;
	}

	// Seteo el input oculto que se va a usar mas adelante.
	document.getElementById("inputOcultoCargar").value = tipoDeCarga;

	$("#cargarDatos").modal();
}


//MODIFICADA PARA NUEVA PRODUCCION//

function cargarProduccionLaunch(inputOcultoCargar)
{

	var fecha = document.getElementById("produccionFecha").value;

	switch(inputOcultoCargar)
	{
	//CAMBIADOS LOS DE VACUNO, A FUTURO AGREGAR LOS DEMAS//	
    case "vaccorte":
        var llamadoPor = "prodvaccorte";
        //var tipo = "corte";
        break;
    case "vacmedia":
        var llamadoPor = "prodvacmedia";
        //var tipo = "media";
        break;
    //CAMBIADOS LOS DE VACUNO, A FUTURO AGREGAR LOS DEMAS//    
    case "cermedia":
        var llamadoPor = "prodcermedia"
        break;
    case "cercorte":
        var llamadoPor = "prodcercorte"
        break;
    case "pollo":
        var llamadoPor = "pollo"
        break;
    case "fiambre":
        var llamadoPor = "fiambre"
        break;
    case "embutido":
        var llamadoPor = "embutido"
        break;
    default:
        break;
	}
	
	/*Campos del modal*/
	if (llamadoPor == "prodvacmedia") 
	{
		var camposModalCargar = document.getElementsByClassName("vacmediaModal");
	}	
	else if (llamadoPor == "prodvaccorte")
	{
		var camposModalCargar = document.getElementsByClassName("vaccorteModal");
	}
	else if (llamadoPor == "prodcermedia")
	{
		var camposModalCargar = document.getElementsByClassName("cermediaModal");
	}
	else if (llamadoPor == "prodcercorte")
	{
		var camposModalCargar = document.getElementsByClassName("cercorteModal");
	}
		else if (llamadoPor == "pollo")
	{
		var camposModalCargar = document.getElementsByClassName("polloModal");
	}
		else if (llamadoPor == "fiambre")
	{
		var camposModalCargar = document.getElementsByClassName("fiambreModal");
	}
		else if (llamadoPor == "embutido")
	{
		var camposModalCargar = document.getElementsByClassName("embutidoModal");
	}

	datosSinPuntos = [llamadoPor, fecha];

    /*
    La funcion de abajo tiene como finalidad sacarle los puntos a los numeros, sino al pasarlos
    a sql te lo tomaba como coma y cargaba 12 en lugar de 12.000"
    */

	for (var i = 0; i < camposModalCargar.length; i++)
	{
		// Se pasa a string y se hace un regex para sacarle el punto
		camposModalCargar[i].value.toString();
		nuevoDato = camposModalCargar[i].value.replace(/\./g, "");
		datosSinPuntos.push(nuevoDato);
	}

	if (llamadoPor == "prodvacmedia") 
	{

		cargarProduccionMedia(datosSinPuntos);
	}
	else if (llamadoPor == "prodvaccorte")
	{
		cargarProduccionCorte(datosSinPuntos);
	}
	else if (llamadoPor == "prodcermedia")
	{
		cargarProduccionCerdoMedia(datosSinPuntos);
	}
	else if (llamadoPor == "prodcercorte")
	{
		cargarProduccionCerdoCorte(datosSinPuntos);
	}
	else if (llamadoPor == "pollo")
	{
		cargarProduccionPollo(datosSinPuntos);
	}
	else if (llamadoPor == "fiambre")
	{
		cargarProduccionFiambre(datosSinPuntos);
	}
	else if (llamadoPor == "embutido")
	{
		cargarProduccionEmbutido(datosSinPuntos);
	}
}

//Funciones para cargar la produccion a BDD


//FUNCIONES MODIFICADAS PARA LA NUEVA PRODUCCION//

function cargarProduccionMedia(arr)
{
	$.ajax({
    type: "POST",
	url: "php/prodVacMediaInsertar.php",
	dataType: "json",
	data: {
		 'tabla' : arr[0],
		 'fecha' : arr[1],
		 'compraKG' : arr[2],
		 'compraUM' : arr[3],
		 'ventaKG' : arr[4],
		 'ventaUM' : arr[5],
		 'prodCorteKG' : arr[6],
		 'prodCorteUM' : arr[7],
		 'prodVariosKG' : arr[8],
		 'prodVariosUM' : arr[9],
		 'sobraKG' : arr[10],
		 'sobraUM' : arr[11]
		 },
	success: function (result) 
	{
		var inputOcultoCargar = document.getElementById("inputOcultoCargar").value;
		var checks = document.getElementById("check"+inputOcultoCargar);
		//checks.getElementsByClassName("prodCheck")[0].classList.remove("oculto");
		//checks.getElementsByClassName("prodCancel")[0].classList.add("oculto");


		$("#produccionCargadaModal").modal();
		produccionOjito();
    },
    error: function (xhr, status, error) {

    	$("#noConexionModal").modal();
    }
    });
}



function cargarProduccionCorte(arr)
{

	$.ajax({
    type: "POST",
	url: "php/prodVacCorteInsertar.php",
	dataType: "json",
	data: { 
		 'tabla' : arr[0],
		 'fecha' : arr[1],
		 'compraCorte' : arr[2],
		 'sobraCorte' : arr[3]
		 },
	success: function (result) 
	{
		var inputOcultoCargar = document.getElementById("inputOcultoCargar").value;
		var checks = document.getElementById("check"+inputOcultoCargar);
		//checks.getElementsByClassName("prodCheck")[0].classList.remove("oculto");
		//checks.getElementsByClassName("prodCancel")[0].classList.add("oculto");


		$("#produccionCargadaModal").modal();
		produccionOjito();
    },
    error: function (xhr, status, error) {

    	$("#noConexionModal").modal();
    }
    });
}


//CERDO NUEVA PRODUCCION//
function cargarProduccionCerdoMedia(arr)
{
	$.ajax({
    type: "POST",
	url: "php/prodCerMediaInsertar.php",
	dataType: "json",
	data: {
		 'tabla' : arr[0],
		 'fecha' : arr[1],
		 'compraKG' : arr[2],
		 'compraUM' : arr[3],
		 'ventaKG' : arr[4],
		 'ventaUM' : arr[5],
		 'prodCorteKG' : arr[6],
		 'prodCorteUM' : arr[7],
		 'sobraKG' : arr[8],
		 'sobraUM' : arr[9]
		 },
	success: function (result) 
	{
		var inputOcultoCargar = document.getElementById("inputOcultoCargar").value;
		var checks = document.getElementById("check"+inputOcultoCargar);
		//checks.getElementsByClassName("prodCheck")[0].classList.remove("oculto");
		//checks.getElementsByClassName("prodCancel")[0].classList.add("oculto");


		$("#produccionCargadaModal").modal();
		produccionOjito();
    },
    error: function (xhr, status, error) {
 
    	$("#noConexionModal").modal();
    }
    });
}

function cargarProduccionCerdoCorte(arr)
{

	$.ajax({
    type: "POST",
	url: "php/prodCerCorteInsertar.php",
	dataType: "json",
	data: { 
		 'tabla' : arr[0],
		 'fecha' : arr[1],
		 'compraCorte' : arr[2],
		 'sobraCorte' : arr[3]
		 },
	success: function (result) 
	{
		var inputOcultoCargar = document.getElementById("inputOcultoCargar").value;
		var checks = document.getElementById("check"+inputOcultoCargar);
		//checks.getElementsByClassName("prodCheck")[0].classList.remove("oculto");
		//checks.getElementsByClassName("prodCancel")[0].classList.add("oculto");


		$("#produccionCargadaModal").modal();
		produccionOjito();
    },
    error: function (xhr, status, error) {
    	
    	$("#noConexionModal").modal();
    }
    });
}

function cargarProduccionPollo(arr)
{
	
	$.ajax({
    type: "POST",
	url: "php/prodPolloInsertar.php",
	dataType: "json",
	data: { 
		 'tabla' : arr[0],
		 'fecha' : arr[1],
		 'compra' : arr[2],
		 'venta' : arr[3],
		 'prodCorte' : arr[4],
		 'sobra' : arr[5],
		 },
	success: function (result) 
	{
		var inputOcultoCargar = document.getElementById("inputOcultoCargar").value;
		var checks = document.getElementById("check"+inputOcultoCargar);
		//checks.getElementsByClassName("prodCheck")[0].classList.remove("oculto");
		//checks.getElementsByClassName("prodCancel")[0].classList.add("oculto");


		$("#produccionCargadaModal").modal();
		produccionOjito();
    },
    error: function (xhr, status, error) {
    
    	$("#noConexionModal").modal();
    }
    });
}

function cargarProduccionFiambre(arr)
{

	$.ajax({
    type: "POST",
	url: "php/prodFiambreInsertar.php",
	dataType: "json",
	data: { 
		 'tabla' : arr[0],
		 'fecha' : arr[1],
		 'produccion' : arr[2],
		 'sobra' : arr[3]
		 },
	success: function (result) 
	{
		var inputOcultoCargar = document.getElementById("inputOcultoCargar").value;
		var checks = document.getElementById("check"+inputOcultoCargar);
		//checks.getElementsByClassName("prodCheck")[0].classList.remove("oculto");
		//checks.getElementsByClassName("prodCancel")[0].classList.add("oculto");


		$("#produccionCargadaModal").modal();
		produccionOjito();
    },
    error: function (xhr, status, error) {
    	
    	$("#noConexionModal").modal();
    }
    });
}

function cargarProduccionEmbutido(arr)
{

	$.ajax({
    type: "POST",
	url: "php/prodEmbutidoInsertar.php",
	dataType: "json",
	data: { 
		 'tabla' : arr[0],
		 'fecha' : arr[1],
		 'produccion' : arr[2],
		 'sobra' : arr[3]
		 },
	success: function (result) 
	{
		var inputOcultoCargar = document.getElementById("inputOcultoCargar").value;
		var checks = document.getElementById("check"+inputOcultoCargar);
		//checks.getElementsByClassName("prodCheck")[0].classList.remove("oculto");
		//checks.getElementsByClassName("prodCancel")[0].classList.add("oculto");


		$("#produccionCargadaModal").modal();
		produccionOjito();
    },
    error: function (xhr, status, error) {
    
    	$("#noConexionModal").modal();
    }
    });
}








/*
FUNCIONES DE CARGAR:
1- Valida los campos y la fecha
2- Valida la fecha en la BDD
4- Crea el modal con los datos
5- Launch para lanzar la consulta
6- Lanza la consulta
*/


/*
FUNCIONES DE MODIFICAR:
1- Valida los campos y la fecha
2- Valida la fecha en la BDD
3- Valida la contrasenia
4- Crea el modal con los datos
5- Launch para lanzar la consulta
6- Lanza la consulta
*/

////////// *ACA TERMINAN LAS FUNCIONES DE CARGA* //////////



////////// *FUNCIONES DE MODIFICAR* //////////



/*Valida que los campos pertinentes al boton de cargar sean correctos*/
function validarModificarProduccion(tipoDeCarga)
{
	/*Setea la variable cada vez que llama a la funcion*/
	var validado = true;
	
	/*Valido la fecha*/
	var fecha = document.getElementById("produccionFecha");
	var fechaF = new Date(fecha.value);
	if (fechaF.getDay() != 0) 
	{
		fecha.classList.add("campoInvalido");
		prodSpanFechaIncorrecta.classList.remove("oculto");	
		var validado = false;	
	}

	/*Valido los campos*/
	else 
	{
		var valores = document.getElementsByClassName(tipoDeCarga);
		var msgError = document.getElementById("prodSpanCamposIncorrectos");
		var reg = /^[0-9.]{1,15}$/;

		/*Valido cada uno de los campos*/
		for (var i = 0; i < valores.length; i++) 
		{
			var match = valores[i].value.match(reg);
			
			// Con este match pueden quedar vacios los campos.
			if (!match && valores[i].value != "")
			
			// Con este match no pueden quedar vacios
			//if (!match)
			{
				valores[i].classList.add("campoInvalido");
				msgError.classList.remove("oculto");
				var validado = false;
				break;
			}
		}
	}

	if (validado == true)
	{
		verFechaModificarProduccion(tipoDeCarga);
	}
}


//VALIDACIONES DE EXISTENCIA DE LA FECHA EN BDD//

function verFechaModificarProduccion(tipoDeCarga)
{
	var fecha = document.getElementById("produccionFecha").value;
	var fechaPosta = addDays(fecha, 1);
	var fechaPostota = fechaPosta.getFullYear() + "/" + (fechaPosta.getMonth()+1) + "/" + fechaPosta.getDate();
	$.ajax({
    type: "POST",
	url: "php/prodExisteFecha.php",
	dataType: "json",
	data: {'fecha' : fecha, 'tipoDeCarga' : tipoDeCarga},
	success: function (result) {
		if (result == false) 
		{
			$("#sinFechaModal").modal();
		}
		else
		{
			/*Pongo el input oculto para saber quien es el tincho que lo esta llamando*/
			document.getElementById("inputOcultoModificar").value = tipoDeCarga;
			$("#contraseniaModificar").modal();
		}
     },
    error: function (xhr, status, error) {
		$("#noConexionModal").modal();
    }
    });
}

function validarContraseniaModificar()
{

	/*Input con la contrasenia*/
	var inputContrasenia = document.getElementById("prodContrInput");

	/*Hashea la contrasenia*/
	var contraseniaHash = sha1(document.getElementById("prodContrInput").value);

	/*Mensaje de error*/
	var spanErrorMsj = document.getElementsByClassName("prodContraseniaSpanMsj");

	$.ajax({
    type: "POST",
	url: "php/validarAdmin.php",
	dataType: "json",
	data: {'password' : contraseniaHash},
	success: function (result) {
		if (result == false) 
		{
			spanErrorMsj[0].classList.remove("oculto");
		}
		else
		{
			/*Esconce del mensaje de error, por si estaba.*/
			spanErrorMsj[0].classList.add("oculto");
			/*Cierra el modal*/
			$("#contraseniaModificar").modal("hide");
			/*Funcion que completa los campos y llama al modal*/
			verProduccionModificar(contraseniaHash);
			// Borro lo que quedaba en el campo, sino se autocompleta.
			}
     },
    error: function (xhr, status, error) {
		$("#noConexionModal").modal();
    }
    });
	inputContrasenia.value = "";
}



//VALIDACIONES DE EXISTENCIA DE LA FECHA EN BDD//

function modificarProduccionLaunch(inputOcultoModificar)
{

	var fecha = document.getElementById("produccionFecha").value;
	//var inputOcultoModificar = document.getElementById("inputOcultoModificar").value;


	switch(inputOcultoModificar)
	{
	//CAMBIADOS LOS DE VACUNO, A FUTURO AGREGAR LOS DEMAS//	
    case "vaccorte":
        var llamadoPor = "prodvaccorte";
        //var tipo = "corte";
        break;
    case "vacmedia":
        var llamadoPor = "prodvacmedia";
        // tipo = "media";
        break;
    //CAMBIADOS LOS DE VACUNO, A FUTURO AGREGAR LOS DEMAS//    
    case "cermedia":
        var llamadoPor = "prodcermedia"
        break;
    case "cercorte":
        var llamadoPor = "prodcercorte"
        break;
    case "fiambre":
        var llamadoPor = "prodfiam"
        break;
    case "embutido":
        var llamadoPor = "prodemb"
        break;
    case "pollo":
        var llamadoPor = "prodpoll"
        break;
    default:
        break;
	}
	
	/*Campos del modal*/
	if (llamadoPor == "prodvacmedia") 
	{
		var camposModalCargar = document.getElementsByClassName("modCargarCamposMedia");
	}
	else if (llamadoPor == "prodvaccorte")
	{
		var camposModalCargar = document.getElementsByClassName("modCargarCamposCorte");
	}
	else if (llamadoPor == "prodcermedia") 
	{
		var camposModalCargar = document.getElementsByClassName("modCargarCamposMediaCerdo");
	}
	else if (llamadoPor == "prodcercorte") 
	{
		var camposModalCargar = document.getElementsByClassName("modCargarCamposCorteCerdo");
	}
	else if (llamadoPor == "prodfiam") 
	{
		var camposModalCargar = document.getElementsByClassName("modCargarCamposFiambre");
	}
	else if (llamadoPor == "prodemb") 
	{
		var camposModalCargar = document.getElementsByClassName("modCargarCamposEmbutido");
	}
	else if (llamadoPor == "prodpoll") 
	{
		var camposModalCargar = document.getElementsByClassName("modCargarCamposPoll");
	}
	

	datosSinPuntos = [llamadoPor, fecha];

    /*
    La funcion de abajo tiene como finalidad sacarle los puntos a los numeros, sino al pasarlos
    a sql te lo tomaba como coma y cargaba 12 en lugar de 12.000"
    */

	for (var i = 0; i < camposModalCargar.length; i++)
	{
		// Se pasa a string y se hace un regex para sacarle el punto
		camposModalCargar[i].value.toString();
		nuevoDato = camposModalCargar[i].value.replace(/\./g, "");
		datosSinPuntos.push(nuevoDato);
	}

	if (llamadoPor == "prodvacmedia") 
	{
		modificarProduccionMedia(datosSinPuntos);
	}
	else if (llamadoPor == "prodvaccorte") 
	{
		modificarProduccionCorte(datosSinPuntos);
	}
	else if (llamadoPor == "prodcermedia")
	{
		modificarProduccionCerdoMedia(datosSinPuntos);
	}
	else if (llamadoPor == "prodcercorte")
	{
		modificarProduccionCerdoCorte(datosSinPuntos);
	}
	else if (llamadoPor == "prodpoll")
	{
		modificarProduccionPollo(datosSinPuntos);
	}
	else if (llamadoPor == "prodfiam")
	{
		modificarProduccionFiambre(datosSinPuntos);
	}
	else if (llamadoPor == "prodemb")
	{
		modificarProduccionEmbutido(datosSinPuntos);
	}

}

//Funciones para cargar la produccion a BDD

function modificarProduccionMedia(arr)
{
	$.ajax({
    type: "POST",
	url: "php/prodVacMediaModificar.php",
	dataType: "json",
	data: {
		 'tabla' : arr[0],
		 'fecha' : arr[1],
		 'compraKG' : arr[2],
		 'compraUM' : arr[3],
		 'ventaKG' : arr[4],
		 'ventaUM' : arr[5],
		 'prodCorteKG' : arr[6],
		 'prodCorteUM' : arr[7],
		 'prodVariosKG' : arr[8],
		 'prodVariosUM' : arr[9],
		 'sobraKG' : arr[10],
		 'sobraUM' : arr[11]
		 },
	success: function (result) 
	{
		$("#produccionModificadaModal").modal();
		produccionOjito();
    },
    error: function (xhr, status, error) {
    
    	$("#noConexionModal").modal();
    }
    });
}

function modificarProduccionCorte(arr)
{

	$.ajax({
    type: "POST",
	url: "php/prodVacCorteModificar.php",
	dataType: "json",
	data: { 
		 'tabla' : arr[0],
		 'fecha' : arr[1],
		 'compraCorte' : arr[2],
		 'sobraCorte' : arr[3]
		 },
	success: function (result) 
	{
		$("#produccionModificadaModal").modal();
		produccionOjito();
    },
    error: function (xhr, status, error) {
    	
    	$("#noConexionModal").modal();
    }
    });
}

function modificarProduccionCerdoMedia(arr)
{
	$.ajax({
    type: "POST",
	url: "php/prodCerMediaModificar.php",
	dataType: "json",
	data: {
		 'tabla' : arr[0],
		 'fecha' : arr[1],
		 'compraKG' : arr[2],
		 'compraUM' : arr[3],
		 'ventaKG' : arr[4],
		 'ventaUM' : arr[5],
		 'prodCorteKG' : arr[6],
		 'prodCorteUM' : arr[7],
		 'sobraKG' : arr[8],
		 'sobraUM' : arr[9]
		 },
	success: function (result) 
	{
		$("#produccionModificadaModal").modal();
		produccionOjito();
    },
    error: function (xhr, status, error) {
    
    	$("#noConexionModal").modal();
    }
    });
}

function modificarProduccionCerdoCorte(arr)
{
	
	$.ajax({
    type: "POST",
	url: "php/prodCerCorteModificar.php",
	dataType: "json",
	data: { 
		 'tabla' : arr[0],
		 'fecha' : arr[1],
		 'compraCorte' : arr[2],
		 'sobraCorte' : arr[3]
		 },
	success: function (result) 
	{
		$("#produccionModificadaModal").modal();
		produccionOjito();
    },
    error: function (xhr, status, error) {
    	
    	$("#noConexionModal").modal();
    }
    });
}

function modificarProduccionPollo(arr)
{
	$.ajax({
    type: "POST",
	url: "php/prodPolloModificar.php",
	dataType: "json",
	data: {
		 'tabla' : arr[0],
		 'fecha' : arr[1],
		 'compra' : arr[2],
		 'venta' : arr[3],
		 'prodCorte' : arr[4],
		 'sobra' : arr[5],
		 },
	success: function (result) 
	{
		$("#produccionModificadaModal").modal();
		produccionOjito();
    },
    error: function (xhr, status, error) {
    

    	$("#noConexionModal").modal();
    }
    });
}

function modificarProduccionFiambre(arr)
{
	$.ajax({
    type: "POST",
	url: "php/prodFiambreModificar.php",
	dataType: "json",
	data: {
		 'tabla' : arr[0],
		 'fecha' : arr[1],
		 'produccion' : arr[2],
		 'sobra' : arr[3],
		 },
	success: function (result) 
	{
		$("#produccionModificadaModal").modal();
		produccionOjito();
    },
    error: function (xhr, status, error) {
    
    	$("#noConexionModal").modal();
    }
    });
}

function modificarProduccionEmbutido(arr)
{
	$.ajax({
    type: "POST",
	url: "php/prodEmbutidoModificar.php",
	dataType: "json",
	data: {
		 'tabla' : arr[0],
		 'fecha' : arr[1],
		 'produccion' : arr[2],
		 'sobra' : arr[3],
		 },
	success: function (result) 
	{
		$("#produccionModificadaModal").modal();
		produccionOjito();
    },
    error: function (xhr, status, error) {
    	
    	$("#noConexionModal").modal();
    }
    });
}

/* ################################# HASTA ACA LAS FUNCIONES ESTAN BIEN XDDDD #################################*/
/* ################################# HASTA ACA LAS FUNCIONES ESTAN BIEN XDDDD #################################*/
/* ################################# HASTA ACA LAS FUNCIONES ESTAN BIEN XDDDD #################################*/


/* ################################# ------ CUIDADO HOMBRES TRABAJANDO :v ------ #################################*/
/* ################################# ------ CUIDADO HOMBRES TRABAJANDO :v ------ #################################*/
/* ################################# ------ CUIDADO HOMBRES TRABAJANDO :v ------ #################################*/

function verProduccionModificar(contraseniaHash)
{

	var inputOcultoModificar = document.getElementById("inputOcultoModificar").value;

	var fecha = document.getElementById("produccionFecha").value;
	var fechaF = addDays(new Date(fecha),1);
	var fechaLatina = fechaF.getDate() + "/" + (fechaF.getMonth()+1) + "/" + fechaF.getFullYear();

	switch(inputOcultoModificar)
	{
    case "vaccorte":
        var tabla = "prodvaccorte"
        var tipo = "corte"
        break;
    case "vacmedia":
        var tabla = "prodvacmedia"
        var tipo = "media"
        break;
    case "cermedia":
        var tabla = "prodcermedia"
        var tipo = "media"
        break;
    case "cercorte":
        var tabla = "prodcercorte"
        var tipo = "corte"
        break;
    case "fiambre":
        var tabla = "prodfiam"
        var tipo = "corte"
        break;
    case "pollo":
        var tabla = "prodpoll"
        var tipo = "corte"
        break;
    case "embutido":
        var tabla = "prodemb"
        var tipo = "corte"
        break;
    default:
        break;
	}

	$.ajax({
	    type: "POST",
		url: "php/produccionVacunoTraerProduccionSemanal.php",
		dataType: "json",
		data: {'fecha' : fecha, 'password' : contraseniaHash, 'tabla' : tabla, 'tipo' :tipo},
		success: function (result) {
	
			/*Pone la fecha formateada en el titulo del modal*/
		
			var valoresLlamadoPor = document.getElementById("inputOcultoModificar").value;

			
			if (tabla == 'prodvacmedia')
			{
					/*Agarra los campos*/
				//document.getElementById("prodModificarFechaMedia").innerHTML = fechaLatina;
				var valoresModificarActuales = document.getElementsByClassName("modCamposAnteriorMedia");
				var valoresACargar = document.getElementsByClassName("modCargarCamposMedia");

				//Agarra datos a cargar
				var datosNuevosACargar = document.getElementsByClassName(valoresLlamadoPor);

				for (var i = 0; i < valoresModificarActuales.length; i++) 
				{
					valoresModificarActuales[i].value = result[i];
				};
				for (var i = 0; i < valoresACargar.length; i++) 
				{
					valoresACargar[i].value = datosNuevosACargar[i].value;
				}

				var fecha = document.getElementById("produccionFecha").value;
				var fechaF = addDays(new Date(fecha),1);
				var fechaLatina = fechaF.getDate() + "/" + (fechaF.getMonth()+1) + "/" + fechaF.getFullYear();
				document.getElementById("fechaModalModificarVacMedia").innerHTML = fechaLatina;


				$("#modalModificarEntero").modal();
			}

			else if (tabla == 'prodvaccorte')
			{
					/*Agarra los campos*/
				//document.getElementById("prodModificarFechaCorte").innerHTML = fechaLatina;
				var valoresModificarActuales = document.getElementsByClassName("modCamposAnteriorCorte");
				var valoresACargar = document.getElementsByClassName("modCargarCamposCorte");

				//Agarra datos a cargar
				var datosNuevosACargar = document.getElementsByClassName(valoresLlamadoPor);

				for (var i = 0; i < valoresModificarActuales.length; i++) 
				{
					valoresModificarActuales[i].value = result[i];
				};
				for (var i = 0; i < valoresACargar.length; i++) 
				{
					valoresACargar[i].value = datosNuevosACargar[i].value;
				}

				var fecha = document.getElementById("produccionFecha").value;
				var fechaF = addDays(new Date(fecha),1);
				var fechaLatina = fechaF.getDate() + "/" + (fechaF.getMonth()+1) + "/" + fechaF.getFullYear();
				document.getElementById("fechaModalModificarVacCorte").innerHTML = fechaLatina;

				$("#modalModificarCortes").modal();
			}

			else if (tabla == 'prodcercorte')
			{
					/*Agarra los campos*/
				//document.getElementById("prodModificarFechaCorte").innerHTML = fechaLatina;
				var valoresModificarActuales = document.getElementsByClassName("modCamposAnteriorCorteCerdo");
				var valoresACargar = document.getElementsByClassName("modCargarCamposCorteCerdo");

				//Agarra datos a cargar
				var datosNuevosACargar = document.getElementsByClassName(valoresLlamadoPor);

				for (var i = 0; i < valoresModificarActuales.length; i++) 
				{
					valoresModificarActuales[i].value = result[i];
				};
				for (var i = 0; i < valoresACargar.length; i++) 
				{
					valoresACargar[i].value = datosNuevosACargar[i].value;
				}

				var fecha = document.getElementById("produccionFecha").value;
				var fechaF = addDays(new Date(fecha),1);
				var fechaLatina = fechaF.getDate() + "/" + (fechaF.getMonth()+1) + "/" + fechaF.getFullYear();
				document.getElementById("fechaModalModificarCerdoCorte").innerHTML = fechaLatina;

				$("#modalModificarCorteCerdo").modal();
			}

			else if (tabla == 'prodcermedia')
			{
					/*Agarra los campos*/
				//document.getElementById("prodModificarFechaCorte").innerHTML = fechaLatina;
				var valoresModificarActuales = document.getElementsByClassName("modCamposAnteriorMediaCerdo");
				var valoresACargar = document.getElementsByClassName("modCargarCamposMediaCerdo");

				//Agarra datos a cargar
				var datosNuevosACargar = document.getElementsByClassName(valoresLlamadoPor);

				for (var i = 0; i < valoresModificarActuales.length; i++) 
				{
					valoresModificarActuales[i].value = result[i];
				};
				for (var i = 0; i < valoresACargar.length; i++) 
				{
					valoresACargar[i].value = datosNuevosACargar[i].value;
				}

				var fecha = document.getElementById("produccionFecha").value;
				var fechaF = addDays(new Date(fecha),1);
				var fechaLatina = fechaF.getDate() + "/" + (fechaF.getMonth()+1) + "/" + fechaF.getFullYear();
				document.getElementById("fechaModalModificarCerdo").innerHTML = fechaLatina;


				$("#modalModificarMediaCerdo").modal();
			}
			else if (tabla == 'prodpoll')
			{
					/*Agarra los campos*/
				//document.getElementById("prodModificarFechaCorte").innerHTML = fechaLatina;
				var valoresModificarActuales = document.getElementsByClassName("modCamposAnteriorPoll");
				var valoresACargar = document.getElementsByClassName("modCargarCamposPoll");

				//Agarra datos a cargar
				var datosNuevosACargar = document.getElementsByClassName(valoresLlamadoPor);

				for (var i = 0; i < valoresModificarActuales.length; i++) 
				{
					valoresModificarActuales[i].value = result[i];
				};
				for (var i = 0; i < valoresACargar.length; i++) 
				{
					valoresACargar[i].value = datosNuevosACargar[i].value;
				}

				var fecha = document.getElementById("produccionFecha").value;
				var fechaF = addDays(new Date(fecha),1);
				var fechaLatina = fechaF.getDate() + "/" + (fechaF.getMonth()+1) + "/" + fechaF.getFullYear();
				document.getElementById("fechaModalModificarPollo").innerHTML = fechaLatina;
				$("#modalModificarPollo").modal();

			}
			else if (tabla == 'prodfiam')
			{
					/*Agarra los campos*/
				//document.getElementById("prodModificarFechaCorte").innerHTML = fechaLatina;
				var valoresModificarActuales = document.getElementsByClassName("modCamposAnteriorFiambre");
				var valoresACargar = document.getElementsByClassName("modCargarCamposFiambre");

				//Agarra datos a cargar
				var datosNuevosACargar = document.getElementsByClassName(valoresLlamadoPor);

				for (var i = 0; i < valoresModificarActuales.length; i++) 
				{
					valoresModificarActuales[i].value = result[i];
				};
				for (var i = 0; i < valoresACargar.length; i++) 
				{
					valoresACargar[i].value = datosNuevosACargar[i].value;
				}
				var fecha = document.getElementById("produccionFecha").value;
				var fechaF = addDays(new Date(fecha),1);
				var fechaLatina = fechaF.getDate() + "/" + (fechaF.getMonth()+1) + "/" + fechaF.getFullYear();
				document.getElementById("fechaModalModificarFiambre").innerHTML = fechaLatina;
				$("#modalModificarFiambre").modal();
			}
			else if (tabla == 'prodemb')
			{
					/*Agarra los campos*/
				//document.getElementById("prodModificarFechaCorte").innerHTML = fechaLatina;
				var valoresModificarActuales = document.getElementsByClassName("modCamposAnteriorEmbutido");
				var valoresACargar = document.getElementsByClassName("modCargarCamposEmbutido");

				//Agarra datos a cargar
				var datosNuevosACargar = document.getElementsByClassName(valoresLlamadoPor);

				for (var i = 0; i < valoresModificarActuales.length; i++) 
				{
					valoresModificarActuales[i].value = result[i];
				};
				for (var i = 0; i < valoresACargar.length; i++) 
				{
					valoresACargar[i].value = datosNuevosACargar[i].value;
				}

				var fecha = document.getElementById("produccionFecha").value;
				var fechaF = addDays(new Date(fecha),1);
				var fechaLatina = fechaF.getDate() + "/" + (fechaF.getMonth()+1) + "/" + fechaF.getFullYear();
				document.getElementById("fechaModalModificarEmbutido").innerHTML = fechaLatina;
				$("#modalModificarEmbutido").modal();
			}
	     },
	    error: function (xhr, status, error, result) {
	

			$("#noConexionModal").modal();

			//$("#noConexionModal").modal();
	    }
    });
}


/*Funciones para borrar la produccion*/
function borrarProduccion(fecha, tabla)
{
	$.ajax({
    type: "POST",
	url: "php/prodBorrar.php",
	dataType: "json",
	data: {'tabla' : tabla, 'fecha' : fecha},
	success: function (result) 
	{
		var inputOcultoCargar = document.getElementById("inputOcultoBorrar").value;
		var checks = document.getElementById("check"+inputOcultoCargar);
		//checks.getElementsByClassName("prodCheck")[0].classList.add("oculto");
		//checks.getElementsByClassName("prodCancel")[0].classList.remove("oculto");

		$("#produccionBorradaModal").modal();
		produccionOjito();
    },
    error: function (xhr, status, error) {
    	$("#noConexionModal").modal();
    }
    });
}

function borrarProduccionLaunch(inputOcultoBorrar)
{
	var fecha = document.getElementById("produccionFecha").value;
	//var inputOcultoBorrar = document.getElementById("inputOcultoBorrar").value;
	switch(inputOcultoBorrar)
	{
		//PARA VACUNO AGREGAR LOS SIGUIENTES CUANDO HAYA CONFIRMACION DEL CLIENTE
    case "vaccorte":
        var tabla = "prodvaccorte"
        break;
    case "vacmedia":
        var tabla = "prodvacmedia"
        break;
        //NECESITAMOS VALIDACION DE CLIENTE
    case "cercorte":
        var tabla = "prodcercorte"
        break;
    case "cermedia":
        var tabla = "prodcermedia"
        break;
    case "fiambre":
        var tabla = "prodfiam"
        break;
    case "pollo":
        var tabla = "prodpoll"
        break;
    case "embutido":
        var tabla = "prodemb"
        break;
    default:
        break;
	}
	borrarProduccion(fecha, tabla);
}

function verProduccionBorrar(contraseniaHash)
{
	var fecha = document.getElementById("produccionFecha").value;
	var fechaF = addDays(new Date(fecha),1);
	var fechaLatina = fechaF.getDate() + "/" + (fechaF.getMonth()+1) + "/" + fechaF.getFullYear();
	var inputOcultoBorrar = document.getElementById("inputOcultoBorrar").value;
	switch(inputOcultoBorrar)
	{
    case "vaccorte":
        var tabla = "prodvaccorte"
        var tipo = "corte"
        break;
    case "vacmedia":
        var tabla = "prodvacmedia"
        var tipo = "media"
        break;
    case "cercorte":
        var tabla = "prodcercorte"
        var tipo = "corte"
        break;
    case "cermedia":
        var tabla = "prodcermedia"
        var tipo = "media"
        break;
    case "fiambre":
        var tabla = "prodfiam"
        var tipo = "media"
        break;
    case "pollo":
        var tabla = "prodpoll"
        var tipo = "media"
        break;
    case "embutido":
        var tabla = "prodemb"
        var tipo = "media"
        break;
    default:
        break;
	}

	if (tabla == 'prodvacmedia') 
	{
		var valoresActuales = document.getElementsByClassName("borrarCamposMedia");
	}
	else if (tabla == 'prodvaccorte')
	{
		var valoresActuales = document.getElementsByClassName("borrarCamposCorte");
	}
	else if (tabla == 'prodcermedia')
	{
		var valoresActuales = document.getElementsByClassName("borrarCamposMediaCerdo");
	}
	else if (tabla == 'prodcercorte')
	{
		var valoresActuales = document.getElementsByClassName("borrarCamposCorteCerdo");
	}
	else if (tabla == 'prodpoll')
	{
		var valoresActuales = document.getElementsByClassName("borrarCamposPollo");
	}
	else if (tabla == 'prodfiam')
	{
		var valoresActuales = document.getElementsByClassName("borrarCamposFiambre");
	}
	else if (tabla == 'prodemb')
	{
		var valoresActuales = document.getElementsByClassName("borrarCamposEmbutido");
	}
	$.ajax({
    type: "POST",
	url: "php/produccionVacunoTraerProduccionSemanal.php",
	dataType: "json",
	data: {'fecha' : fecha, 'password' : contraseniaHash, 'tabla': tabla, 'tipo' : tipo},
	success: function (result) {
		for (var i = 0; i < valoresActuales.length; i++) 
		{
			var campoActual = valoresActuales[i];
			campoActual.value = result[i];
			
		}
		if (tabla == "prodvacmedia") 
		{
			document.getElementById("fechaBorrarMedia").innerHTML = "Borrar datos " + fechaLatina + "?";
			$("#modalBorrarEntero").modal();
		}
		else if (tabla == "prodvaccorte")
		{
			document.getElementById("fechaBorrarCorte").innerHTML = "Borrar datos " + fechaLatina + "?";
			$("#modalBorrarCortes").modal();
		}
		else if (tabla == "prodcermedia")
		{
			document.getElementById("fechaBorrarMediaCerdo").innerHTML = "Borrar datos " + fechaLatina + "?";
			$("#modalBorrarMediaCerdo").modal();
		}
		else if (tabla == "prodcercorte")
		{
			document.getElementById("fechaBorrarCorteCerdo").innerHTML = "Borrar datos " + fechaLatina + "?";
			$("#modalBorrarCorteCerdo").modal();
		}
		else if (tabla == "prodpoll")
		{
			document.getElementById("fechaBorrarPollo").innerHTML = "Borrar datos " + fechaLatina + "?";
			$("#modalBorrarPollo").modal();
		}
		else if (tabla == "prodemb")
		{
			document.getElementById("fechaBorrarEmbutido").innerHTML = "Borrar datos " + fechaLatina + "?";
			$("#modalBorrarEmbutido").modal();
		}
		else if (tabla == "prodfiam")
		{
			document.getElementById("fechaBorrarFiambre").innerHTML = "Borrar datos " + fechaLatina + "?";
			$("#modalBorrarFiambre").modal();
		}
		
     },
    error: function (xhr, status, error, result) {
    
		$("#noConexionModal").modal();
    }
    });
}

function validarBorrarProduccion(tipoDeCarga)
{
	/*Setea la variable cada vez que llama a la funcion*/
	var validado = true;

	/*Valido la fecha*/
	var fecha = document.getElementById("produccionFecha");
	var fechaF = new Date(fecha.value);
	if (fechaF.getDay() != 0) 
	{
		fecha.classList.add("campoInvalido");
		prodSpanFechaIncorrecta.classList.remove("oculto");
		var validado = false;	
	}
	if (validado == true) 
	{
		verFechaBorrarProduccion(fecha.value, tipoDeCarga);
	}

}

function verFechaBorrarProduccion(fecha, tipoDeCarga)
{
	$.ajax({
    type: "POST",
	url: "php/prodExisteFecha.php",
	dataType: "json",
	data: {'tipoDeCarga' : tipoDeCarga, 'fecha' : fecha},
	success: function (result) {
		if (result == false) 
		{
			$("#sinFechaModal").modal();
		}
		else
		{
			document.getElementById("inputOcultoBorrar").value = tipoDeCarga;
			$("#contraseniaBorrar").modal();
		}
     },
    error: function (xhr, status, error) {
    	
		$("#noConexionModal").modal();
    }
    });
}

/*IMPORTANTE AAAAAAAAAAAAAA*/
/*LA VARIABLE ACCESO ES IMPORTANTE PARA QUE NO REVELEN EL CODIGO DEL MODAL 
POR CONSOLA*/
/*HAY QUE VER COMO GARCHA HAGO PARA VALIDAR LA MODIFICACION*/

/*HABRIA QUE RECARGAR LA PAGINA CUANDO SE MODIFICAN DATOS PARA QUE
SE VUELVA A SETEAR LA VARIABLE ACCESO
*/


function validarContraseniaBorrar()
{
	/*Input con la contrasenia*/
	var inputContrasenia = document.getElementById("prodContrInputBorrar");

	/*Hashea la contrasenia*/
	var contraseniaHash = sha1(document.getElementById("prodContrInputBorrar").value);

	/*Mensaje de error*/
	var spanErrorMsj = document.getElementsByClassName("prodContraseniaSpanMsj");

	$.ajax({
    type: "POST",
	url: "php/validarAdmin.php",
	dataType: "json",
	data: {'password' : contraseniaHash},
	success: function (result) {
		if (result == false) 
		{
			spanErrorMsj[0].classList.remove("oculto");
		}
		else
		{
			/*Esconce del mensaje de error, por si estaba.*/
			spanErrorMsj[0].classList.add("oculto");

			/*Cierra el modal*/
			$("#contraseniaBorrar").modal("hide");
			/*Funcion que completa los campos y llama al modal*/
			verProduccionBorrar(contraseniaHash);

			// Borro lo que quedaba en el campo, sino se autocompleta.
			}
     },
    error: function (xhr, status, error) {
		$("#noConexionModal").modal();
    }
    });
	inputContrasenia.value = "";
}

/*Funcion LOCA que pone en mayuscula la primer letra de la palabra pasada por parametro con .capitalize(); */
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

function addDays(date, days) {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}   


// Funcion obsoleta

function cargarTildes()
{
	var fecha = document.getElementById("produccionFecha");
	var fechaF = new Date(fecha.value);
	var fechaTil = addDays(fechaF, 1);
	var fechaTilde = String(fechaTil.getFullYear()) +"/"+ String(fechaTil.getMonth()+1) +"/"+ String(fechaTil.getDate());
	// TILDES
	//Un AJAX por cada consulta a las 5 tablas produccion
	//para saber si hay un dato cargado en la fecha para los tildes
	//VACUNO
	$.ajax({
    type: "POST",
	url: "php/tildeVacuno.php",
	dataType: "json",
	data: {'fecha' : fechaTilde},
	success: function (result) 
	{
		if (result == true) 
		{
			document.getElementById("vacIcoCheck").classList.remove("oculto");
			document.getElementById("vacIcoCancel").classList.add("oculto");
		}
		else
		{
			document.getElementById("vacIcoCheck").classList.add("oculto");
			document.getElementById("vacIcoCancel").classList.remove("oculto");
		}
    },
    error: function (xhr, status, error) {
    
    	$("#noConexionModal").modal();}
    })

	//POLLO
	$.ajax({
    type: "POST",
	url: "php/tildePollo.php",
	dataType: "json",
	data: {'fecha' : fechaTilde},
	success: function (result) 
	{
		if (result == true) 
		{
			document.getElementById("polIcoCheck").classList.remove("oculto");
			document.getElementById("polIcoCancel").classList.add("oculto");
		}
		else
		{
			document.getElementById("polIcoCheck").classList.add("oculto");
			document.getElementById("polIcoCancel").classList.remove("oculto");
		}
    },
    error: function (xhr, status, error) {
    	
    	$("#noConexionModal").modal();}
    })

	//CERDO

	$.ajax({
    type: "POST",
	url: "php/tildeCerdo.php",
	dataType: "json",
	data: {'fecha' : fechaTilde},
	success: function (result) 
	{
		if (result == true) 
		{
			document.getElementById("cerIcoCheck").classList.remove("oculto");
			document.getElementById("cerIcoCancel").classList.add("oculto");
		}
		else
		{
			document.getElementById("cerIcoCheck").classList.add("oculto");
			document.getElementById("cerIcoCancel").classList.remove("oculto");
		}
    },
    error: function (xhr, status, error) {
    	
    	$("#noConexionModal").modal();}
    })

	//EMBUTIDOS
	$.ajax({
    type: "POST",
	url: "php/tildeEmbutido.php",
	dataType: "json",
	data: {'fecha' : fechaTilde},
	success: function (result) 
	{
		if (result == true) 
		{
			document.getElementById("embIcoCheck").classList.remove("oculto");
			document.getElementById("embIcoCancel").classList.add("oculto");
		}
		else
		{
			document.getElementById("embIcoCheck").classList.add("oculto");
			document.getElementById("embIcoCancel").classList.remove("oculto");
		}
    },
    error: function (xhr, status, error) {
    	
    	$("#noConexionModal").modal();}
    })
    
	//FIAMBRE
	$.ajax({
    type: "POST",
	url: "php/tildeFiambre.php",
	dataType: "json",
	data: {'fecha' : fechaTilde},
	success: function (result) 
	{
		if (result == true) 
		{
			document.getElementById("fiaIcoCheck").classList.remove("oculto");
			document.getElementById("fiaIcoCancel").classList.add("oculto");
		}
		else
		{
			document.getElementById("fiaIcoCheck").classList.add("oculto");
			document.getElementById("fiaIcoCancel").classList.remove("oculto");
		}
    },
    error: function (xhr, status, error) {
  
    	$("#noConexionModal").modal();
    	}
    })
    fecha.classList.remove("campoInvalido");
	prodSpanFechaIncorrecta.classList.add("oculto");

}



/*#################################### NUEVAS FUNCIONES ####################################*/

/*SE FIJA QUE BOTON LLAMO A LA FUNCION Y TOMA CAMINOS DISTINTOS DEPENDIENDO*/
function modalCargar(llamadoPor){
	/*switch para distinta funcion depende quien llama*/
}

function modalModificar(llamadoPor){

}




function mediaModalCargar(tipo)
{
	var fecha = document.getElementById("produccionFecha").value;
	var fechaPosta = addDays(fecha, 1);
	var fechaPostota = fechaPosta.getFullYear() + "/" + (fechaPosta.getMonth()+1) + "/" + fechaPosta.getDate();
	var fechaLatina = fechaPosta.getDate() + "/" + (fechaPosta.getMonth()+1) + "/" + fechaPosta.getFullYear()
	var datos = document.getElementsByClassName("vacmedia");
	var datosModal = document.getElementsByClassName("vacmediaModal");
	document.getElementById("inputOcultoCargar").value = tipo;
	document.getElementById("titulitoCargar").innerHTML = "Cargar datos " + fechaLatina + " ?";
	for (var i = 0; i < datos.length; i++) 
	{
		datosModal[i].value = datos[i].value;
	}
	$("#modalCargarEntero").modal();
}


function cerMediaModalCargar(tipo)
{

	var fecha = document.getElementById("produccionFecha").value;
	var fechaPosta = addDays(fecha, 1);
	var fechaPostota = fechaPosta.getFullYear() + "/" + (fechaPosta.getMonth()+1) + "/" + fechaPosta.getDate();
	var fechaLatina = fechaPosta.getDate() + "/" + (fechaPosta.getMonth()+1) + "/" + fechaPosta.getFullYear()
	
	var datos = document.getElementsByClassName("cermedia");
	var datosModal = document.getElementsByClassName("cermediaModal");
	document.getElementById("inputOcultoCargar").value = tipo;
	document.getElementById("titulitoCerdoMedia").innerHTML = "Cargar datos " + fechaLatina + " ?";
	for (var i = 0; i < datos.length; i++) 
	{
		datosModal[i].value = datos[i].value;
	}
	$("#modalCargarMediaCerdo").modal();
}


function corteModalCargar(tipo)
{
	var fecha = document.getElementById("produccionFecha").value;
	var fechaPosta = addDays(fecha, 1);
	var fechaPostota = fechaPosta.getFullYear() + "/" + (fechaPosta.getMonth()+1) + "/" + fechaPosta.getDate();
	var fechaLatina = fechaPosta.getDate() + "/" + (fechaPosta.getMonth()+1) + "/" + fechaPosta.getFullYear()
	var datos = document.getElementsByClassName("vaccorte");
	var datosModal = document.getElementsByClassName("vaccorteModal");

	
	document.getElementById("inputOcultoCargar").value = tipo;
	document.getElementById("titulitoCorte").innerHTML = "Cargar datos " + fechaLatina + " ?";
	for (var i = 0; i < datos.length; i++)
	{
		datosModal[i].value = datos[i].value;
	}
	$("#modalCargarCortes").modal();
}

function cerCorteModalCargar(tipo)
{
	var fecha = document.getElementById("produccionFecha").value;
	var fechaPosta = addDays(fecha, 1);
	var fechaPostota = fechaPosta.getFullYear() + "/" + (fechaPosta.getMonth()+1) + "/" + fechaPosta.getDate();
	var fechaLatina = fechaPosta.getDate() + "/" + (fechaPosta.getMonth()+1) + "/" + fechaPosta.getFullYear()
	var datos = document.getElementsByClassName("cercorte");
	var datosModal = document.getElementsByClassName("cercorteModal");

	document.getElementById("inputOcultoCargar").value = tipo;
	document.getElementById("titulitoCerdoCorte").innerHTML = "Cargar datos " + fechaLatina + " ?";
	for (var i = 0; i < datos.length; i++)
	{
		datosModal[i].value = datos[i].value;
	}
	$("#modalCargarCortesCerdo").modal();
}

function polloModalCargar(tipo)
{
	var fecha = document.getElementById("produccionFecha").value;
	var fechaPosta = addDays(fecha, 1);
	var fechaPostota = fechaPosta.getFullYear() + "/" + (fechaPosta.getMonth()+1) + "/" + fechaPosta.getDate();
	var fechaLatina = fechaPosta.getDate() + "/" + (fechaPosta.getMonth()+1) + "/" + fechaPosta.getFullYear()
	var datos = document.getElementsByClassName("pollo");
	var datosModal = document.getElementsByClassName("polloModal");
	
	document.getElementById("inputOcultoCargar").value = tipo;
	document.getElementById("titulitoPollo").innerHTML = "Cargar datos " + fechaLatina + " ?";
	for (var i = 0; i < datos.length; i++)
	{
		datosModal[i].value = datos[i].value;
	}
	$("#modalCargarPollo").modal();
}

function embutidoModalCargar(tipo)
{
	var fecha = document.getElementById("produccionFecha").value;
	var fechaPosta = addDays(fecha, 1);
	var fechaPostota = fechaPosta.getFullYear() + "/" + (fechaPosta.getMonth()+1) + "/" + fechaPosta.getDate();
	var fechaLatina = fechaPosta.getDate() + "/" + (fechaPosta.getMonth()+1) + "/" + fechaPosta.getFullYear()
	var datos = document.getElementsByClassName("embutido");
	var datosModal = document.getElementsByClassName("embutidoModal");
	
	document.getElementById("inputOcultoCargar").value = tipo;
	document.getElementById("titulitoEmbutido").innerHTML = "Cargar datos " + fechaLatina + " ?";
	for (var i = 0; i < datos.length; i++)
	{
		datosModal[i].value = datos[i].value;
	}
	$("#modalCargarEmbutido").modal();
}

function fiambreModalCargar(tipo)
{
	var fecha = document.getElementById("produccionFecha").value;
	var fechaPosta = addDays(fecha, 1);
	var fechaPostota = fechaPosta.getFullYear() + "/" + (fechaPosta.getMonth()+1) + "/" + fechaPosta.getDate();
	var fechaLatina = fechaPosta.getDate() + "/" + (fechaPosta.getMonth()+1) + "/" + fechaPosta.getFullYear()
	var datos = document.getElementsByClassName("fiambre");
	var datosModal = document.getElementsByClassName("fiambreModal");

	document.getElementById("inputOcultoCargar").value = tipo;
	document.getElementById("titulitoFiambre").innerHTML = "Cargar datos " + fechaLatina + " ?";
	for (var i = 0; i < datos.length; i++)
	{
		datosModal[i].value = datos[i].value;
	}
	$("#modalCargarFiambre").modal();
}

function produccionOjito()
{
	
	var fecha = document.getElementById("produccionFecha");
	var fechaF = new Date(fecha.value);
	var fechaTil = addDays(fechaF, 1);
	var fechaTilde = String(fechaTil.getFullYear()) +"/"+ String(fechaTil.getMonth()+1) +"/"+ String(fechaTil.getDate());

	$.ajax({
    type: "POST",
	url: "php/produccionOjito.php",
	dataType: "json",
	data: {'fecha' : fechaTilde},
	success: function (result) 
	{
	
		ponerTildes(result);
    },
    error: function (xhr, status, error) {
    	
    	$("#noConexionModal").modal();}
    })
}

function ponerTildes(resultados){
	var checks = document.getElementsByClassName("prodCheck");
	var cancels = document.getElementsByClassName("prodCancel");
	for (var i = 0; i < resultados.length; i++) {
		if (resultados[i] == 0) 
		{
			checks[i].classList.add("oculto");
			cancels[i].classList.remove("oculto");
		}
		else
		{
			cancels[i].classList.add("oculto");
			checks[i].classList.remove("oculto");
		}
	}
}