<?php
session_start();
$p_sdo = $_SESSION["p_sdo"];
if ($p_sdo != 1)
{
  header('Location: index.php');
  die();
} 
?>

<!DOCTYPE html>
<html>
<head>
  <link rel="icon" href="resources/pyramid.png">
	<script src="js/jquery-3.2.1.js"></script>
	<link rel="stylesheet" type="text/css" href="css/estilos.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  <script type="text/javascript" src="js/sha.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
  <script type="text/javascript">
    var id_user="<?php echo $_SESSION['iduser']?>";
  </script>
	<title>Carga de saldo</title>
</head> 
<body>

  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <a class="navbar-brand" href="bienvenido.php">Visir</a>
        <ul class="nav navbar-nav pull-right">

          <?php 
          if ($_SESSION["p_admin"] == 1) 
          {
            echo
            "
            <li>
            <a href='admin.php'>Admin</a>
            </li>
            "
            ;
          }

          if ($_SESSION["p_prod"] == 1) 
          {
            echo
            "
            <li>
            <a href='cargaProduccion.php'>Produccion</a>
            </li>
            "
            ;
          }

          if ($_SESSION["p_sdo"] == 1) 
          {
            echo
            "
            <li>
            <a href='cargaDatos.php'>Saldo</a>
            </li>
            "
            ;
          }
          
          

          if ($_SESSION["p_rrhh"] == 1) 
          {
            echo
            "
            <li>
            <a href='cargaRRHH.php'>RRHH</a>
            </li>
            "
            ;
          }
          ?>

          <a href="logout.php">
            <button class="btn btn-danger navbar-btn" class="active">Cerrar Sesion</button>
          </a>
        </ul>
    </div>
  </nav>
  
  <div class="container-fluid col-lg-10 col-lg-offset-1">
    <div class="panel panel-primary filterable">

      <div class="panel-heading">
          <h5 class="panel-title">
            SALDO
          </h5>
          <div class="pull-right">
          </div>
      </div>
    
      <div class="panel-footer">

        <div class="div-principal">

  
          <div class="div-fecha">
            <span class="texto-fecha">Fecha:</span>
            &nbsp;
            <input type="date" id="sdoFechaInput">
            <span class="texto-error-fecha oculto" id="sdoErrorSpanFecha">
              *Ingrese un dia lunes por favor
            </span>
            
          </div>


          <?php 
          if ($_SESSION["p_ing_min"] == 1) 
          {
            echo
            "
            <div class='div-campo'>

              <div class='div-campo-texto'>
                <span class='texto-campo'>Ingresos Minorista:</span>
              </div><!--
           --><div class='div-campo-inputs'>
                <input class='input-campo' type='text' id='ingMinoristaInput'>
              </div><!--
            --><div class='div-campo-botones'>
                <button type='button' class='btn btn-primary' onclick='cargarDatos(\"ingMinoristaInput\")'>Cargar</button>
                <button type='button' class='btn btn-warning' onclick='modificarDatos(\"ingMinoristaInput\")'>Modificar</button>
                <button type='button' class='btn btn-danger' onclick='eliminarDatos(\"ingMinoristaInput\")'>Borrar</button>
                  <object type='image/svg+xml' data='resources/check.svg' class='oculto saldoCheck  svgIcon svgIconSaldo'></object>
                  <object type='image/svg+xml' data='resources/cancel.svg' class='oculto saldoCancel  svgIcon svgIconSaldo'></object>
              </div>

            </div>

            "
            ;
          }

          ?>

          <?php 
          if ($_SESSION["p_ing_may"] == 1) 
          {
            echo
            "
            <div class='div-campo'>

              <div class='div-campo-texto'>
                <span class='texto-campo'>Ingresos Mayorista:</span>
              </div><!--
           --><div class='div-campo-inputs'>
                <input class='input-campo' type='text' id='ingMayoristaInput'>
              </div><!--
           --><div class='div-campo-botones'>
                <button type='button' class='btn btn-primary' onclick='cargarDatos(\"ingMayoristaInput\")'>Cargar</button>
                <button type='button' class='btn btn-warning' onclick='modificarDatos(\"ingMayoristaInput\")'>Modificar</button>
                <button type='button' class='btn btn-danger' onclick='eliminarDatos(\"ingMayoristaInput\")'>Borrar</button>
                  <object type='image/svg+xml' data='resources/check.svg' class='oculto saldoCheck  svgIcon svgIconSaldo'></object>
                  <object type='image/svg+xml' data='resources/cancel.svg' class='oculto saldoCancel  svgIcon svgIconSaldo'></object>
              </div>

            </div>

            "
            ;
          }
          ?>

          <?php 
          if ($_SESSION["p_ing_ext"] == 1) 
          {
            echo
            "
            <div class='div-campo'>

              <div class='div-campo-texto'>
                <span class='texto-campo'>Ingresos Extraordinarios:</span>
              </div><!--
           --><div class='div-campo-inputs'>
                <input class='input-campo' type='text' id='ingExtraordinarioInput'>
              </div><!--
           --><div class='div-campo-botones'>
                <button type='button' class='btn btn-primary' onclick='cargarDatos(\"ingExtraordinarioInput\")'>Cargar</button>
                <button type='button' class='btn btn-warning' onclick='modificarDatos(\"ingExtraordinarioInput\")'>Modificar</button>
                <button type='button' class='btn btn-danger' onclick='eliminarDatos(\"ingExtraordinarioInput\")'>Borrar</button>
                  <object type='image/svg+xml' data='resources/check.svg' class='oculto saldoCheck  svgIcon svgIconSaldo'></object>
                  <object type='image/svg+xml' data='resources/cancel.svg' class='oculto saldoCancel  svgIcon svgIconSaldo'></object>
              </div>

            </div>

            "
            ;
          }
          ?>



          <?php 
          if ($_SESSION["p_egr"] == 1) 
          {
            echo
            "
            <div class='div-campo'>

              <div class='div-campo-texto'>
                <span class='texto-campo'>Egreso:</span>
              </div><!--
           --><div class='div-campo-inputs'>
                <input class='input-campo' type='text' id='egresoInput'>
              </div><!--
           --><div class='div-campo-botones'>
                <button type='button' class='btn btn-primary' onclick='cargarDatos(\"egresoInput\")'>Cargar</button>
                <button type='button' class='btn btn-warning' onclick='modificarDatos(\"egresoInput\")'>Modificar</button>
                <button type='button' class='btn btn-danger' onclick='eliminarDatos(\"egresoInput\")'>Borrar</button>
                <object type='image/svg+xml' data='resources/check.svg' class='oculto saldoCheck  svgIcon svgIconSaldo'></object>
                <object type='image/svg+xml' data='resources/cancel.svg' class='oculto saldoCancel  svgIcon svgIconSaldo'></object>
              </div>

            </div>
            "
            ;
          }
          ?>



          <?php 
          if ($_SESSION["p_egr_ext"] == 1) 
          {
            echo
            "
            <div class='div-campo'>

              <div class='div-campo-texto'>
                <span class='texto-campo'>Egreso Extraordinario:</span>
              </div><!--
           --><div class='div-campo-inputs'>
                <input class='input-campo' type='text' id='egresoExtraordinarioInput'>
              </div><!--
           --><div class='div-campo-botones'>
                <button type='button' class='btn btn-primary' onclick='cargarDatos(\"egresoExtraordinarioInput\")'>Cargar</button>
                <button type='button' class='btn btn-warning' onclick='modificarDatos(\"egresoExtraordinarioInput\")'>Modificar</button>
                <button type='button' class='btn btn-danger' onclick='eliminarDatos(\"egresoExtraordinarioInput\")'>Borrar</button>
                <object type='image/svg+xml' data='resources/check.svg' class='oculto saldoCheck  svgIcon svgIconSaldo'></object>
                <object type='image/svg+xml' data='resources/cancel.svg' class='oculto saldoCancel  svgIcon svgIconSaldo'></object>
              </div>

            </div>
            "
            ;
          }
          ?>



          <?php 
          if ($_SESSION["p_ret"] == 1) 
          {
            echo
            "
            <div class='div-campo'>

              <div class='div-campo-texto'>
                <span class='texto-campo'>Retiros:</span>
              </div><!--
           --><div class='div-campo-inputs'>
                <input class='input-campo' type='text' id='retirosInput'>
              </div><!--
           --><div class='div-campo-botones'>
                <button type='button' class='btn btn-primary' onclick='cargarDatos(\"retirosInput\")'>Cargar</button>
                <button type='button' class='btn btn-warning' onclick='modificarDatos(\"retirosInput\")'>Modificar</button>
                <button type='button' class='btn btn-danger' onclick='eliminarDatos(\"retirosInput\")'>Borrar</button>
                <object type='image/svg+xml' data='resources/check.svg' class='oculto saldoCheck  svgIcon svgIconSaldo'></object>
                <object type='image/svg+xml' data='resources/cancel.svg' class='oculto saldoCancel  svgIcon svgIconSaldo'></object>
              </div>

            </div>
            "
            ;
          }
          ?>

          <?php 
          if ($_SESSION["p_acob"] == 1) 
          {
            echo
            "
            <div class='div-campo'>

              <div class='div-campo-texto'>
                <span class='texto-campo'>A Cobrar:</span>
              </div><!--
           --><div class='div-campo-inputs'>
                <input class='input-campo' type='text' id='aCobrarInput'>
              </div><!--
           --><div class='div-campo-botones'>
                <button type='button' class='btn btn-primary' onclick='cargarDatos(\"aCobrarInput\")'>Cargar</button>
                <button type='button' class='btn btn-warning' onclick='modificarDatos(\"aCobrarInput\")'>Modificar</button>
                <button type='button' class='btn btn-danger' onclick='eliminarDatos(\"aCobrarInput\")'>Borrar</button>
                <object type='image/svg+xml' data='resources/check.svg' class='oculto saldoCheck  svgIcon svgIconSaldo'></object>
                <object type='image/svg+xml' data='resources/cancel.svg' class='oculto saldoCancel  svgIcon svgIconSaldo'></object>
              </div>

            </div>
            "
            ;
          }
          ?>

          <?php 
          if ($_SESSION["p_apag"] == 1) 
          {
            echo
            "
            <div class='div-campo'>

              <div class='div-campo-texto'>
                <span class='texto-campo'>A Pagar:</span>
              </div><!--
           --><div class='div-campo-inputs'>
                <input class='input-campo' type='text' id='aPagarInput'>
              </div><!--
           --><div class='div-campo-botones'>
                <button type='button' class='btn btn-primary' onclick='cargarDatos(\"aPagarInput\")'>Cargar</button>
                <button type='button' class='btn btn-warning' onclick='modificarDatos(\"aPagarInput\")'>Modificar</button>
                <button type='button' class='btn btn-danger' onclick='eliminarDatos(\"aPagarInput\")'>Borrar</button>
                <object type='image/svg+xml' data='resources/check.svg' class='oculto saldoCheck  svgIcon svgIconSaldo'></object>
                <object type='image/svg+xml' data='resources/cancel.svg' class='oculto saldoCancel  svgIcon svgIconSaldo'></object>
              </div>

            </div>
            "
            ;
          }
          ?>

          <?php 
          if ($_SESSION["p_sfr"] == 1) 
          {
            echo
            "
            <div class='div-campo'>

              <div class='div-campo-texto'>
                <span class='texto-campo'>Saldo Final Real:</span>
              </div><!--
           --><div class='div-campo-inputs'>
                <input class='input-campo' type='text' id='saldoFinalRealInput'>
              </div><!--
           --><div class='div-campo-botones'>
                <button type='button' class='btn btn-primary' onclick='cargarDatos(\"saldoFinalRealInput\")'>Cargar</button>
                <button type='button' class='btn btn-warning' onclick='modificarDatos(\"saldoFinalRealInput\")'>Modificar</button>
                <button type='button' class='btn btn-danger' onclick='eliminarDatos(\"saldoFinalRealInput\")'>Borrar</button>
                <object type='image/svg+xml' data='resources/check.svg' class='oculto saldoCheck  svgIcon svgIconSaldo'></object>
                <object type='image/svg+xml' data='resources/cancel.svg' class='oculto saldoCancel  svgIcon svgIconSaldo'></object>
              </div>

            </div>

            "
            ;
          }
          ?>

          <div class="div-campo">
            <span class="texto-error-campos oculto" id="sdoErrorSpanCampos">
              *Ingrese solo numeros enteros por favor.
            </span>
          </div>          
     
        </div>
      </div>

    </div>
  </div>

  <!-- Modal carga de datos -->
  <div id='cargaModal' class='modal fade' role='dialog'>
    <div class='modal-dialog'>
      <div class='modal-content'>

        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h4 class='modal-title'>Cargar Datos?</h4>
        </div>

        <div class='modal-body'>
          <div class='modal-panel-saldo'>

            <div class='campo-panel'>
              <div class='lado-izquierdo'>
                <span class='texto-fecha'>Fecha:</span>
              </div>

              <div class='lado-derecho'>
                <span id='sdoCargarSpanFecha'>...</span>            
              </div>
            </div>

            <div class='campo-panel'>
              <input type="hidden" id="inputOcultoInsertarSaldo">
              <div class='lado-izquierdo' id="sdoCargaSpanTexto">
                ...
              </div>

              <div class='lado-derecho' id="sdoCargarSpanValor">
                ...
              </div>
            </div>

          </div>
        </div>

        <div class='modal-footer'>
          <button type='button' class='btn btn-primary' data-dismiss='modal' onclick="insertarValorAjaxLauncher()">Cargar</button>
          <button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>
        </div>

      </div>
    </div>
  </div>

  

  <!-- Modal modificar de datos -->
  <div id='modificarModal' class='modal fade' role='dialog'>
    <div class='modal-dialog modal-lg'>
      <div class='modal-content'>

        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h4 class='modal-title'>Modificar Datos?</h4>
        </div>

        <div class='modal-body'>
          <div class='modal-panel-saldo modal-saldo'>

            <div class='campo-panel'>
              <div class='lado-izquierdo'>
                <span class='texto-fecha'>Fecha:</span>
              </div>

              <div class='lado-derecho'>
                <span id='sdoModificarSpanFecha'>...</span>            
              </div>
            </div>

            <div class='campo-panel'>
              <div class='lado-izquierdo'>
                <!-- Input oculto que sirve medio que como variable global  -->
                <input type="hidden" id="inputOcultoModificarSaldo">
                <span id="sdoModifiarSpanTextoAnterior">...</span>
                <span>anterior:</span>
              </div>

              <div class='lado-derecho'>
                <span id="sdoModificarSpanValorAnterior">...</span>
              </div>
            </div>

            <div class='campo-panel'>
              <div class='lado-izquierdo'>
                <span id="sdoModifiarSpanTextoNuevo">...</span>
                <span>nuevo:</span>
              </div>

              <div class='lado-derecho'>
                <span id="sdoModificarSpanValorNuevo">...</span>
              </div>
            </div>

          </div>
        </div>

        <div class='modal-footer'>
          <button type='button' class='btn btn-warning' data-dismiss='modal' onclick="modificarValorAjaxLauncher()">Modificar</button>
          <button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>
        </div>

      </div>
    </div>
  </div>

  <!-- Modal borrar datos -->
  <div id='borrarModal' class='modal fade' role='dialog'>
    <div class='modal-dialog'>
      <div class='modal-content'>

        <div class='modal-header'>
          <button type='button' class='close' data-dismiss='modal'>&times;</button>
          <h4 class='modal-title'>Borrar Datos?</h4>
        </div>

        <div class='modal-body'>
          <div class='modal-panel-saldo'>

            <div class='campo-panel'>
              <div class='lado-izquierdo'>
                <span class='texto-fecha'>Fecha:</span>
              </div>

              <div class='lado-derecho'>
                <span id='sdoBorrarSpanFecha'>...</span>            
              </div>
            </div>

            <div class='campo-panel'>
              <input type="hidden" id="inputOcultoBorrarSaldo">
              <div class='lado-izquierdo' id="sdoBorrarSpanTexto">
                ...
              </div>

              <input type="hidden" id="inputOcultoBorrarSaldo">
              <div class='lado-derecho' id="sdoBorrarSpanValor">
                ...
              </div>
            </div>

          </div>
        </div>

        <div class='modal-footer'>
          <button type='button' class='btn btn-danger' data-dismiss='modal' onclick="borrarValorAjaxLauncher()">Borrar</button>
          <button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>
        </div>

      </div>
    </div>
  </div>


  <!-- MODAL QUE AVISA QUE HAY FECHA CARGADA -->
  <div id="fechaExistenteModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title prodContraseniaTextoTitulo">Alerta</h4>
        </div>

        <div class="modal-body">
          <div class="prodContraseniaDivPrincipal">
            <span>Ya hay un registro cargado en esa fecha.</span>
            <br>
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
        </div>

      </div>
    </div>
  </div>

    <!-- MODAL QUE AVISA QUE NO HAY DATO PA BORRA -->

    <div id="borrarFechaNoValidadaModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title prodContraseniaTextoTitulo">Alerta</h4>
          </div>
          <div class="modal-body">
            <div class="prodContraseniaDivPrincipal">
              <span>No hay datos cargados en esa fecha.</span>
              <br>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
          </div>

        </div>
      </div>
    </div>

    <!-- MODAL QUE AVISA QUE NO HAY CONEXIONES CON LA BASE DE DATOS -->
    <div id="noConexionModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title text-center prodContraseniaTextoTitulo textoRojo">Alerta</h4>
          </div>

          <div class="modal-body">
            <div class="prodContraseniaDivPrincipal">
              <span>No se puede conectar con el servidor.</span>
              <br>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
          </div>

        </div>
      </div>
    </div>

    <!-- MODAL QUE AVISA QUE SE CARGARON LOS DATOS -->

    <div id="saldoCargadoModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title prodContraseniaTextoTitulo">Alerta</h4>
          </div>

          <div class="modal-body">
            <div class="prodContraseniaDivPrincipal">
              <span>El dato se cargó correctamente.</span>
              <br>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
          </div>

        </div>
      </div>
    </div>


<!-- MODAL QUE AVISA QUE SE MODIFICARON LOS DATOS -->

    <div id="saldoModificadoModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title prodContraseniaTextoTitulo">Alerta</h4>
          </div>
          <div class="modal-body">
            <div class="prodContraseniaDivPrincipal">
              <span>El dato se modificó correctamente.</span>
              <br>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
          </div>
        </div>
      </div>
    </div>

<!-- MODAL QUE AVISA QUE SE BORRO EL DATO -->

    <div id="saldoBorradoModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title prodContraseniaTextoTitulo">Alerta</h4>
          </div>
          <div class="modal-body">
            <div class="prodContraseniaDivPrincipal">
              <span>El dato se borró correctamente.</span>
              <br>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
          </div>

        </div>
      </div>
    </div>



    <!-- MODAL CONTRASENIA QUE PIDE MODIFICAR -->
    <div id="contraseniaModificar" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title prodContraseniaTextoTitulo">Requiere autorizacion</h4>
          </div>

          <div class="modal-body">
            <div class="prodContraseniaDivPrincipal">
              <span>Contraseña:</span>
              <br>
              <input type="password" class="inputCentrado" id="sdoContrInputModificar">
            </div>
            <div class="prodContraseniaDivMensaje">
              <span class="prodContraseniaSpanMsj oculto">
                Contraseña incorrecta
              </span>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="validarContraseniaModificar()">Aceptar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>

        </div>
      </div>
    </div>


    <!-- MODAL CONTRASENIA QUE PIDE BORRAR -->
    <div id="contraseniaBorrar" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title prodContraseniaTextoTitulo">Requiere autorizacion</h4>
          </div>

          <div class="modal-body">
            <div class="prodContraseniaDivPrincipal">
              <span>Contraseña:</span>
              <br>
              <input type="password" class="inputCentrado" id="sdoContrInputBorrar">
            </div>
            <div class="prodContraseniaDivMensaje">
              <span class="prodContraseniaSpanMsj oculto" id="prodContraseniaSpanMsjBorrar">
                Contraseña incorrecta
              </span>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="validarContraseniaBorrar()">Aceptar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>

        </div>
      </div>
    </div>

  </body>

<script type="text/javascript" src="js/jsSaldo.js"></script>

</html>